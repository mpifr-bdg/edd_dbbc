"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

This script reads a file containing VDIF data. The data is plotted as integrated spectra and
a histogram showing the statistics of the VDIF data.
"""
import argparse
import time
import os
from argparse import RawTextHelpFormatter

import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
import numpy as np
from dbbc import vdif, get_unit
from mpikat.utils import logger

_LOG = logger.getLogger("mpikat.dbbc.scripts.quicklook")
NSEC = 10

def matplot_table_layout(table):
    """
    """
    table.auto_set_font_size(False)
    table.set_fontsize(12)
    for (row, __), cell in table.get_celld().items():
        if (row == 0):
            cell.set_text_props(fontproperties=FontProperties(weight='bold'))

def print_stats(histogram: np.ndarray, samples: int):
    """_summary_

    Args:
        histogram (np.ndarray): _description_
        samples (int): _description_
    """
    _LOG.info("\n\nCH\tTot\t\t--\t\t-\t\t+\t\t++\t--\t-\t+\t++\t\n")
    s_out = ""
    for chan in range(histogram.shape[0]):
        s_out += f"{chan}\t{samples}\t"
        for val in histogram[chan]:
            s_out += f"{val}\t"
        for val in histogram[chan]:
            s_out += f"{val/samples*100:.2f}\t"
        s_out += "\n"
    _LOG.info(s_out)

def reshape_with_padding(array: np.ndarray, new_shape: tuple):
    new_size = np.prod(new_shape)
    current_size = array.size
    if new_size > current_size:
        padded_array = np.zeros(new_size, dtype=array.dtype)
        padded_array[:current_size] = array.flatten()
    else:
        padded_array = array.flatten()[:new_size]
    return padded_array.reshape(new_shape)

def process(reader: vdif.Reader, frames: int, skip: int) -> tuple:
    """_summary_

    Args:
        reader (vdif.Reader): _description_

    Returns:
        tuple: _description_
    """
    nchan = reader.nchannel
    nfft = reader.nsample
    nsec = frames // reader.frames_per_second
    if frames % reader.frames_per_second:
        nsec + 1
    histogram = np.zeros((nchan, 4), dtype=np.int64)
    data = reader.read_data(frames).reshape(-1, nchan).transpose(1,0)
    folded = reshape_with_padding(data, (nchan, nsec, 1000, reader.frames_per_second*reader.nsample // 1000))
    folded = np.sum(np.abs(folded)**2, axis=-1)
    spectrum = np.sum(np.abs(np.fft.rfft(data.reshape(nchan, -1, nfft)[:,::skip], axis=2))**2, axis=1)[:, 1:]
    for i in range(nchan):
        for val in range(4):
            histogram[i][val] = np.sum(data[i] == val)
    return spectrum, histogram, folded

def main_cli():
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('--input_file', '-i', action='store',
                        default="", dest="ifile",
                        help="The input file containing VDIF data")
    parser.add_argument('--nframes', '-n', action='store', default=-1, dest="frames",
                        help="Number of frames to read")
    parser.add_argument('--offset', '-o', action='store', default=0, dest="offset",
                        help="Offset in bytes where to start reading the data")
    parser.add_argument('--center_freq', '-fc', action='store', default=16e6, dest="fc",
                        help="Center freuqnecy of the channel")
    parser.add_argument('--fname', '-t', action='store', default="plot.pdf", dest="fname",
                        help="Title of the plot")
    parser.add_argument('--skip', '-s', action='store', default="4", dest="skip",
                        help="Skips the calculation of spectra to speed up the processing")
    parser.add_argument('--odir', action='store', default="", dest="odir",
                        help="Stores .png's to the directory, If 'show' is disabled or explictly set")
    # Assign arguments to variables
    ifile = str(parser.parse_args().ifile)
    frames = int(parser.parse_args().frames)
    offset = int(parser.parse_args().offset)
    fc = int(parser.parse_args().fc)
    fname = parser.parse_args().fname
    skip = int(parser.parse_args().skip)
    odir = parser.parse_args().odir
    # Process the given file
    with open(ifile, mode="rb") as file:
        reader = vdif.Reader(file, offset)
        fs = reader.sample_rate
        frame_read = reader.frames_per_second * NSEC
        if frames < 1:
            frames = reader.tot_frames
        nchan = reader.header.nchannel
        nfft = reader.header.nsample // 2
        spectrum = np.zeros((nchan, nfft), dtype=np.float32)
        folded = np.zeros((nchan, int(np.ceil(frames / reader.frames_per_second)), 1000), dtype=np.float32)
        histogram = np.zeros((nchan, 4), dtype=np.int64)
        start = 0
        while frames > 0:
            if frames < frame_read:
                fpr = frames
            else:
                fpr = frame_read
            s = time.time()
            spec, hist, fold = process(reader, fpr, skip)
            spectrum += spec
            histogram += hist
            folded[:, start:start+NSEC] = fold
            start += NSEC
            frames -= fpr
            stop = time.time()
            _LOG.info(f"Took {stop -s} to process {fpr} frames = {fpr/(stop-s)} fps")

    # Finally, print the historgram
    tot_samples = np.sum(histogram[0])
    print_stats(histogram, tot_samples)

    # and do the plotting
    div, unit = get_unit(fc)
    left = (fc - fs / 4) / div
    right = (fc + fs / 4) / div
    x_axis = np.linspace(left, right, spectrum.shape[1])
    for chan in range(nchan):
        fft = spectrum[chan]
        fig = plt.figure(figsize=(18,12))
        fig.suptitle(f"CH {chan} in Thread {reader.header.thread_id} -")
        ax = fig.add_subplot(311)
        ax.set_title("Spectrum")
        ax.set_ylabel("Power [dB]")
        ax.plot(x_axis, 10*np.log10(fft))
        ax = fig.add_subplot(312)
        ax.set_title("Folded Time Series")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Time [s]")
        ax.imshow(folded[chan], aspect="auto")
        ax = fig.add_subplot(325)
        table = ax.table(
            cellText=[histogram[chan], [f"{x/tot_samples*100:.2f} %" for x in histogram[chan]]],
            colLabels=["- -"," - "," + ","+ +"],
            loc='center')
        matplot_table_layout(table)
        ax.axis('off')
        ax = fig.add_subplot(326)
        table = ax.table(
            cellText=[
                ["sample rate", fs/(get_unit(fs)[0]), f"{get_unit(fs)[1]}Hz"],
                ["Total samples", tot_samples, ""],
                ["Integration time", tot_samples/fs, "s"]],
            colLabels=["Name","Value","Unit"],
            loc='center')
        matplot_table_layout(table)
        ax.axis('off')
        fig.tight_layout()
        if odir != "":
            plt.savefig(os.path.join(odir,fname))
        else:
            plt.show()

if __name__ == "__main__":
    main_cli()
