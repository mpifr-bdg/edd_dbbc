import aiokatcp
import time
import asyncio
import json
import os
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from dbbc.vdif import Reader
import threading

HOST="edgar-inf00"
PORT=7147
PROV_DESC="tests/vlbi_test"


async def provision(client):
    val, res = await client.request("provision", PROV_DESC)
    print("Client returned", val, res)

async def deprovision(client):
    val, res = await client.request("deprovision")
    print("Client returned", val, res)

async def configure(client):
    val, res = await client.request("configure", '{}')
    print("Client returned", val, res)

async def capture_start(client):
    val, res = await client.request("capture-start")
    print("Client returned", val, res)

async def measurement_prepare(client):
    val, res = await client.request("measurement-prepare")
    print("Client returned", val, res)

async def measurement_start(client):
    val, res = await client.request("measurement-start")
    print("Client returned", val, res)

async def measurement_stop(client):
    val, res = await client.request("measurement-stop", 1)
    print("Client returned", val, res)
    return val, res

async def deconfigure(client):
    val, res = await client.request("deconfigure")
    print("Client returned", val, res)

def wait_until(target_time_str):
    today_date = datetime.utcnow().date()
    target_time = datetime.strptime(target_time_str, "%H:%M:%S").time()
    target = datetime.combine(today_date, target_time)
    print(f"Waiting for {(target-datetime.combine(today_date, datetime.utcnow().time())).total_seconds()}")
    while True:
        now = datetime.combine(today_date, datetime.utcnow().time())
        if now >= target:
            break
        time.sleep(0.1)



def ascii_get_header(hdr: str, name: str) -> str:
    """Retrieve the data from a DADA header parameter

    Args:
        hdr (str): The header string
        name (str): The parameters name

    Returns:
        str: The value of the parameter
    """
    for line in hdr:
        l = line.split(" ")
        l = [x for x in l if x]
        if len(l) < 2:
            continue
        if l[0] == name:
            return l[1]
    return None

def get_subdirs(resp: dict) -> list:
    """Retrieve the sub directories from the pipeline after a measurement-stop

    Args:
        config (dict): The configuration dictioanry
        resp (dict): The pipeline response on measurement-stop

    Returns:
        list: List of subdirs
    """
    subdirs: list = []
    for path, meta in resp.items():
        if "vdif_packer_1" == meta["pipeline_id"]:
            subdirs.append(path)
    return subdirs

def fold(data: np.ndarray, fs: int, nbin: int) -> np.ndarray:
    """Fold the data aligned to 1 second

    Args:
        data (np.ndarray): Raw voltage data
        fs (int): sampling rate
        nbin (int): binning -> number of points per second

    Returns:
        np.ndarray: 2D array slow time vs fast time -> detected power
    """
    inte = fs // nbin
    l = len(data) // fs
    return (np.abs(data.reshape(-1, inte))**2).sum(axis=-1).reshape(l, nbin)

def plot(subdirs: list):

    odata = []
    for path in subdirs:
        fname = os.path.join(path, sorted(os.listdir(path))[0])
        print(f"Working on file {fname}")
        # Open the first file in the subdir
        with open(fname, "rb") as f:
            # Retrieve the DADA header
            hdr = f.read(4096).decode("utf-8").split("\n")
            scs = int(ascii_get_header(hdr, "SAMPLE_CLOCK_START"))
            ts = float(ascii_get_header(hdr, "SYNC_TIME"))
            fs = int(ascii_get_header(hdr, "SAMPLE_CLOCK"))
            reader = Reader(f, 4096)
            data = reader.read_data().flatten()
            start_time = ts + scs / fs
            cutoff = len(data) // fs
            # Read out the data
            odata.append(fold(data[:cutoff*fs], fs, 100))
    print(f"data shape {data.shape}")
    print(f"SYNC_TIME: {ts} SAMPLE_CLOCK: {fs} SAMPLE_CLOCK_START {scs} -> First timestamp: {start_time}")
    # Plot the data
    fig = plt.figure(num=len(subdirs), figsize=(12,12))
    for i in range(len(odata)):
        sub = fig.add_subplot(len(odata)//2, 2, i+1)
        sub.imshow(odata[i], aspect="auto", interpolation="nearest")
        sub.set_xlabel('ms')
        sub.set_ylabel('s')
    fig.suptitle(f"{path.split('/')[0]} SYNC_TIME: {ts} SAMPLE_CLOCK: {fs} SAMPLE_CLOCK_START {scs} -> First timestamp: {start_time}")
    fig.savefig(f"/beegfsEDD/NESSER/vdif_timing/{start_time}.png")

async def docontrol():
    client = await aiokatcp.client.Client.connect(HOST, PORT)
    await client.wait_connected()
    print("Client connected")
    try:
        await provision(client)
        await configure(client)
        await capture_start(client)
        thread = None
        for i in range(25):
            print(f"Run {i+1}")
            await measurement_prepare(client)
            print("Waiting to start")
            await measurement_start(client)
            print("Measurement started\nWaiting to stop")
            await asyncio.sleep(70)
            if thread is not None:
                thread.join()
            res, __ = await measurement_stop(client)
            res = json.loads(res[0].decode('ascii'))
            subdirs = get_subdirs(res)
            thread = threading.Thread(target=plot(subdirs), args=(subdirs,))
            thread.start()
            print(f"Measurement stopped: {res}")
    except Exception as e:
        print(f"Exception: {e}")
        await deprovision(client)
    await deprovision(client)


if __name__ == "__main__":
    asyncio.run(docontrol())