"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

This script reads VDIF files and unpacks it into binary int8 data.
"""
import os
import argparse
from argparse import RawTextHelpFormatter

from dbbc import vdif

def main_cli():
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('--input_file', '-i', action='store', dest="ipath",
                        help="The input files containing VDIF data.")
    parser.add_argument('--odir', '-o', action='store', default="", dest="odir",
                        help="Offset in bytes where to start reading the data")
    parser.add_argument('--offset', '-b', action='store', default=0, dest="offset",
                        help="Offset in bytes where to start reading the data")
    parser.add_argument('--prefix', '-p', action='store', default="", dest="prefix",
                        help="Offset in bytes where to start reading the data")
    ipath = str(parser.parse_args().ipath)
    odir = str(parser.parse_args().odir)
    prefix = str(parser.parse_args().prefix)
    offset = str(parser.parse_args().offset)
    if not odir.endswith("/") and odir != "":
        odir += "/"
    if not os.path.exists(odir):
        os.makedirs(odir)
    # If ifile is a directory and not a file get all files in it
    if os.path.isdir(ipath):
        ifiles = sorted([ipath + s for s in os.listdir(ipath)])
    else:
        ifiles = [ipath]
    for ii, ifile in enumerate(ifiles):
        print(f"Unpacking file {ifile.split('/')[-1]} ({ii+1}/{len(ifiles)})")
        with open(ifile, "rb") as i_file:
            reader = vdif.Reader(i_file, offset)
            fnames = []
            for chan in range(reader.header.nchannel):
                fnames.append(f"{odir}{prefix}channel_{chan}_{reader.header.timestamp}.dat")
                files = [open(fname, "wb") for fname in fnames]
            for __, payload in reader:
                for i, file in enumerate(files):
                    file.write(payload.data[i].tobytes())
                print(f"Processed {payload.update_cnt} frames\r", end="")
            for file in files:
                file.close()

if __name__ == "__main__":
    main_cli()