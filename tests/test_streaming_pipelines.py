"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description: Test all pipelines which starts streaming on capture_start.
    Here these are called streaming-pipelines
"""
import unittest
import json

from mpikat.utils.testing import setup_redis
from mpikat.core.edd_pipeline_aio import EDDPipeline
from mpikat.core import logger as logging

from dbbc.pipelines import DigitalDownConverter, DDCPlotter


_LOG = logging.getLogger("mpikat.dbbc.tests.test_streaming_pipelines")
HOST = "127.0.0.1"


class TEST_StreamingPipelines(unittest.IsolatedAsyncioTestCase):

    def setUp(self) -> None:
        self.redis = setup_redis(HOST)
        return super().setUp()

    def tearDown(self) -> None:
        self.redis.shutdown()
        return super().tearDown()

    async def __sequence(self, pipeline: EDDPipeline, test_conf: dict={}):
        test_conf["data_store"] = {}
        test_conf["data_store"]["port"] = self.redis.server_config["port"]
        test_conf["data_store"]["ip"] = HOST

        await pipeline.set(json.dumps(test_conf).replace(" ", ""))
        self.assertEqual(pipeline.state, 'idle')
        await pipeline.configure()
        self.assertEqual(pipeline.state, 'configured')
        await pipeline.capture_start()
        self.assertEqual(pipeline.state, 'streaming')
        await pipeline.measurement_start()
        self.assertEqual(pipeline.state, 'streaming')
        await pipeline.measurement_stop()
        self.assertEqual(pipeline.state, 'streaming')
        await pipeline.measurement_prepare()
        self.assertEqual(pipeline.state, 'streaming')
        await pipeline.deconfigure()
        self.assertEqual(pipeline.state, 'idle')
        _LOG.info("Pipeline deconfigured")

    async def test_DigitalDownConverter_sequence(self):
        _LOG.info("Testing DigitalDownConverter with default configuration")
        pipeline = DigitalDownConverter("localhost", 1234)
        test_conf = {
            "nonfatal_numacheck":True,
            "input_type":"dummy",
            "output_type":"null"
        }
        _LOG.info("Setted configuration")
        await self.__sequence(pipeline, test_conf)

    async def test_DDCPlotter_sequence(self):
        _LOG.info("Testing DDCPlotter with default configuration")
        pipeline = DDCPlotter("localhost", 1234)
        await self.__sequence(pipeline)


if __name__ == '__main__':
    unittest.main()
