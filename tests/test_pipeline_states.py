"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description: State model testing of all pipelines
"""
import unittest

HOST = "127.0.0.1"
PORT = 1337

from mpikat.utils.testing import state_change_patch, Test_StateModel, Test_StateModelOnRequests, setup_redis
# Patch the state_change decorator / Must be called before importing pipeline implementations
unittest.mock.patch("mpikat.core.edd_pipeline_aio.state_change", wraps=state_change_patch).start()


from dbbc.pipelines import DDCPlotter, DigitalDownConverter, VDIFPacker

class Test_VDIFPacker_StateModel(Test_StateModel):
    def setUp(self) -> None:
        super().setUpBase(VDIFPacker(HOST, PORT), False)

class Test_DDCPlotter_StateModel(Test_StateModel):
    def setUp(self) -> None:
        self.redis = setup_redis(HOST)
        pipeline = DDCPlotter(HOST, PORT)
        pipeline._config["data_store"]["port"] = self.redis.server_config['port']
        super().setUpBase(DDCPlotter(HOST, PORT), True)

    def tearDown(self) -> None:
        self.redis.shutdown()

class Test_DigitalDownConverter_StateModel(Test_StateModel):
    def setUp(self) -> None:
        pipeline = DigitalDownConverter(HOST, PORT)
        pipeline._config["nonfatal_numacheck"] = True
        super().setUpBase(pipeline, True)

class Test_VDIFPacker_StateModelOnRequests(Test_StateModelOnRequests):
    async def asynSetUp(self) -> None:
        await super().setUpBase(VDIFPacker)

class Test_DDCPlotter_StateModelOnRequests(Test_StateModelOnRequests):
    async def asynSetUp(self) -> None:
        await super().setUpBase(DDCPlotter, True)

class Test_DigitalDownConverter_StateModelOnRequests(Test_StateModelOnRequests):
    async def asynSetUp(self) -> None:
        await super().setUpBase(DigitalDownConverter, True)


if __name__ == "__main__":
    unittest.main()
