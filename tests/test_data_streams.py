import unittest

from dbbc import data_streams as ds

class Test_CLI_Commands(unittest.TestCase):

    def setUp(self) -> None:
        return super().setUp()
    def tearDown(self) -> None:
        return super().tearDown()
    def test_ddc_stream_mkrecv(self):
        """Should test the mkrecv command is correctly initalized with the default description
        """
        desc = ds.DDCDataStreamFormat.stream_meta
        desc["key"] = ""
        desc["channel_list"] = set([0,1])
        desc["polarization_list"] = set([0,1])
        cmd = ds.ddc_mkrecv_cmd(desc)
        self.assertTrue(isinstance(cmd, str))

class Test_SpeadHandlers(unittest.TestCase):
    def setUp(self) -> None:
        return super().setUp()
    def tearDown(self) -> None:
        return super().tearDown()

    def test_items_ddc_heap(self) -> None:
        """Should test if DDCSpeadHandler has the correct item group parameters
        """
        tobj = ds.DDCSpeadHandler()
        for key in ds.DDCDataStreamFormat.create_item_group().keys():
            self.assertTrue(key in tobj.item_group)

    # def test_ddc_callback_correct_values(self) -> None:
    #     """Should test if the latest item group contains the correct values
    #     """
    #     import spead2.send
    #     ig = spead2.send.ItemGroup(flavour=spead2.Flavour(4, 64, 48, 0))
    #     ig.add_item(0x1600, 'timestamp', '', shape=(6,), dtype='>u1')
    #     ig.add_item(0x1601, 'channel', '', shape=(6,), dtype='>u1')
    #     ig.add_item(0x1602, 'polarization', '', shape=(6,), dtype='>u1')
    #     ig.add_item(0x1603, 'data', 'Payload data of spead heap', shape=(2048,), dtype='float32')

    #     data = np.random.rand(2048).astype("float32")
    #     tobj = ds.DDCSpeadHandler()

    #     ig["timestamp"].value = convert64_48(1234567)
    #     ig["channel"].value = convert64_48(4)
    #     ig["polarization"].value = convert64_48(1)
    #     ig["data"].value = data

    #     tobj(ig.get_heap())
    #     self.assertEqual(convert48_64(tobj.item_group["timestamp"]), 1234567)
    #     self.assertEqual(convert48_64(tobj.item_group["channel"]), 4)
    #     self.assertEqual(convert48_64(tobj.item_group["polarization"]), 1)
    #     self.assertTrue(np.all(data == tobj.item_group["data"]))



if __name__ == "__main__":
    unittest.main()