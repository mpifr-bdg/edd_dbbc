if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "RELEASE")
endif ()
set(CMAKE_CXX_FLAGS "--std=c++${CMAKE_CXX_STANDARD} -fPIC -DENABLE_CUDA -Wno-missing-field-initializers -Wno-unused-local-typedefs -Wno-error=range-loop-construct")

set(ARCH "broadwell" CACHE STRING "target architecture (-march=native, x86-64), defaults to broadwell")
set(TUNE "generic" CACHE STRING "target tune architecture (-mtune=native, x86-64), defaults to generic")

if(CMAKE_CXX_COMPILER MATCHES icpc)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wcheck -wd2259 -wd1125")
endif()
if (CMAKE_CXX_COMPILER_ID MATCHES Clang)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -march=${ARCH} -mtune=${TUNE}")
endif ()
if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-ignored-attributes -pthread -Werror -Wcast-align -march=${ARCH} -mtune=${TUNE}")
endif ()

set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -pg -Wall -Wextra -pedantic")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O3 -g -Wall -Wextra -pedantic")

# --------------------- #
# -- CUDA NVCC FLAGS -- #
# --------------------- #
set(CUDA_HOST_COMPILER ${CMAKE_CXX_COMPILER})
set(CUDA_PROPAGATE_HOST_FLAGS OFF)

set(CMAKE_CUDA_FLAGS "-DENABLE_CUDA --std=c++${CMAKE_CXX_STANDARD} -Wno-deprecated-gpu-targets --expt-relaxed-constexpr")
set(CMAKE_CUDA_FLAGS_DEBUG "-O0 -G --ptxas-options='-v' '-Xcompiler=-Wextra,-Werror,--all-warnings'")
set(CMAKE_CUDA_FLAGS_RELEASE "-O3 -use_fast_math -restrict --ptxas-options='-v'")
set(CMAKE_CUDA_FLAGS_RELWITHDEBINFO "${CMAKE_CUDA_FLAGS_RELEASE} --generate-line-info")

# Here we need to add the cuda include path - otherwise it might fail when it is not set as ENV
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -I${CUDAToolkit_INCLUDE_DIRS}")

