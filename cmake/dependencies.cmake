find_package(CUDAToolkit REQUIRED)
find_package(Boost COMPONENTS log program_options system date_time thread REQUIRED)
find_package(psrdada_cpp)

include(cmake/compiler_settings.cmake)
include(cmake/FindOrFetch.cmake)

find_or_fetch_package(GTest https://github.com/google/googletest.git v1.14.0 src/include)
set(GTEST_LIBRARIES GTest::gtest_main GTest::gtest GTest::gmock_main GTest::gmock)

# Setup googlebenchmark
if(ENABLE_BENCHMARK)
    set(BENCHMARK_USE_BUNDLED_GTEST, OFF CACHE BOOL "Disable testing" FORCE)
    set(BENCHMARK_ENABLE_GTEST_TESTS OFF CACHE BOOL "Disable testing" FORCE)
    set(BENCHMARK_ENABLE_TESTING OFF CACHE BOOL "Disable testing" FORCE)
    find_or_fetch_package(googlebenchmark https://github.com/google/benchmark.git v1.8.3 googlebenchmark/include)
    set(BENCHMARK_LIBRARIES benchmark::benchmark benchmark::benchmark_main)
endif()

# enable_testing must be called after Benchmark was fetched otherwise benchmark tests run as well
if(ENABLE_TESTING)
    enable_testing()
endif()

set(DEPENDENCY_LIBRARIES ${PSRDADA_CPP_LIBRARIES} ${Boost_LIBRARIES} ${GTEST_LIBRARIES})

