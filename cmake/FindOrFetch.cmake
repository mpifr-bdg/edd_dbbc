function(find_or_fetch_package package_name repo_url tag include_subdir)
    list(APPEND EXTERNAL_PACKAGES ${package_name})
    # Check if FORCE_DOWNLOAD is not set, attempt to find the package
    if(NOT FORCE_DOWNLOAD)
        find_package(${package_name} QUIET)
        if(${package_name}_FOUND)
            message(STATUS "${package_name} found! Version ${${package_name}_VERSION}")
        endif()
    endif()
    # If the package was not found or FORCE_DOWNLOAD is set, use FetchContent
    if(FORCE_DOWNLOAD OR NOT ${package_name}_FOUND)
        set(EXTERNAL_PACKAGES "${EXTERNAL_PACKAGES}" PARENT_SCOPE)
        message(STATUS "Fetching content for ${package_name} from remote..")
        include(FetchContent)
        FetchContent_Declare(
            ${package_name}
            GIT_REPOSITORY ${repo_url}
            GIT_TAG ${tag}
            GIT_PROGRESS TRUE
            PREFIX ${CMAKE_BINARY_DIR}/${package_name}
            CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
        )
        FetchContent_MakeAvailable(${package_name})
        string(TOUPPER "${package_name}" PACKAGE_NAME)
        set(${PACKAGE_NAME}_SOURCE_DIR ${${package_name}_SOURCE_DIR} PARENT_SCOPE)
        set(${PACKAGE_NAME}_INCLUDE_DIR ${${package_name}_SOURCE_DIR}/${include_subdir} PARENT_SCOPE)
    endif()
endfunction()
