# Version 230613

Redesigned of DDC spead stream, time coherence in DDC processing and refactoring VDIF pipeline

## DDC pipeline

- Use a single mksend instance per polarization instead of multiple and dbsplit

## DDC processing

- The output data are reordered according to the heap size. This is a transposition of the form slow time, DDC band, lower and upper, fast time -> TFFT. The old format can be used by setting the flag `nsamples_pack` to `0` in the `ddc_processing.py` script
- Bug fixes in the processing -> added synchronization and processing stream

## VDIF Pipeline

- Make VDIF packer NUMA aware -> process polarization on two different NUMA nodes
- Refactor:
    - revised function description and naming schemes
    - Start the output handlers on measurement-prepare -> Creates new files on each measurement-prepare
    - File size is aligned to full seconds.

---

# Version 230705

Timing offset fixed for VDIF, new scripts and minor performance improvments

## VDIF

- The time offset of 1 second is now fixed in the C++ code
- Handle spillover bytes in the C++ code and corresponding pipeline tests

## General

- Improvements for deconfigure-commands -> killing Processes

## Scripts

- Added dbbc/scripts/ddc_benchmark.py -> benchmarking the ddc pipeline
- Added dbbc/scripts/run_vlbi_modes.py -> Runs VLBI modes from 1MHz to 128MHz, automatically provision backend

---

# Version 230706

Restructuring folders and refactoring installation of CMake/C++

## C++ Folder Structure Changes and Installation
- The C++ main folder is named cpp/ instead of edd_dbbc/
- Install header files to ${CMAKE_INCLUDE_DIR}include/
- Install shared object to ${CMAKE_PREFIX_DIR}lib/

## Python Folder Structure Changes - dbbc module
- The utils folder is removed -> util files and folders moved to base module path (dbbc/)
- Folder integration_tests moved to tests/ as subfolder