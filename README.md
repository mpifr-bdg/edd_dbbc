# VLBI for EDD  

The **Effelsberg Direct Digitization (EDD)** is a modular concept for radio astronomy systems. It integrates components such as receivers, time distribution, digitization, packetization, and backend data processing.  

This repository provides an **EDD backend plugin** for **VLBI (Very Long Baseline Interferometry) operations**, but it has also been successfully applied to other use cases.  

## Requirements  

To build and run this project, you need the following dependencies:  

### **Core Dependencies**  
- **CMake** ≥ 3.18  
- **CUDA** ≥ 11.8  
- **C++ Libraries**:  
  - `psrdada`  
  - `psrdada_cpp`  
  - `boost`  
  - Google Test Framework  

### **Python Dependencies**  
- **Python 3** with:  
  - `numpy`  
  - `matplotlib`  
  - `scipy`  
  - `mpikat`  

## Installation  

The installation process uses **CMake**.  

### **Clone the repository**  

```sh
git clone https://gitlab.mpcdf.mpg.de/mpifr-bdg/edd_dbbc
```

### **Build using CMake**  

```sh
cmake -S edd_dbbc/ -B build_path -DENABLE_PYTHON=ON
```

This will compile both the **Python and C++ code**. However, **CMake does not manage Python dependencies**, so you must install them manually or use `pip` to handle dependencies automatically.  

### **Install Python dependencies**  

Using `pip`, you can install all required Python dependencies from the `pyproject.toml` file:  

```sh
cd edd_dbbc && pip install .
```

## Documentation  

The complete documentation is available here:  
[📖 EDD DBBC Documentation](http://mpifr-bdg.pages.mpcdf.de/edd_dbbc/)  

## Contact  

For questions or contributions, please contact:  

**Author:** Niclas Esser  
📧 Email: [nesser@mpifr-bonn.mpg.de](mailto:nesser@mpifr-bonn.mpg.de)  
