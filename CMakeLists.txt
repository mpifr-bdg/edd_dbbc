# project name and specify the minumum cmake version.
cmake_minimum_required (VERSION 3.18.0 FATAL_ERROR)
project (edd_dbbc LANGUAGES CXX CUDA)

# Compiling and build options for submodules
option(ENABLE_TESTING "Build and enable testing" ON)
option(ENABLE_TESTING_COMPREHENSIVE "Build and enable comprehensive tests. In Debug mode always turned on. Only has effect when ENABLE_TESTING is ON" OFF)
option(ENABLE_BENCHMARK "Build and enable benchmark" OFF)
option(ENABLE_PYTHON "Enables the installation of the python module, turn off for disabling" OFF)
option(ENABLE_CPP "Enables the installation of the C++ sources, turn off for disabling" ON)
option(FORCE_DOWNLOAD "Forces to download and build dependency repositories (googletest, googlebenchmark, tcc, cudawrappers)" OFF)


# The version number.
set(edd_dbbc_VERSION_MAJOR 0)
set(edd_dbbc_VERSION_MINOR 1)
set(edd_dbbc_VERSION_PATCH 0)
set(PROJECT_VERSION "${edd_dbbc_VERSION_MAJOR}.${edd_dbbc_VERSION_MINOR}.${edd_dbbc_VERSION_PATCH}")

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_CUDA_ARCHITECTURES "75;86;89")

# get project dependencies and compiler settings.
if(ENABLE_CPP)
    include(cmake/dependencies.cmake)
    add_subdirectory(cpp)
endif()
if(ENABLE_PYTHON)
    add_subdirectory(python)
endif()

# === Print build options summary.
set(DEBUG_PRINT ON)
if (DEBUG_PRINT)
    message(STATUS "")
    message(STATUS "****************************************************************************")
    message(STATUS " name: ${CMAKE_PROJECT_NAME} version: ${PROJECT_VERSION}")
    message(STATUS "")
    message(STATUS "")
    message(STATUS " Compiler Options:")
    message(STATUS "  Build type: ${CMAKE_BUILD_TYPE}")
    message(STATUS "  C++ Compiler: ${CMAKE_CXX_COMPILER}")
    message(STATUS "  C++ flags:    ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${CMAKE_BUILD_TYPE_UPPER}}")
    message(STATUS "  NVCC flags:   ${CMAKE_CUDA_FLAGS} ${CMAKE_CUDA_FLAGS_${CMAKE_BUILD_TYPE_UPPER}}")
    message(STATUS "  CUDA arch:    ${CMAKE_CUDA_ARCHITECTURES}")
    message(STATUS "")
    message(STATUS "Install locations: (make install)")
    message(STATUS "  Dep libs:  ${DEPENDENCY_LIBRARIES}")
    message(STATUS "  Libraries: ${CMAKE_INSTALL_PREFIX}/lib")
    message(STATUS "  Inclues:   ${CMAKE_INSTALL_PREFIX}/${INCLUDE_INSTALL_DIR}")
    message(STATUS "  Binaries:  ${CMAKE_INSTALL_PREFIX}/${BINARY_INSTALL_DIR}")
    message(STATUS "  Other:     ${CMAKE_INSTALL_PREFIX}/${MODULES_INSTALL_DIR}")
    message(STATUS "****************************************************************************")
    message(STATUS "")
endif (DEBUG_PRINT)
