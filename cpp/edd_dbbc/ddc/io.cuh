/**
 * @file    io.cuh
 * @author  Niclas Esser (nesser@mpifr-bonn.mpg.de)
 * @brief   The io.cuh implements a copy engine which is used to batch the output data of the pipeline.
 * @version 0.1
 * @date    2024-04-25
 *
 */
#pragma once

#include <functional>
#include <thrust/device_vector.h>
#include <cuda.h>
#include <psrdada_cpp/dada_output_stream.hpp>

#include "edd_dbbc/ddc/config.hpp"
#include <psrdada_cpp/cuda_utils.hpp>

namespace edd_dbbc{
namespace ddc{
namespace io{

/**
 * @brief Copies output data of a DDC object under the use of cudaAsyncMemcpy calls.
 *
 * @tparam T the data type of the use pointer
 * @param dst Destination pointer
 * @param src Source pointer
 * @param config The pipeline configuration. Tells the function information about dimension and batching
 * @param stream The Copy stream
 * @param kind The direction of the copy. Defaults to cudaMemcpyDeviceToHost
 */
template<typename T>
void copy_ddc_output(
    T* dst,
    T* src,
    pipe_config& config,
    cudaStream_t& stream,
    cudaMemcpyKind kind=cudaMemcpyDeviceToHost)
{
    // Copy data to the host in two different ways
    // packetize to batches of size _config.batch_size()
    // 1 band lower 1 batch | 1 band upper 1 batch | 2 band lower 1 batch | ... | N band upper 1 batch
    // 1 band lower M batch | 1 band upper M batch | 2 band lower M batch | ... | N band upper M batch
    if(config.batch_size() != 0)
    {
        std::size_t osize = config.output_size() / sizeof(float);
        std::size_t nband = config.ddc.nsignals() * 2;
        std::size_t width = config.batch_size() * sizeof(float);
        std::size_t height = osize / (config.batch_size() * nband);
        std::size_t offset = config.ddc.hilbert.ntaps;
        std::size_t dpitch = config.batch_size() * nband * sizeof(float);
        std::size_t spitch = width;
        for(std::size_t i = 0; i < nband; i++){
            CUDA_ERROR_CHECK(cudaMemcpy2DAsync(
                static_cast<void*>(dst + i * config.batch_size()), dpitch,
                static_cast<void*>(src + i * (osize / nband + 2 * offset) + offset),
                spitch, width, height, kind, stream));
        }
    // Continues bands - not batched
    // 1 band lower | 1 band upper | 2 band lower | 2 band upper | ... | N band upper
    }
    else
    {
        std::size_t offset = config.ddc.hilbert.ntaps;
        std::size_t height = config.ddc.nsignals() * 2;
        std::size_t width = config.output_size() / height;
        std::size_t spitch = width + offset * sizeof(float) * 2;
        CUDA_ERROR_CHECK(cudaMemcpy2DAsync(
            static_cast<void*>(dst), width,
            static_cast<void*>(src + offset),
            spitch, width, height, kind, stream));
    }
}


/**
 * @brief Wrapper for copying data
 *    The template DadaOutputStream::operator(RawBytes&, CopyEngine) calls the operator() of this class
 *
 */
template<typename T>
class CopyEngine : public psrdada_cpp::AbstractCopyEngine{
public:
    using CopyFunc = std::function<void(T*, T*, pipe_config&, cudaStream_t&, cudaMemcpyKind)>;
    /**
     * @brief Construct a new CopyBeamformEngine object
     *
     * @param conf The Pipeline configuration
     * @param stream The stream
     * @param func The function to copy with - must have the signature void(T*, T*, PipelineConfig&, cudaStream_t&, cudaMemcpyKind)
     * @param kind Copy directions defaults to cudaMemcpyDeviceToHost
     */
    CopyEngine(
        pipe_config& conf,
        cudaStream_t& stream,
        CopyFunc func,
        cudaMemcpyKind kind=cudaMemcpyDeviceToHost)
        : _config(conf), _stream(stream), _func(func), _kind(kind){}

    /**
     * @brief Wrapper around CopyFunc
     *
     * @param dst pointer to the destination
     * @param src pointer to the source
     * @param nbytes number of bytes to copy - here ignored/not used
     */
    void operator()(void* dst, void* src, std::size_t nbytes)
    {
        _func(reinterpret_cast<T*>(dst),
            reinterpret_cast<T*>(src),
            _config, _stream, _kind);
        first = false;
    }


    /**
     * @brief Sychronize the stream
     */
    void sync()
    {
        CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream));
    }
private:
    pipe_config _config;
    cudaStream_t _stream;
    cudaMemcpyKind _kind;
    CopyFunc _func;
    bool first = true;
};

}
}
}