/**
 * @file    pipeline.cuh
 * @author  Niclas Esser (nesser@mpifr-bonn.mpg.de)
 * @brief   The pipeline.cuh defines a streaming pipeline under the use of psrdada_cpp components.
 *          The pipleine applys a digital downconversion algorithm.
 *          The function implementation are located in src/pipeline.cu
 * @version 0.1
 * @date    2024-04-25
 *
 */
#pragma once

#include <psrdada_cpp/raw_bytes.hpp>
#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/double_device_buffer.cuh>
#include <psrdada_cpp/double_host_buffer.cuh>
#include <psrdada_cpp/common/unpacker.cuh>
#include <psrdada_cpp/cuda_utils.hpp>
#include <cuda.h>
#include <thrust/device_vector.h>
#include <cmath>
#include <psrdada/ascii_header.h> // dada
#include <cstring>
#include <iostream>
#include <sstream>
#include <chrono>

#include "edd_dbbc/math_tools.cuh"
#include "edd_dbbc/ddc/config.hpp"
#include "edd_dbbc/ddc/io.cuh"
#include "edd_dbbc/ddc/ddc.cuh"

namespace edd_dbbc {
namespace ddc {

/**
 @class Pipeline
 @brief Convert data to 2bit data in VDIF format.
 */
template <typename HandlerType, typename InputType>
class Pipeline
{
public:
    /**
     * @brief      Constructor
     *
     * @param      config The pipeline config
     * @param      handler Output handler
     *
     */
    Pipeline(
        const pipe_config& config,
        HandlerType &handler
    );

    ~Pipeline();

    /**
     * @brief      A callback to be called on connection
     *             to a ring buffer.
     *
     * @details     The first available header block in the
     *             in the ring buffer is provided as an argument.
     *             It is here that header parameters could be read
     *             if desired.
     *
     * @param      block  A RawBytes object wrapping a DADA header buffer
     */
    void init(psrdada_cpp::RawBytes &block);

    /**
     * @brief      A callback to be called on acqusition of a new
     *             data block.
     *
     * @param      block  A RawBytes object wrapping a DADA data buffer output
     *             are the integrated specttra with/without bit set.
     */
    bool operator()(psrdada_cpp::RawBytes &block);

    /**
     * @brief Get the number of calls to operator()-function
     *
     * @return std::size_t
     */
    std::size_t call_count(){return _call_count;}

    /**
     * @brief Get the number of input samples
     *
     * @return std::size_t
     */
    std::size_t isamples (){return _config.isamples();}

    /**
     * @brief Get the number of the output size
     *
     * @return std::size_t
     */
    std::size_t output_size(){return _config.output_size();}

    /**
     * @brief Get the used ddc config
     *
     * @return ddc_t
     */
    ddc_t ddc_config(){return _processor->config();}

    /**
     * @brief Get the pipeline config
     *
     * @return pipe_config
     */
    pipe_config config(){return _config;}

private:
    std::size_t _call_count;
    pipe_config _config;
    HandlerType &_handler;
    // Data arrays / buffers;
    psrdada_cpp::DoubleDeviceBuffer<float> unpacked_buffer;
    psrdada_cpp::DoubleDeviceBuffer<float> resampled_buffer;
    psrdada_cpp::DoubleDeviceBuffer<InputType> input_buffer;
    // Processing objects
    std::unique_ptr<PolyphaseDownConverter<thrust::device_vector<float>, thrust::device_vector<float2>>> _processor;
    std::unique_ptr<psrdada_cpp::Unpacker> _unpacker;
    // CUDA streams
    cudaStream_t host2dev_stream;
    cudaStream_t processing_stream;
    cudaStream_t dev2host_stream;
};

}
}


#include "edd_dbbc/ddc/detail/pipeline.cu"
