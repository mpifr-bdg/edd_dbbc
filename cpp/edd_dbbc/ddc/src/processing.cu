#include "edd_dbbc/ddc/processing.h"

namespace edd_dbbc {
namespace ddc {
namespace processing {
namespace kernel {


__global__ void convolve(
    const float* idata,
    const float* filter,
    float* odata,
    unsigned filter_size,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned tidy = blockIdx.y;
    float tmp = 0;
    if(tidx < width){
        for(unsigned j = 0; j < filter_size; j++)
        {
            unsigned i_idx = tidy * width + tidx + j - (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp += idata[i_idx] * filter[j];
            }
        }
        odata[tidy * width + tidx] = tmp;
    }
}


__global__ void convolve(
    const float2* idata,
    const float* filter,
    float2* odata,
    unsigned filter_size,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned tidy = blockIdx.y;
    float2 tmp = {.0f,.0f};
    if(tidx < width){
        for(unsigned j = 0; j < filter_size; j++)
        {
            unsigned i_idx = tidy * width + tidx + j - (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp.x += idata[i_idx].x * filter[j];
                tmp.y += idata[i_idx].y * filter[j];
            }
        }
        odata[tidy * width + tidx] = tmp;
    }
}


__global__ void convolve_hilbert(
    const float2* idata,
    const float* filter,
    float* odata,
    unsigned filter_size,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    unsigned tidy = blockIdx.y;
    float2 tmp = {.0f,.0f};

    extern __shared__ float s_filter[];

    for(int i = threadIdx.x; i < filter_size; i+=blockDim.x){
        s_filter[i] = filter[i];
    }
__syncthreads();

    if(tidx < width){
        for(unsigned j = 0; j < filter_size; j++)
        {
            unsigned i_idx = tidy * width + tidx - j + (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp.x += idata[i_idx].x * s_filter[j];
                tmp.y += idata[i_idx].y * s_filter[filter_size - 1 - j];
            }
        }
        odata[tidy * width * 2 + tidx] = tmp.x + tmp.y;
        odata[tidy * width * 2 + tidx + width] = tmp.x - tmp.y;
    }
}


__global__ void poly_resample(const float2* idata,
    const float* filter,
    float2* odata,
    unsigned up,
    unsigned down,
    unsigned width,
    unsigned ntaps)
{
    unsigned tidx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned tidy = blockIdx.y;
    unsigned o_width = width * up / down;
    unsigned half_fir = (ntaps - 1) / 2;
    unsigned rotate = up - down + up * down;
    unsigned iidx = 0;
    extern __shared__ float s_filter[];

    for(int i = threadIdx.x; i < ntaps; i+=blockDim.x){
        s_filter[i] = filter[i];
    }
__syncthreads();
    for(unsigned oidx = tidx; oidx < o_width; oidx+=blockDim.x*gridDim.x){
        float2 tmp = {0,0};
        unsigned offset = (oidx * down);
        unsigned poly_branch = (half_fir + rotate * oidx) % up;
        for(unsigned fidx = poly_branch; fidx < ntaps; fidx+=up){
            iidx = tidy * width + (offset + fidx - half_fir) / up;
            if(iidx >= tidy * width && iidx < (tidy+1) * width){
                tmp.x += idata[iidx].x * s_filter[fidx];
                tmp.y += idata[iidx].y * s_filter[fidx];
            }
        }
        odata[tidy * o_width + oidx] = tmp;
    }
}


__global__ void mat_vec_mult(
    const float2* matrix,
    const float* vector,
    float2* odata,
    float2* alpha,
    unsigned width,
    unsigned height)
{
    __shared__ float s_vec[1024];
    unsigned glob_idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(glob_idx < width){
        s_vec[threadIdx.x] = vector[glob_idx];
        __syncthreads();
        for(unsigned i = glob_idx; i < height*width; i+=width){
            odata[i] = cmult(matrix[i], alpha[i/width], s_vec[threadIdx.x]);
        }
    }
}



const unsigned N_OSAMPLES = 32;

/**
 * @brief
 *
 * @param idata
 * @param filter
 * @param odata
 * @param up
 * @param down
 * @param width
 * @param ntaps
 */
__global__ void mix_poly_resample(
    const float* idata,
    const float* filter,
    const float* f_lo,
    const float2* oscillations,
    float2* odata,
    const int up,
    const int down,
    const int width,
    const int height,
    const int half_fir,
    const int ntaps_pad,
    const float t0)
{
    // The block-wide thread index
    const int tid = threadIdx.x;
    // The grid offset with respect to the output data
    const int offset = blockIdx.x * N_OSAMPLES;
    // The output index - only 32 threads of the first warp are used, rest is ignored
    const int ooidx = offset + threadIdx.x;
    // Number of warps
    const int n_warp = blockDim.x / WARP_SIZE;
    // The number of output items (per band)
    const int o_width = width * up / down;
    // The length of the padded poly branch -> always padded to a multiple of WARP_SIZE
    const int poly_branch_len_pad = ntaps_pad / up; // Pad poly branch length - a multiple of 32
    // The floored sampling ratio
    const int ratio_int = down / up;
    // The rotation is the number of branch rotations per polyphase cycle
    // const int rotate = down * up + up - down;
    const int rotate = ratio_int * up + up - down;
    // Used to compare if the used poly_branch is smaller than the comprator
    const int comp = static_cast<float>(up) * ((down / static_cast<float>(up)) - ratio_int) + 0.5;
    // const int comp = up * fdividef(down, up) - ratio_int + 0.5;
    // The shift width after each output iteration
    int shift = ratio_int;
    // The input index used to load data from the global memory
    int iidx = 0;
    // Temp variable to store and load input data
    float tmpf = 0;
    // Temp variable used for computitation stroing the real part
    float tmp_real = .0;
    // Temp variable used for computitation storing the imaginary part
    float tmp_imag = .0;
    // Temp variable the filtered product
    float filtered;
    // Temp variable to store the real part of the mixing product
    float mixed_real;
    // Temp variable to store the imaginary part of the mixing product
    float mixed_imag;
    // temp variable to store the osclaltion frequency
    float2 osci;


    // ------------- //
    // Shared memory //
    // ------------- //
    // Static shared memory array to store intermediate results. This array is warp_reduced to get a single output sample
    __shared__ float2 s_alpha[WARP_SIZE];
    // Holder for the dynamically allocated shared memory
    extern __shared__ char dyn_smem[];
    // The dynamic array containing the filter taps of the polyphase filter
    float* s_filter = (float*) (&dyn_smem[0]);
    // The dynamic array containing the input data - temporary
    float* s_input  = (float*) (&dyn_smem[ntaps_pad * 4]);
    // The dynamic array containing 32 x height output samples
    float2* s_odata = (float2*)(&dyn_smem[ntaps_pad * 8]);
    // The dyanamic array to store intermediate results -> gets warp reduced
    float2* s_inter = (float2*)(&dyn_smem[ntaps_pad * 8 + WARP_SIZE * height * 8]);

    // -------------------- //
    // Initial data loading //
    // -------------------- //
    if(tid < height)
    {
        float phase = -2.0 * f_lo[tid] * static_cast<float>(t0);
        s_alpha[tid].x = cospif(phase);
        s_alpha[tid].y = sinpif(phase);
    }
__syncthreads();
    // Load filter into shared memory - always padded to multiple of 32
    for(int i = tid; i < ntaps_pad; i+=blockDim.x)
    {
        s_filter[i] = filter[i];
    }
    // Load input into shared memory - always padded to multiple of 32
__syncthreads();
    int k;
    for(k = 0; k < ntaps_pad; k+=blockDim.x)
    {
        iidx = (offset * down + (half_fir + rotate * offset) % up + (k+tid) * up - half_fir) / up;
        if(iidx >= 0 && iidx < width)
        {
            s_input[k+tid] = idata[iidx];
        }
        else
        {
            s_input[k+tid] = 0;
        }
    }
__syncthreads(); // Ensure data are loaded
    iidx -= (k - blockDim.x); // Later used for pipelined loading of input data

    // --------------- //
    // Processing part //
    // --------------- //
    // Every warp computes a single output sample per iteration.
    // In total 32 output sample per mixing frequency and warp are calculated
#pragma unroll
    for(int oidx = 0; oidx < N_OSAMPLES && oidx + offset < o_width; oidx++)
    {
        // Compute the currently polyphase branch
        int poly_branch = (half_fir + rotate * (oidx + offset)) % up;
        // Iterate over mixing frequencies
        for(int i = 0; i < height; i++)
        {
__syncthreads();
            float2 tmp_alpha = s_alpha[i];
            tmp_real = 0;
            tmp_imag = 0;
            for(int ii = 0; ((ii + tid) < poly_branch_len_pad) && ((iidx + ii) < width) && ((iidx + ii) >= 0); ii+=blockDim.x)
            {
                osci = oscillations[i * width + iidx + ii];
                filtered = s_input[ii + tid] * s_filter[poly_branch * poly_branch_len_pad + ii + tid];
                mixed_real = osci.x * tmp_alpha.x;
                mixed_imag = osci.x * tmp_alpha.y;
                mixed_real = fmaf(osci.y, tmp_alpha.y*-1, mixed_real);
                mixed_imag = fmaf(osci.y, tmp_alpha.x, mixed_imag);
                tmp_real = fmaf(mixed_real, filtered, tmp_real);
                tmp_imag = fmaf(mixed_imag, filtered, tmp_imag);
            }
            // warp wide reduction
#pragma unroll
            for(int offset = 16; offset > 0; offset /=2)
            {
                tmp_real += __shfl_down_sync(0xffffffff, tmp_real, offset);
                tmp_imag += __shfl_down_sync(0xffffffff, tmp_imag, offset);
            }
            s_inter[tid] = {tmp_real, tmp_imag};
__syncthreads();
            // Reduce the block by using only the first thread
            if(tid == 0)
            {
                for(int ii = WARP_SIZE; ii < n_warp * WARP_SIZE; ii+=WARP_SIZE)
                {
                    tmp_real += s_inter[ii].x;
                    tmp_imag += s_inter[ii].y;
                }
                s_odata[i * N_OSAMPLES + oidx] = {tmp_real, tmp_imag};
            }
        }

        // ---------------------- //
        // Pipelined data loading //
        // ---------------------- //
        // Shift input data and load next input data
        // Compute the shift range.
        if(poly_branch < comp)
        {
            // If down / up has decimal points it is compensated here
            shift = ratio_int + 1;
        }
        else
        {
            shift = ratio_int;
        }
        // Shift the data in the shared memory by value of shift
        for(int i = 0; i < ntaps_pad - shift; i+=blockDim.x)
        {
            // If thread is used for the shift, first load the sample into register
            int hidx = i + tid;
            if(hidx < (ntaps_pad - shift))
            {
                tmpf = s_input[hidx + shift];
            }
__syncthreads();
            // If the threads are synced -> all participating threads have loaded the sample into register
            // write the sample back to the corresponding location in the shared memory
            if(hidx < (ntaps_pad - shift))
            {
                s_input[hidx] = tmpf;
            }
__syncthreads();
        }

        // Load new input data
        for(int i = 0; i < shift; i+=blockDim.x)
        {
            int s_iidx = ntaps_pad - shift + i + tid;
            int glob_iidx = iidx + i + ntaps_pad;
            if((glob_iidx >= 0) && (glob_iidx < width) && (s_iidx < ntaps_pad))
            {
                s_input[s_iidx] = idata[glob_iidx];
            }
        }
        iidx += shift; // Increment the input index for all threads
__syncthreads(); // Ensure data are loaded
    }
    // ------------------- //
    // Offload output data //
    // ------------------- //

    if(ooidx < o_width && tid < N_OSAMPLES)
    {
#pragma unroll
        for(int i = 0; i < height; i++)
        {
            odata[o_width * i + ooidx] = s_odata[i * N_OSAMPLES + tid];
        }
    }
}


} // namespace kernel

__host__ __device__ float2 cmult(float2 a, float2 b, float c){
    float2 res;
    res.x = (a.x * b.x - a.y * b.y) * c;
    res.y = (a.x * b.y + a.y * b.x) * c;
    return res;
}
void convolve(
    const thrust::device_vector<float>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float>& output,
    std::size_t n_signals,
    cudaStream_t stream)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = filter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    kernel::convolve<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(filter.data()),
        thrust::raw_pointer_cast(output.data()),
        filter_length, signal_length
    );
}


void convolve(
    const thrust::host_vector<float>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float>& output,
    std::size_t n_signals)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = lfilter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    for (std::size_t n = 0; n < n_signals; n++){
        for (std::size_t oidx = 0; oidx < signal_length; oidx++){
            output[n * signal_length + oidx] = 0;
            for(std::size_t fidx = 0; fidx < lfilter.size(); fidx++){
                int iidx = oidx + fidx - (lfilter.size()-1) / 2;
                if(iidx >= 0 && iidx < (int)signal_length){
                    output[n * signal_length + oidx] += input[n * signal_length + iidx] * lfilter[fidx];
                }
            }
        }
    }
}


void convolve(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float2>& output,
    std::size_t n_signals,
    cudaStream_t stream)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = filter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    kernel::convolve<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(filter.data()),
        thrust::raw_pointer_cast(output.data()),
        filter.size(), signal_length
    );
}


void convolve(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    std::size_t n_signals)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = lfilter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    for (std::size_t n = 0; n < n_signals; n++){
        for (std::size_t oidx = 0; oidx < signal_length; oidx++){
            output[n * signal_length + oidx] = {0,0};
            for(std::size_t fidx = 0; fidx < lfilter.size(); fidx++){
                int iidx = oidx + fidx - (lfilter.size()-1) / 2;
                if(iidx >= 0 && iidx < (int)(signal_length)){
                    output[n * signal_length + oidx].x += input[n * signal_length + iidx].x * lfilter[fidx];
                    output[n * signal_length + oidx].y += input[n * signal_length + iidx].y * lfilter[fidx];
                }
            }
        }
    }
}


void convolve_hilbert(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& bfilter,
    thrust::device_vector<float>& output,
    std::size_t n_signals,
    cudaStream_t stream)
{
    assert((input.size() / n_signals) % 1 == 0);
    // assert(input.size() * 2 == output.size());

    std::size_t signal_length = input.size() / n_signals;
    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    unsigned s_memory = bfilter.size() * sizeof(float);

    kernel::convolve_hilbert<<<grid, 1024, s_memory, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(bfilter.data()),
        thrust::raw_pointer_cast(output.data()),
        bfilter.size(), signal_length
    );
}


void convolve_hilbert(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& bfilter,
    thrust::host_vector<float>& output,
    std::size_t n_signals)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = bfilter.size();

    assert(signal_length >= filter_length);
    assert(input.size() * 2 == output.size());

    for(std::size_t n = 0; n < n_signals; n++){
        for (std::size_t i = 0; i < signal_length; i++){
            float2 tmp = {.0,.0};
            for (std::size_t j = 0; j < filter_length; j++){
                int iidx = i - j + (filter_length - 1) / 2;
                if (iidx >= 0 && iidx < (int)(signal_length)){
                    tmp.x += input[n * signal_length + iidx].x * bfilter[j];
                    tmp.y += input[n * signal_length + iidx].y * bfilter[filter_length - 1 - j];
                }
            }
            output[n * signal_length * 2 + i] = tmp.x + tmp.y;
            output[n * signal_length * 2 + i + signal_length] = tmp.x - tmp.y;
        }
    }
}


void poly_resample(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal)
{
    assert(output.size() == input.size() * up / down);
    std::size_t i_length =  input.size() / n_signal;
    std::size_t o_length = output.size() / n_signal;
    std::size_t half_fir = (lfilter.size() - 1) / 2;
    std::size_t rotate = up - down + up * down;
    for(std::size_t n = 0; n < n_signal; n++)
    {
        std::size_t o_offset = n * o_length;
        std::size_t i_offset = n * i_length;
        for(std::size_t oidx = 0; oidx < o_length; oidx++)
        {
            std::size_t poly_branch = (half_fir + rotate * oidx) % up;
            for(std::size_t fidx = poly_branch; fidx < lfilter.size(); fidx += up)
            {
                int iidx = (oidx * down + fidx - half_fir) / up;
                if (iidx >= 0 && iidx < (int)i_length){
                    output[o_offset + oidx].x += input[i_offset + iidx].x * lfilter[fidx];
                    output[o_offset + oidx].y += input[i_offset + iidx].y * lfilter[fidx];
                }
            }
        }
    }
}


void poly_resample(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& lfilter,
    thrust::device_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal,
    cudaStream_t stream)
{
    dim3 grid;
    if(output.size() > (std::size_t)(N_SM / n_signal * N_THREADS)){
        grid.x = N_SM / n_signal;
    }else{
        grid.x = output.size() / N_THREADS + 1;
    }
    grid.y = n_signal;
    unsigned s_memory = lfilter.size() * sizeof(float);
    kernel::poly_resample<<<grid, N_THREADS, s_memory, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(lfilter.data()),
        thrust::raw_pointer_cast(output.data()),
        up, down, input.size() / n_signal, lfilter.size()
    );
}


void mix_poly_resample(
    const thrust::device_vector<float>& input,
    const thrust::device_vector<float>& lfilter,
    const thrust::device_vector<float>& f_lo,
    const thrust::device_vector<float2>& oscillations,
    thrust::device_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t ntaps,
    double t0,
    cudaStream_t stream)
{
    int ntaps_pad = lfilter.size();
    int half_fir = (ntaps - 1) / 2;
    int height = f_lo.size();
    int nwarps = (ntaps_pad / (up * WARP_SIZE) < N_WARPS) ?
        ntaps_pad / (up * WARP_SIZE) : N_WARPS;
    unsigned s_memory =
        ntaps_pad * 2 * sizeof(float) // Input and filter data
        + WARP_SIZE * height * sizeof(float2) // Output data
        + WARP_SIZE * nwarps * sizeof(float2);// Store intermediate results
    dim3 grid, block;
    grid.x = ceil(output.size() / static_cast<float>(WARP_SIZE*height));
    block.x = WARP_SIZE * nwarps;

    assert(ntaps_pad % WARP_SIZE == 0); // filter size is not aligned to WARP_SIZE (=32)
    assert((s_memory + WARP_SIZE * sizeof(float2)) < MAX_SMEM_SIZE); // Not enough requested shared memory
    kernel::mix_poly_resample<<<grid, block, s_memory, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(lfilter.data()),
        thrust::raw_pointer_cast(f_lo.data()),
        thrust::raw_pointer_cast(oscillations.data()),
        thrust::raw_pointer_cast(output.data()),
        up, down, input.size(), height, half_fir, ntaps_pad, t0);
}


void mat_vec_mult(
    thrust::device_vector<float2>& matrix,
    thrust::device_vector<float>& vector,
    thrust::device_vector<float2>& output,
    thrust::device_vector<float2>& alpha,
    cudaStream_t stream)
{
    assert(matrix.size() % vector.size() == 0);
    assert(output.size() == matrix.size());
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    dim3 grid(cols / 1024 + 1, 1, 1);
    kernel::mat_vec_mult<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(matrix.data()),
        thrust::raw_pointer_cast(vector.data()),
        thrust::raw_pointer_cast(output.data()),
        thrust::raw_pointer_cast(alpha.data()), cols, rows
    );
}


void mat_vec_mult(
    thrust::host_vector<float2>& matrix,
    thrust::host_vector<float>& vector,
    thrust::host_vector<float2>& output,
    thrust::host_vector<float2>& alpha)
{
    assert(matrix.size() % vector.size() == 0);
    assert(output.size() == matrix.size());
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    assert(rows == alpha.size());
    for(std::size_t i = 0; i < rows; i++){
        for(std::size_t ii = 0; ii < cols; ii++){
            output[i * cols + ii] = cmult(alpha[i], matrix[i * cols + ii], vector[ii]);
        }
    }
}


void mat_vec_mult(
    thrust::host_vector<float2>& matrix,
    thrust::host_vector<float>& vector)
{
    assert(matrix.size() % vector.size() == 0);
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    for(std::size_t i = 0; i < rows; i++){
        for(std::size_t ii = 0; ii < cols; ii++){
            matrix[i * cols + ii].x = vector[ii] * matrix[i * cols + ii].x;
            matrix[i * cols + ii].y = vector[ii] * matrix[i * cols + ii].y;
        }
    }
}



}
}
}