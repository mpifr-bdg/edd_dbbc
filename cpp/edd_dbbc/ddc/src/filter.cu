#include "edd_dbbc/ddc/filter.h"
namespace edd_dbbc {
namespace ddc {
namespace filter {
namespace kernel {

const float ROOT2 = 1.414213562f;

__global__ void firhilbert(
    float* data,
    int length,
    float a,
    float w1,
    float w2)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    float t;
    if(tid < length){
        t = 2 * M_PI * (tid - (length - 1) / 2);
        if(t == 0){
            data[tid] = ROOT2 * (w2 - w1);
        }else if(t == M_PI/2*a){
            data[tid] = a * (sinf(M_PI/4 * ( (a+2*w2) / a)) - sinf( M_PI/4 *( (a+2*w1) / a )));
        }else if(t == -M_PI/2*a){
            data[tid] = a * (sinf(M_PI/4 * ( (a-2*w1) / a)) - sinf( M_PI/4 *( (a-2*w2) / a )));
        }else{
            data[tid] = 2 * M_PI * M_PI * cosf(a * t)
                / (t * (4 * a * a * t * t - M_PI * M_PI))
                * (sinf(w1 * t + M_PI / 4)
                - sinf(w2 * t + M_PI / 4));
        }
    }
}

} // namespace kernel


void firhilbert(
    thrust::device_vector<float>& data,
    float a,
    float w1,
    float w2)
{
    std::size_t length = data.size();
    kernel::firhilbert<<<1024 / length + 1, 1024, 0>>>(
        thrust::raw_pointer_cast(data.data()),
        length, a, w1, w2
    );

}


void firhilbert(
    thrust::host_vector<float>& data,
    float a,
    float w1,
    float w2)
{
    float t;
    int length = (int)data.size();
    for(int i = 0; i < length; i++)
    {
        // Calculate t
        t = 2 * M_PI * (i - (int)(length - 1) / 2);
        if(t == 0){
            data[i] = sqrt(2) * (w2 - w1);
        }else if(t == M_PI/2*a){
            data[i] = a * (sin(M_PI/4 * ( (a+2*w2) / a)) - sin( M_PI/4 *( (a+2*w1) / a )));
        }else if(t == -M_PI/2*a){
            data[i] = a * (sin(M_PI/4 * ( (a-2*w1) / a)) - sin( M_PI/4 *( (a-2*w2) / a )));
        }else{
            data[i] = 2 * M_PI * M_PI * cos(a * t)
                / (t * (4 * a * a * t * t - M_PI * M_PI))
                * (sin(w1 * t + M_PI / 4)
                - sin(w2 * t + M_PI / 4));
        }
    }
}


void reshape_poly_fir(
    thrust::host_vector<float>& fir,
    const std::size_t up,
    const std::size_t down,
    const std::size_t pad)
{
    std::size_t branch_len = ceil(fir.size() / (float)up);
    std::size_t pad_len = (branch_len / pad + 1) * pad;
    std::vector<std::vector<float>> tmp(up, std::vector<float>(pad_len, 0));
    for (int i = 0; i < fir.size(); ++i)
    {
        int r = i % up; // Calculate row index
        int c = i / up; // Calculate column index
        tmp[r][c] = fir[i];
    }
    fir.resize(up * pad_len);
    for(int r = 0; r < up; r++)
    {
        for(int c = 0; c < pad_len; c++)
        {
            fir[r * pad_len + c] = tmp[r][c];
        }
    }
}


void reshape_poly_fir(
    thrust::device_vector<float>& fir,
    const std::size_t up,
    const std::size_t down,
    const std::size_t pad)
{
    thrust::host_vector<float> tmp = fir;
    reshape_poly_fir(tmp, up, down, pad);
    fir.resize(tmp.size());
    thrust::copy(tmp.begin(), tmp.end(), fir.begin());
}


}
}
}