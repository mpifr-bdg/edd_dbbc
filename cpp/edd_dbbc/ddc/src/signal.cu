#include "edd_dbbc/ddc/signal.cuh"

namespace edd_dbbc {
namespace ddc {
namespace signal {
namespace kernel {

__global__ void chirp(
    float* data,
    float fs,
    float f0,
    float f1,
    float gain,
    float t1,
    int width)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    float duration = width / fs;
    float beta = (f1 - f0) / duration, phase, t;
    if(tid < width)
    {
        t = tid / fs;
        phase = 2 * M_PI * (f0 * t + 0.5 * beta * t * t);
        data[tid] = gain * cosf(phase);
    }
}


__global__ void phase(
    float2* phases,
    float* freq,
    std::size_t fs,
    std::size_t nsamples,
    double precision)
{
    int tidx = threadIdx.x;
    double phase = -2.0 * M_PI * freq[tidx] * nsamples / static_cast<double>(fs);
    phases[tidx].x = cos(phase);
    phases[tidx].y = sin(phase);
    if(phases[tidx].x >= -precision && phases[tidx].x <= precision){
        phases[tidx].x = 0;
    }
    if(phases[tidx].y >= -precision && phases[tidx].y <= precision){
        phases[tidx].y = 0;
    }
}


} // namespace kernel


void chirp(
    thrust::device_vector<float>& data,
    float fs,
    float f0,
    float f1,
    float gain,
    float t1)
{
    std::size_t width = data.size();
    if(t1 < 0){t1 = width / fs;}
    kernel::chirp<<<width / 1024 + 1, 1024>>>(
        thrust::raw_pointer_cast(data.data()),
        fs, f0, f1, gain, t1, width
    );
}


void chirp(
    thrust::host_vector<float>& data,
    float fs,
    float f0,
    float f1,
    float gain,
    float t1)
{
    float duration = data.size() / fs;
    float beta = (f1 - f0) / duration;
    float phase, t;
    if(t1 < 0){t1=duration;}
    for(std::size_t i = 0; i < data.size(); i++)
    {
        t = i / fs;
        phase = 2 * M_PI * (f0 * t + 0.5 * beta * t * t);
        data[i] = gain * cos(phase);
    }
}


void chirp(
    thrust::host_vector<float2>& data,
    float fs,
    float f0,
    float f1,
    float gain,
    float t1)
{
    float duration = data.size() / fs;
    float beta = (f1 - f0) / duration;
    float phase, t;
    if(t1 < 0){t1=duration;}
    for(std::size_t i = 0; i < data.size(); i++)
    {
        t = i / fs;
        phase = 2 * M_PI * (f0 * t + 0.5 * beta * t * t);
        data[i].x = gain * cos(phase);
        data[i].y = gain * sin(phase);
    }
}


void phase(
    thrust::device_vector<float2>& phases,
    thrust::device_vector<float>& freq,
    std::size_t fs,
    std::size_t nsamples,
    cudaStream_t stream)
{
    kernel::phase<<<1, phases.size(), 0, stream>>>(
        thrust::raw_pointer_cast(phases.data()),
        thrust::raw_pointer_cast(freq.data()),
        fs, nsamples
    );
}


void phase(
    thrust::host_vector<float2>& phases,
    thrust::host_vector<float>& freq,
    std::size_t fs,
    std::size_t nsamples)
{
    for(std::size_t i = 0; i < freq.size(); i++){
        phases[i].x = cos(-2 * M_PI * freq[i] * nsamples / fs);
        phases[i].y = sin(-2 * M_PI * freq[i] * nsamples / fs);
    }
}

}
}
}