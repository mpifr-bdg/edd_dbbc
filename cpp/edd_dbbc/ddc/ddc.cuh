#pragma once

#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cuComplex.h>
#include <cufft.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <psrdada_cpp/common.hpp>

#include <cmath>

#include <psrdada_cpp/cuda_utils.hpp>
#include "edd_dbbc/ddc/filter.h"
#include "edd_dbbc/ddc/signal.cuh"
#include "edd_dbbc/ddc/processing.h"

namespace edd_dbbc {
namespace ddc {


const unsigned WARP_SIZE = 32;
const unsigned N_WARPS = 32;


/**
 * @brief The configruation for a DDC object
 *
 */
struct ddc_t
{
    // Input sample frequency
    std::size_t input_size = 32000;
    // Input sample frequency
    std::size_t fs_in = 32000;
    // Downsampling / output frequency of the sidebands
    std::size_t fs_dw = 4000;
    // Frequency of local oscillator, for multiple seperate by comma
    std::string f_lo = "8000";
    // If set to true method 'pad_size()' returns the pad size to allow streaming - No effect on the DDC object itsel -> propably go into another struct
    bool padding = false;
    // The hilbert bandpass configuration
    filter::hilbert_t hilbert;
    // The lowpass configuration
    filter::lowpass_t lowpass;

    /**
     * @brief Returns the local oscillationf requency in a host_vector. Splits the string 'f_lo' by comma
     *
     * @return thrust::host_vector<float>
     */
    thrust::host_vector<float> lo_list()
    {
        thrust::host_vector<float> lo_vec(0);
        std::string tmp = this->f_lo + ","; std::size_t pos =0;
        while((pos = tmp.find(',')) != std::string::npos){
            lo_vec.push_back(std::stof(tmp.substr(0, pos)));
            tmp.erase(0, pos+1);
        }
        return lo_vec;
    }
    /**
     * @brief Get the up-factor of rational resampling
     *
     * @return std::size_t
     */
    std::size_t up()
    {
        return std::lcm(fs_in, fs_dw) / fs_in;
    }
    /**
     * @brief Get the down-factor of rational resampling
     *
     * @return std::size_t
     */
    std::size_t down()
    {
        return std::lcm(fs_in, fs_dw) / fs_dw;
    }
    /**
     * @brief Get the number of signals -> len(lo_list)
     *
     * @return std::size_t
     */
    std::size_t nsignals()
    {
        return this->lo_list().size();
    }
    /**
     * @brief Get the pad size required to get a continous stream
     *
     * @return std::size_t
     */
    std::size_t pad_size()
    {
        return (padding) ? hilbert.ntaps * down() / up() : 0;
    }

    std::size_t output_size()
    {
        return input_size * up() / down() * nsignals() * 2;
    }

};


/**
 * @brief The DDC class implments the conventional and simplest approach of a rational resampler
 *          It is the standard reference, but the slowest implementation.
 *          Depending on the passed vector types the processing runs on the CPU or GPU
 *          (e.g. thrust::host_vector to run on the CPU, thrust::device_vector to run on the GPU)
 *
 * @tparam RealVectorType
 * @tparam CplxVectorType
 */
template<typename RealVectorType, typename CplxVectorType>
class DDC
{
public:
    typedef typename RealVectorType::value_type real;
    typedef typename CplxVectorType::value_type cplx;
public:
    /**
     * @brief Construct a new DDC object
     *
     * @param conf the ddc configruation
     */
    DDC(ddc_t& conf);

    /**
     * @brief Destroy the DDC object
     *
     */
    ~DDC();

    /**
     * @brief Allocates the necessary memory for the processing,
     *        also generates filter coefficents
     *
     */
    virtual void allocate();

    /**
     * @brief Processes idata and writes the processed data to the odata vector
     *
     * @param idata input
     * @param odata output
     */
    void process(RealVectorType& idata, RealVectorType& odata);

    /**
     * @brief Set the config object
     *
     * @param conf
     */
    void set_config(ddc_t& conf);
    /**
     * @brief Get the config object
     *
     * @return ddc_t
     */
    ddc_t config(){return _conf;}
    /**
     * @brief get the upsampling-factor
     *
     * @return std::size_t
     */
    std::size_t up(){return _up;}
    /**
     * @brief get the downsampling-factor
     *
     * @return std::size_t
     */
    std::size_t down(){return _down;}
    /**
     * @brief Get the number of signals generated
     *
     * @return std::size_t
     */
    std::size_t nsignals(){return local_osci.size();}
    /**
     * @brief Get the coefficents of the lowpass
     *
     * @return RealVectorType&
     */
    RealVectorType& lowpass(){return lowpass_fir;}
    /**
     * @brief Get the coefficents of the lowpass
     *
     * @return RealVectorType&
     */
    filter::lowpass_t& lowpass_conf(){return _conf.lowpass;}

protected:
    void _init();
protected:
    ddc_t _conf;
    std::size_t _fs_up;
    std::size_t _up;
    std::size_t _down;
    std::size_t _lcm;
    std::size_t _max_rate;
    std::size_t _call_count = 0;

    double _buffer_duration;
    double _pad_duration;

    RealVectorType lowpass_fir;
    RealVectorType hilbert_fir;
    RealVectorType local_osci;
    CplxVectorType alpha;
    CplxVectorType lo_buffer;
    CplxVectorType mx_buffer;
    CplxVectorType up_buffer;
    CplxVectorType fl_buffer;
    CplxVectorType rs_buffer;
};


/**
 * @brief The PolyphaseDownConverter class implments a rational resampler using a polyphase resampler.
 *          It allows arbitary resampling and is a efficient implementation
 *          Depending on the passed vector types the processing runs on the CPU or GPU
 *          (e.g. thrust::host_vector to run on the CPU, thrust::device_vector to run on the GPU)
 *
 * @tparam RealVectorType
 * @tparam CplxVectorType
 */
template<typename RealVectorType, typename CplxVectorType>
class PolyphaseDownConverter : public DDC<RealVectorType, CplxVectorType>
{
public:
    typedef typename RealVectorType::value_type real;
    typedef typename CplxVectorType::value_type cplx;
    typedef RealVectorType ivector_type;
    typedef CplxVectorType ovector_type;
public:
    /**
     * @brief Construct a new Polyphase Down Converter object
     *
     * @param conf
     * @param device_id
     */
    PolyphaseDownConverter(ddc_t& conf, std::size_t device_id=0);

    /**
     * @brief Destroy the Polyphase Down Converter object
     *
     */
    ~PolyphaseDownConverter();

    /**
     * @brief
     *
     */
    void allocate() override;

    /**
     * @brief
     *
     * @param input
     * @param output
     */
    void process(RealVectorType& input, RealVectorType& output);

    /**
     * @brief Process input data with the polyphase resample algorithm. Only meant GPU processing with a streaming.
     *
     * @param input
     * @param output
     * @param stream
     */
    void process(
        thrust::device_vector<PolyphaseDownConverter<RealVectorType, CplxVectorType>::real>& input,
        thrust::device_vector<PolyphaseDownConverter<RealVectorType, CplxVectorType>::real>& output,
        cudaStream_t& stream
    );

    std::size_t total_memory()
    {
        return this->_conf.lowpass.ntaps * sizeof(real)
            + this->_conf.lowpass.ntaps * sizeof(cplx)
            + this->_conf.input_size * this->local_osci.size() * sizeof(cplx) * 2
            + this->_conf.input_size * this->local_osci.size() * this->_up / this->_down;
    }

private:

    cudaDeviceProp _cu_prop;
    std::size_t _device_id;
};

template<typename RealVectorType, typename CplxVectorType>
class FourierDownConverter : public DDC<RealVectorType, CplxVectorType>
{
public:
  typedef typename RealVectorType::value_type real;
  typedef typename CplxVectorType::value_type cplx;
public:
  FourierDownConverter(ddc_t& conf, std::size_t device_id=0);
  ~FourierDownConverter();
  void process(RealVectorType idata, RealVectorType odata);

private:
  void transform_lowpass();
  cudaDeviceProp _cu_prop;
  // cublasHandle_t cublas_handle_mx;
  cublasHandle_t cublas_handle_lp;
  // cublasHandle_t cublas_handle_bp;
  cufftHandle cufft_plan_lp;
  cufftHandle cuifft_plan_lp;
  cufftHandle cufft_plan_hb;

  std::size_t _buffer_size;
  std::size_t _device_id;
  CplxVectorType bp_buffer;
  CplxVectorType d_lowpass_fir;
  CplxVectorType d_hilbert_fir;

  float2 alpha = {1,0};
  float2 beta = {0,0};
  dim3 _grid;
  dim3 _block;
};



}
}

#include "edd_dbbc/ddc/detail/ddc.cu"