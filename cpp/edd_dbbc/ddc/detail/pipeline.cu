#pragma once

namespace edd_dbbc{
namespace ddc {


template <typename HandlerType, typename InputType>
Pipeline<HandlerType, InputType>::Pipeline(
    const pipe_config& config,
    HandlerType &handler)
  : _config(config), _handler(handler), _call_count(0)
{
    _config.ddc.padding = true;
    _config.ddc.input_size = _config.input_size()
        / (_config.nbit() / 8)
        + _config.ddc.pad_size() * 2; // times 2 because left and right padding
    // Check if the input buffer size matches the output buffer size
    std::size_t actual_osamples = _config.isamples()
        * _config.ddc.up()
        * _config.ddc.nsignals() * 2
        / _config.ddc.down();
    if(actual_osamples != _config.osamples())
    {

        BOOST_LOG_TRIVIAL(error) << "Given buffer size does not match, expected" 
            << actual_osamples << ", got " << _config.osamples(); 
        assert(actual_osamples == _config.osamples()); // Input size does not match with output size
    }

    // Create the CUDA streams
    CUDA_ERROR_CHECK(cudaStreamCreate(&host2dev_stream));
    CUDA_ERROR_CHECK(cudaStreamCreate(&processing_stream));
    CUDA_ERROR_CHECK(cudaStreamCreate(&dev2host_stream));

    // Dynamically instantiate objects
    _unpacker.reset(new psrdada_cpp::Unpacker(processing_stream));
    _processor.reset(new PolyphaseDownConverter<thrust::device_vector<float>,
        thrust::device_vector<float2>>(_config.ddc));

    std::size_t total_mem = _processor->total_memory()
        + _config.input_size() * 2
        + _config.ddc.input_size * 2
        + _config.ddc.input_size * _config.ddc.up() * _config.ddc.nsignals() * 2 / _config.ddc.down() * 2
        + _config.ddc.hilbert.ntaps * 2;
    BOOST_LOG_TRIVIAL(info) << "Total required memory size " << total_mem / (1000*1000) << "MB";
    // Allocate buffers
    _processor->allocate();
    input_buffer.resize(_config.input_size() / sizeof(InputType), 0);
    unpacked_buffer.resize(_config.ddc.input_size, 0);
    resampled_buffer.resize(_config.ddc.input_size
        * _config.ddc.up()
        * _config.ddc.nsignals() * 2
        / _config.ddc.down()
        + _config.ddc.hilbert.ntaps, 0);

    if(_config.batch_size() > 0){
      assert((_config.osamples() / (_config.ddc.nsignals() * 2)) % _config.batch_size() == 0); // Output buffer size is not a multiple of _config.batch_size()
    }
} // constructor



template <typename HandlerType, typename InputType>
Pipeline<HandlerType, InputType>::~Pipeline()
{
  BOOST_LOG_TRIVIAL(debug) << "Destroying Pipeline";
  CUDA_ERROR_CHECK(cudaStreamDestroy(host2dev_stream));
  CUDA_ERROR_CHECK(cudaStreamDestroy(processing_stream));
  CUDA_ERROR_CHECK(cudaStreamDestroy(dev2host_stream));
}


template <typename HandlerType, typename InputType>
void Pipeline<HandlerType, InputType>::init(psrdada_cpp::RawBytes &block)
{
  std::size_t sample_clock_start = 0; double sync_time = 0;
  if (ascii_header_get(block.ptr(), "SAMPLE_CLOCK_START", "%ld", &sample_clock_start) != 1){
      BOOST_LOG_TRIVIAL(warning)
        << "No or multiple SAMPLE_CLOCK_START in header stream! Not setting reference time";
      return;
  }
  if (ascii_header_get(block.ptr(), "SYNC_TIME", "%ld", &sync_time) != 1){
      BOOST_LOG_TRIVIAL(warning)
        << "No or multiple SYNC_TIME in header stream!";
  }
  double frac_scs = sample_clock_start * _config.ddc.up() /  (double)_config.ddc.down();
  if(tools::is_fractional(frac_scs)){
      BOOST_LOG_TRIVIAL(error) << "The down converted SAMPLE_CLOCK_START is fractional, exiting";
      throw std::runtime_error("The down converted SAMPLE_CLOCK_START is fractional, exiting");
  }
  BOOST_LOG_TRIVIAL(info) << "Timestamp before downsampling " << sync_time + sample_clock_start / static_cast<double>(_config.ddc.fs_in);
  BOOST_LOG_TRIVIAL(info) << "SAMPLE_CLOCK_START before downsampling " << sample_clock_start;
  std::size_t nsamples_out = _config.output_size() / (sizeof(float) * _config.ddc.nsignals() * 2);
  sample_clock_start = sample_clock_start * _config.ddc.up() / _config.ddc.down();
  ascii_header_set(block.ptr(), "SAMPLE_CLOCK_START", "%lu", sample_clock_start);
  ascii_header_set(block.ptr(), "SAMPLE_CLOCK", "%lu", _config.ddc.fs_dw);
  BOOST_LOG_TRIVIAL(info) << "Timestamp after downsampling " << sync_time + sample_clock_start / _config.ddc.fs_dw;
  BOOST_LOG_TRIVIAL(info) << "SAMPLE_CLOCK_START after downsampling " << sample_clock_start;
  _handler.init(block);
}


template <typename HandlerType, typename InputType>
bool Pipeline<HandlerType, InputType>::operator()(psrdada_cpp::RawBytes &block)
{
    ++_call_count;
    BOOST_LOG_TRIVIAL(debug) << "Pipeline operator() called " << _call_count;
    /* Unexpected buffer size */
    if (block.used_bytes() != _config.input_size())
    {
        BOOST_LOG_TRIVIAL(error)
            << "Unexpected Buffer Size - Got "
            << block.used_bytes() << " byte, expected "
            << _config.input_size() << " byte)";
        CUDA_ERROR_CHECK(cudaDeviceSynchronize());
        return true;
    }
    BOOST_LOG_TRIVIAL(debug)
        << "Processing " << block.used_bytes() << "/" << _config.input_size() << " bytes \n";

    CUDA_ERROR_CHECK(cudaStreamSynchronize(host2dev_stream));
    CUDA_ERROR_CHECK(cudaStreamSynchronize(processing_stream));
    CUDA_ERROR_CHECK(cudaStreamSynchronize(dev2host_stream));

    input_buffer.swap();
    // Copy new data from host to device
    CUDA_ERROR_CHECK(cudaMemcpyAsync(
      static_cast<void *>(input_buffer.a_ptr()),
      static_cast<void *>(block.ptr()),
      _config.input_size(), cudaMemcpyHostToDevice, host2dev_stream));

    if (_call_count == 1) {return false;}

    unpacked_buffer.swap();
    // unpack buffer if it is an integer, otherwise unpacking is not required
    if constexpr (std::is_same<uint64_t, InputType>::value){
        if(_config.nbit() == 8){
            _unpacker->unpack<8>(input_buffer.b_ptr(),
                unpacked_buffer.a_ptr() + _config.ddc.pad_size(),
                input_buffer.size());
        }else if(_config.nbit() == 10){
            _unpacker->unpack<10>(input_buffer.b_ptr(),
                unpacked_buffer.a_ptr() + _config.ddc.pad_size(),
                input_buffer.size());
        }else if(_config.nbit() == 12){
            _unpacker->unpack<12>(input_buffer.b_ptr(),
                unpacked_buffer.a_ptr() + _config.ddc.pad_size(),
                input_buffer.size());
        }else{
          throw std::runtime_error("Unsupported bit depth with template typename uint64_t");
        }
    // If type is float, no unpacking is required - direct copy
    }else if constexpr (std::is_same<float, InputType>::value){
        CUDA_ERROR_CHECK(cudaMemcpyAsync(
            static_cast<void *>(unpacked_buffer.a_ptr() + _config.ddc.pad_size()),
            static_cast<void *>(input_buffer.b_ptr()),
            _config.input_size(), cudaMemcpyDeviceToDevice, processing_stream));
    }
    // Copy padding samples from the latest to the previous buffer
    CUDA_ERROR_CHECK(cudaMemcpyAsync(
        static_cast<void *>(unpacked_buffer.b_ptr() + unpacked_buffer.size() - _config.ddc.pad_size()),
        static_cast<void *>(unpacked_buffer.a_ptr() + _config.ddc.pad_size()),
        _config.ddc.pad_size() * sizeof(float), cudaMemcpyDeviceToDevice, processing_stream)
    );
    // Copy padding samples from the previous to the latest buffer
    CUDA_ERROR_CHECK(cudaMemcpyAsync(
        static_cast<void *>(unpacked_buffer.a_ptr()),
        static_cast<void *>(unpacked_buffer.b_ptr() + unpacked_buffer.size() - _config.ddc.pad_size() * 2),
        _config.ddc.pad_size() * sizeof(float), cudaMemcpyDeviceToDevice, processing_stream)
    );

    if (_call_count == 2) {return false;}

    // resample the unpacked buffer
    resampled_buffer.swap();
    _processor->process(unpacked_buffer.b(), resampled_buffer.a(), processing_stream);

    if (_call_count == 3) {return false;}

    psrdada_cpp::RawBytes bytes(
        reinterpret_cast<char *>(resampled_buffer.b_ptr()),
        _config.output_size(), _config.output_size());
    io::CopyEngine<float> copy_engine(_config, dev2host_stream, io::copy_ddc_output<float>);

    _handler(bytes, copy_engine);

    return false;
}

}
}

