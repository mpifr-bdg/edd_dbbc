#pragma once 

namespace edd_dbbc {
namespace ddc {
namespace filter {
namespace kernel {

template<typename T>
__global__ void firwin(
    T* data,
    T* window,
    std::size_t length,
    float f_cutoff)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    if(tid < length){
        data[tid] = f_cutoff * signal::sinc(((T)tid-length/2)*f_cutoff) * window[tid];
    }
}

template<typename T>
__global__ void window_hamming(T* idata, int width)
{
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	if (tidx < width)
	{
		idata[tidx] = 0.54 - 0.46 * cosf(2*tidx*M_PI / (width - 1));
	}
}

template<typename T>
__global__ void window_hann(T* idata, int width)
{
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	if (tidx < width)
	{
		idata[tidx] = 0.5*(1 + cosf(2*tidx*M_PI / (width - 1)));
	}
}

template<typename T>
__global__ void window_bartlett(T* idata, int width)
{
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	if (tidx < width)
	{
		idata[tidx] = 0;
	}
}

template<typename T>
__global__ void window_blackman(T* idata, int width)
{
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	if (tidx < width)
	{
		idata[tidx] = 0.74 / 2 * -0.5 * cosf(2 * M_PI*tidx / (width - 1)) + 0.16 / 2 * sinf(4 * M_PI*tidx / (width - 1));
	}
}

} // namespace kernel

template<typename VectorType>
void firhilbert(VectorType& data, hilbert_t conf)
{
    firhilbert(data, conf.a, conf.w1, conf.w2);
}


template<typename T>
void window(
    thrust::device_vector<T>& data,
    std::string window_name)
{
    std::size_t length = data.size();
    if(window_name == "hamming"){
        kernel::window_hamming<<<length / 1024 + 1, 1024>>>(
            thrust::raw_pointer_cast(data.data()), length
        );
    }
    else if(window_name == "hann"){
        kernel::window_hann<<<length / 1024 + 1, 1024>>>(
            thrust::raw_pointer_cast(data.data()), length
        );
    }
    else if(window_name == "hamming"){
        kernel::window_bartlett<<<length / 1024 + 1, 1024>>>(
            thrust::raw_pointer_cast(data.data()), length
        );
    }
    else if(window_name == "hamming"){
        kernel::window_blackman<<<length / 1024 + 1, 1024>>>(
            thrust::raw_pointer_cast(data.data()), length
        );
    }
    else{
        std::cout << "Window function " << window_name
            << " not implemented, using rectangle" << std::endl;
        thrust::fill(data.begin(), data.end(), 1);
    }
}


template<typename T>
void window(
    thrust::host_vector<T>& data,
    std::string window_name)
{
    std::size_t length = data.size();
    if(window_name == "hamming"){
        for(std::size_t i = 0; i < length; i++){
            data[i] = 0.54 - 0.46 * cos(2 * M_PI * i / (length - 1));
        }
    }else{
        std::cout << "Window function " << window_name
            << " not implemented, using rectangle" << std::endl;
        thrust::fill(data.begin(), data.end(), 1);
    }
}


template<typename T>
void firwin(
    thrust::device_vector<T>& data,
    float f_cutoff,
    std::string window)
{
    std::size_t length = data.size();
    thrust::device_vector<T> window_function(length);
    filter::window(window_function, window);
    kernel::firwin<<<length / 1024 + 1, 1024, 0>>>(
        thrust::raw_pointer_cast(data.data()),
        thrust::raw_pointer_cast(window_function.data()),
        length, f_cutoff
    );
}


template<typename T>
void firwin(
    thrust::host_vector<T>& data,
    float f_cutoff,
    std::string window)
{
    assert(f_cutoff >= 0 && f_cutoff <= 1);
    std::size_t length = data.size();
    thrust::host_vector<T>window_func(length);
    filter::window(window_func, window);
    for(std::size_t i = 0; i < length; i++)
    {
        data[i] = f_cutoff
            * signal::sinc(((T)i-length/2)*f_cutoff)
            * window_func[i];
    }
}


template<typename VectorType>
void firwin(
    VectorType& data,
    lowpass_t conf)
{
    firwin(data, conf.f_cutoff, conf.window);
}

}
}
}