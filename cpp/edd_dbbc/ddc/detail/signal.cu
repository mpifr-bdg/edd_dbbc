#pragma once

namespace edd_dbbc {
namespace ddc {
namespace signal {
namespace kernel {

template<typename T>
__global__ void oscillate(
    float2* data,
    float* lo,
    float fs,
    int width,
    int height,
    int offset)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    int tidy = blockIdx.y;
    double fs_1 = 1/static_cast<double>(fs);
    if(tidx < width && tidy < height)
    {
        T phase = -2.0 * lo[tidy] * ((tidx+offset) * fs_1);
        if constexpr(std::is_same<float, T>::value)
        {
            data[tidy * width + tidx].x = cospif(phase);
            data[tidy * width + tidx].y = sinpif(phase);
        }
        else
        {
            data[tidy * width + tidx].x = cospi(phase);
            data[tidy * width + tidx].y = sinpi(phase);
        }
    }
}

} // namespace kernel


__host__ __device__ float sinc(float x)
{
  return (x == 0) ? 1 : sin(M_PI * x) / (M_PI * x);
}


template<typename T>
void random(
    thrust::host_vector<T>& data,
    int mean,
    int sigma)
{
  std::default_random_engine engine;
  std::normal_distribution<double> dist(mean, sigma);
  for (std::size_t i = 0; i < data.size(); i++)
  {
    if constexpr (std::is_same<float2, T>::value){
      data[i].x = decltype(T::x)(dist(engine));
      data[i].y = decltype(T::y)(dist(engine));
    }else if (std::is_integral<T>::value){
      data[i] = (T)(dist(engine) + 1);
    }else{
      data[i] = (T)(dist(engine));
    }
  }
}


template<typename T>
void random(
    thrust::device_vector<T>& data,
    int mean,
    int sigma)
{
    thrust::host_vector<T> tmp(data.size());
    signal::random(tmp, mean, sigma);
    data = tmp;
}


template<typename T>
void oscillate(
    thrust::device_vector<float2>& data,
    thrust::device_vector<float>& f_lo,
    std::size_t fs,
    int offset)
{
    assert(data.size() % f_lo.size() == 0);
    std::size_t length = data.size() / f_lo.size();
    dim3 grid(length / 1024 + 1, f_lo.size(), 1);
    kernel::oscillate<T><<<grid, 1024>>>(
        thrust::raw_pointer_cast(data.data()),
        thrust::raw_pointer_cast(f_lo.data()),
        fs, length, f_lo.size(), offset
    );
}


template<typename T>
void oscillate(
    thrust::host_vector<float2>& data,
    thrust::host_vector<float>& f_lo,
    std::size_t fs,
    int offset)
{
    assert(data.size() % f_lo.size() == 0);
    std::size_t length = data.size() / f_lo.size();
    for(std::size_t i = 0; i < f_lo.size(); i++)
    {
        for(std::size_t ii = 0; ii < length; ii++)
        {
            T time = (ii+offset) / (T)fs;
            T phase = -2 * M_PI * f_lo[i] * time;
            data[i * length + ii].x = cos(phase);
            data[i * length + ii].y = sin(phase);
        }
    }
}


}
}
}