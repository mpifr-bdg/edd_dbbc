#pragma once

namespace edd_dbbc{
namespace ddc {

template<typename RealVectorType, typename CplxVectorType>
DDC<RealVectorType, CplxVectorType>::DDC(ddc_t& conf)
  : _conf(conf)
{
    this->_init();
}

template<typename RealVectorType, typename CplxVectorType>
DDC<RealVectorType, CplxVectorType>::~DDC()
{
    BOOST_LOG_TRIVIAL(info) << "Destroying DDC-object" << std::endl;
}

template<typename RealVectorType, typename CplxVectorType>
void DDC<RealVectorType, CplxVectorType>::_init()
{
    _up = _conf.up();
    _down = _conf.down();
    _max_rate = (_up > _down) ? _up : _down;
    _conf.lowpass.f_cutoff = 1.0 / _max_rate;
    _conf.lowpass.ntaps = _conf.lowpass.mult * _max_rate;
    if(_conf.lowpass.ntaps % 2 == 0){_conf.lowpass.ntaps -= 1;}
    if(_conf.lowpass.ntaps > 4095){_conf.lowpass.ntaps = 4095;}
    local_osci = _conf.lo_list();
    BOOST_LOG_TRIVIAL(info) << "DDC configuration: ntaps " << _conf.lowpass.ntaps
        << "\n\t- upsample:\t" << _up
        << "\n\t- downsample:\t" << _down
        << "\n\t- lowpass taps:\t" << _conf.lowpass.ntaps
        << "\n\t- bandpass taps:" << _conf.hilbert.ntaps
        << "\n\t- input samples:" << _conf.input_size;
}

template<typename RealVectorType, typename CplxVectorType>
void DDC<RealVectorType, CplxVectorType>::set_config(ddc_t& config)
{
    _conf = config;
    this->_init();
}

template<typename RealVectorType, typename CplxVectorType>
void DDC<RealVectorType, CplxVectorType>::allocate()
{
    BOOST_LOG_TRIVIAL(info) << "Allocating ressources for DDC processing" << std::endl;
    lowpass_fir.resize(_conf.lowpass.ntaps);
    hilbert_fir.resize(_conf.hilbert.ntaps);
    alpha.resize(local_osci.size());
    for(std::size_t i = 0; i < local_osci.size(); i++){
        alpha[i] = {1,0};
    }
    lo_buffer.resize(_conf.input_size * local_osci.size());
    mx_buffer.resize(_conf.input_size * local_osci.size());
    up_buffer.resize(_conf.input_size * local_osci.size() * _conf.up());
    fl_buffer.resize(_conf.input_size * local_osci.size() * _conf.up());
    rs_buffer.resize(_conf.input_size * local_osci.size() * _conf.up() / _conf.down());
    filter::firwin<RealVectorType>(lowpass_fir, _conf.lowpass);
    filter::firhilbert<RealVectorType>(hilbert_fir, _conf.hilbert);
    signal::oscillate<double>(lo_buffer, local_osci, _conf.fs_in);
}

template<typename RealVectorType, typename CplxVectorType>
void DDC<RealVectorType, CplxVectorType>::process(RealVectorType& idata, RealVectorType& odata)
{
    assert(idata.size() * _up / _down * local_osci.size() * 2 == odata.size());
    processing::mat_vec_mult(lo_buffer, idata, mx_buffer, alpha);
    processing::upsample(mx_buffer, up_buffer, _up, local_osci.size());
    processing::convolve(up_buffer, lowpass_fir, fl_buffer, local_osci.size());
    processing::downsample(fl_buffer, rs_buffer, _down, local_osci.size());
    processing::convolve_hilbert(rs_buffer, hilbert_fir, odata, local_osci.size());
}


/*-------------------------------*/
/*  Polyphase Filter based DDC   */
/*-------------------------------*/
template<typename RealVectorType, typename CplxVectorType>
PolyphaseDownConverter<RealVectorType, CplxVectorType>::PolyphaseDownConverter(ddc_t& conf, std::size_t device_id)
  : DDC<RealVectorType, CplxVectorType>(conf), _device_id(device_id)
{
    BOOST_LOG_TRIVIAL(info) << "Constructing PolyphaseDownConverter object" << std::endl;
}

template<typename RealVectorType, typename CplxVectorType>
PolyphaseDownConverter<RealVectorType, CplxVectorType>::~PolyphaseDownConverter()
{
    BOOST_LOG_TRIVIAL(info) << "Deconstructing PolyphaseDownConverter object" << std::endl;
}

template<typename RealVectorType, typename CplxVectorType>
void PolyphaseDownConverter<RealVectorType, CplxVectorType>::allocate()
{
    BOOST_LOG_TRIVIAL(info) << "Allocating ressources for PolyphaseDownConverter, total memory: "
        << this->total_memory() / (1000*1000) << " MB" << std::endl;
    this->lowpass_fir.resize(this->_conf.lowpass.ntaps);
    this->hilbert_fir.resize(this->_conf.hilbert.ntaps);
    this->lo_buffer.resize(this->_conf.input_size * this->local_osci.size());
    this->rs_buffer.resize(this->_conf.input_size * this->local_osci.size() * this->_up / this->_down);
    filter::firwin<RealVectorType>(this->lowpass_fir, this->_conf.lowpass);
    filter::firhilbert<RealVectorType>(this->hilbert_fir, this->_conf.hilbert);
    signal::oscillate<double>(this->lo_buffer, this->local_osci, this->_conf.fs_in, (-1)*(int)this->_conf.pad_size());
    filter::reshape_poly_fir(this->lowpass_fir, this->_up, this->_down);
}

template<typename RealVectorType, typename CplxVectorType>
void PolyphaseDownConverter<RealVectorType, CplxVectorType>::process(RealVectorType& input, RealVectorType& output)
{
    processing::mix_poly_resample(
        input,
        this->lowpass_fir,
        this->local_osci,
        this->lo_buffer,
        this->rs_buffer,
        this->_up,
        this->_down,
        this->_conf.lowpass.ntaps, .0);
    processing::convolve_hilbert(this->rs_buffer, this->hilbert_fir, output, this->local_osci.size());
}

template<typename RealVectorType, typename CplxVectorType>
void PolyphaseDownConverter<RealVectorType, CplxVectorType>::process(
    thrust::device_vector<PolyphaseDownConverter<RealVectorType, CplxVectorType>::real>& input,
    thrust::device_vector<PolyphaseDownConverter<RealVectorType, CplxVectorType>::real>& output,
    cudaStream_t& stream)
{
    std::size_t sample_t0 = this->_call_count * (input.size() - 2 * this->_conf.pad_size());
    double t0 = (sample_t0 % this->_conf.fs_in)/ static_cast<double>(this->_conf.fs_in);
    processing::mix_poly_resample(
        input,
        this->lowpass_fir,
        this->local_osci,
        this->lo_buffer,
        this->rs_buffer,
        this->_up,
        this->_down,
        this->_conf.lowpass.ntaps,
        t0, stream);
    processing::convolve_hilbert(this->rs_buffer, this->hilbert_fir, output, this->local_osci.size(), stream);
    this->_call_count += 1;
}


/*-------------------------------*/
/*  Fourier Transform based DDC  */
/*-------------------------------*/
template<typename RealVectorType, typename CplxVectorType>
FourierDownConverter<RealVectorType, CplxVectorType>::FourierDownConverter(ddc_t& conf, std::size_t device_id)
  : DDC<RealVectorType, CplxVectorType>(conf), _device_id(device_id)
{
    // assert(_buffer_size % lowpass_fir.size() == 0);
    BOOST_LOG_TRIVIAL(info) << "Constructing FourierDownConverter object" << std::endl;
    assert(this->_up == 1);

    CUDA_ERROR_CHECK(cudaGetDeviceProperties(&_cu_prop, _device_id));
    // CUBLAS_ERROR_CHECK(cublasCreate(&cublas_handle_mx));
    // CUBLAS_ERROR_CHECK(cublasCreate(&cublas_handle_lp));
    // CUBLAS_ERROR_CHECK(cublasCreate(&cublas_handle_bp));
    CUFFT_ERROR_CHECK(cufftPlan1d(
        &cufft_plan_lp, this->_conf.input_size, CUFFT_C2C, 1)
    );
    CUFFT_ERROR_CHECK(cufftPlan1d(
        &cuifft_plan_lp, this->rs_buffer.size(), CUFFT_C2C, 1)
    );

    filter::firwin<RealVectorType>(this->lowpass_fir, this->_conf.lowpass);

    signal::oscillate<double>(this->lo_buffer, this->local_osci, this->_conf.fs_in);
    // Allocate
    d_lowpass_fir.resize(this->lowpass_fir.size());
    this->transform_lowpass();
}


template<typename RealVectorType, typename CplxVectorType>
FourierDownConverter<RealVectorType, CplxVectorType>::~FourierDownConverter()
{
    // CUBLAS_ERROR_CHECK(cublasDestroy(cublas_handle_mx));
    // CUBLAS_ERROR_CHECK(cublasDestroy(cublas_handle_lp));
    // CUBLAS_ERROR_CHECK(cublasDestroy(cublas_handle_bp));
    CUFFT_ERROR_CHECK(cufftDestroy(cufft_plan_lp));
    CUFFT_ERROR_CHECK(cufftDestroy(cuifft_plan_lp));
    // CUFFT_ERROR_CHECK(cufftDestroy(cufft_plan_hb));
}

template<typename RealVectorType, typename CplxVectorType>
void FourierDownConverter<RealVectorType, CplxVectorType>::transform_lowpass()
{
    cufftHandle tmp_plan;
    CUFFT_ERROR_CHECK(cufftPlan1d(
        &tmp_plan, this->lowpass_fir.size(), CUFFT_R2C, 1));
    CUFFT_ERROR_CHECK(cufftExecR2C(
        tmp_plan,
        thrust::raw_pointer_cast(this->lowpass_fir.data()),
        thrust::raw_pointer_cast(d_lowpass_fir.data())));
    CUFFT_ERROR_CHECK(cufftDestroy(tmp_plan));
}

template<typename RealVectorType, typename CplxVectorType>
void FourierDownConverter<RealVectorType, CplxVectorType>::process(RealVectorType idata, RealVectorType odata)
{
  CUFFT_ERROR_CHECK(cufftExecC2C(
        cufft_plan_lp,
        thrust::raw_pointer_cast(this->mx_buffer.data()),
        thrust::raw_pointer_cast(this->mx_buffer.data()),
        CUFFT_FORWARD));

  CUFFT_ERROR_CHECK(cufftExecC2C(
        cuifft_plan_lp,
        thrust::raw_pointer_cast(this->mx_buffer.data()),
        thrust::raw_pointer_cast(this->mx_buffer.data()),
        CUFFT_INVERSE));
}


}
}