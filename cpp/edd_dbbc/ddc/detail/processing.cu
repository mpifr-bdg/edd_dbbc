#pragma once

// Includes here are optional, but allow intellisense to autocomplete and highlight
#include <cuda.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <psrdada_cpp/cuda_utils.hpp>

namespace edd_dbbc {
namespace ddc {
namespace processing {
namespace kernel {

template<typename T>
__global__ void upsample(
    const T* idata,
    T* odata,
    unsigned up,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    if(tidx < width){
        odata[tidx * up] = idata[tidx];
    }
}


template<typename T>
__global__ void downsample(
    const T* idata,
    T* odata,
    unsigned down,
    unsigned width)
{
    unsigned tidx = threadIdx.x + blockIdx.x * blockDim.x;
    if(tidx < width){
        odata[tidx] = idata[tidx * down];
    }
}


template<typename T>
__global__ void poly_resample(const T* idata,
    const T* filter,
    T* odata,
    unsigned up,
    unsigned down,
    unsigned width,
    unsigned ntaps)
{
    unsigned tidx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned tidy = blockIdx.y;
    unsigned o_width = width * up / down;
    unsigned half_fir = (ntaps - 1) / 2;
    unsigned rotate = up - down + up * down;
    unsigned iidx = 0;
    extern __shared__ float s_filter[];

    for(unsigned i = threadIdx.x; i < ntaps; i+=blockDim.x){
        s_filter[i] = filter[i];
    }
__syncthreads();

    for(unsigned oidx = tidx; oidx < o_width; oidx+=blockDim.x*gridDim.x){
        float tmp = 0;
        unsigned poly_branch = (half_fir + rotate * oidx) % up;
        for(unsigned fidx = poly_branch; fidx < ntaps; fidx+=up){
            iidx = tidy * width + (oidx * down + fidx - half_fir) / up;
            if(iidx >= tidy * width && iidx < (tidy+1) * width){
                tmp += idata[iidx] * s_filter[fidx];
            }
        }
        odata[tidy * o_width + oidx] = tmp;
    }

}


template<typename T>
__global__ void mat_vec_mult(
    T* matrix,
    const T* vector,
    unsigned width,
    unsigned height
)
{
    __shared__ T s_vec[1024];
    unsigned glob_idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(glob_idx < width){
        s_vec[threadIdx.x] = vector[glob_idx];
    }
    __syncthreads();
    for(unsigned i = glob_idx; i < height*width; i+=width){
        matrix[i].x = matrix[i].x * s_vec[threadIdx.x].x - matrix[i].y * s_vec[threadIdx.x].y;
        matrix[i].y = matrix[i].x * s_vec[threadIdx.x].y + matrix[i].y * s_vec[threadIdx.x].x;
    }
}


}


template<typename T>
void upsample(
    const thrust::device_vector<T>& input,
    thrust::device_vector<T>& output,
    std::size_t up,
    std::size_t n_signals,
    cudaStream_t stream)
{
    assert(input.size() * up == output.size());
    kernel::upsample<<<input.size() / 1024 + 1, 1024, 0>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(output.data()),
        up, input.size()
    );
}


template<typename T>
void upsample(
    const thrust::host_vector<T>& input,
    thrust::host_vector<T>& output,
    std::size_t up,
    std::size_t n_signals)
{
    assert(input.size() * up == output.size());
    std::size_t isignal_length = input.size() / n_signals;
    std::size_t osignal_length = output.size() / n_signals;
    for(std::size_t n = 0; n < n_signals; n++){
        for(std::size_t i = 0; i < isignal_length; i++){
            output[n * osignal_length + i * up] = input[n * osignal_length + i];
        }
    }
}


template<typename T>
void downsample(
    const thrust::device_vector<T>& input,
    thrust::device_vector<T>& output,
    std::size_t down,
    std::size_t n_signals,
    cudaStream_t stream)
{
    assert(input.size() / down == output.size());
    kernel::downsample<<<output.size() / 1024 + 1, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(output.data()),
        down, output.size()
    );
}


template<typename T>
void downsample(
    const thrust::host_vector<T>& input,
    thrust::host_vector<T>& output,
    std::size_t down,
    std::size_t n_signals)
{
    assert(input.size() / down == output.size());
    std::size_t isignal_length = input.size() / n_signals;
    std::size_t osignal_length = output.size() / n_signals;
    for(std::size_t n = 0; n < n_signals; n++){
        for(std::size_t i = 0; i < osignal_length; i++){
            output[n * osignal_length + i] = input[n * isignal_length + i * down];
        }
    }
}


template<typename VectorType, typename FilterType>
void resample(
    const VectorType& input,
    const FilterType& lfilter,
    VectorType& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal)
{
    assert(output.size() == input.size() * up / down);
    VectorType upsampled(up*input.size());
    VectorType filtered(upsampled.size());
    processing::upsample(input, upsampled, up, n_signal);
    processing::convolve(upsampled, lfilter, filtered, n_signal);
    processing::downsample(filtered, output, down, n_signal);
}


template<typename T>
void poly_resample(
    const thrust::host_vector<T>& input,
    const thrust::host_vector<T>& lfilter,
    thrust::host_vector<T>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal)
{
    assert(output.size() == input.size() * up / down);
    std::size_t i_length =  input.size() / n_signal;
    std::size_t o_length = output.size() / n_signal;
    std::size_t half_fir = (lfilter.size() - 1) / 2;
    std::size_t rotate = up - down + up * down;
    for(std::size_t n = 0; n < n_signal; n++){
        std::size_t o_offset = n * o_length;
        std::size_t i_offset = n * i_length;
        for(std::size_t oidx = 0; oidx < o_length; oidx++){
            std::size_t poly_branch = (half_fir + rotate * oidx) % up;
            for(std::size_t fidx = poly_branch; fidx < lfilter.size(); fidx += up){
                int iidx = (oidx * down + fidx - half_fir) / up;
                if (iidx >= 0 && iidx < (int)i_length){
                    output[o_offset + oidx] += input[i_offset + iidx] * lfilter[fidx];
                }
            }
        }
    }
}


template<typename T>
void poly_resample(
    const thrust::device_vector<T>& input,
    const thrust::device_vector<T>& lfilter,
    thrust::device_vector<T>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal,
    cudaStream_t stream)
{
    dim3 grid;
    grid.x = (N_SM * N_THREADS < input.size()) ? N_SM / n_signal : input.size() / N_THREADS + 1;
    grid.y = n_signal;
    unsigned s_memory = lfilter.size() * sizeof(T);
    kernel::poly_resample<<<grid, N_THREADS, s_memory, stream>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(lfilter.data()),
        thrust::raw_pointer_cast(output.data()),
        up, down, input.size() / n_signal, lfilter.size()
    );
}


template<typename T>
void mat_vec_mult(
    thrust::device_vector<T>& matrix,
    thrust::device_vector<T>& vector,
    cudaStream_t stream)
{
    assert(matrix.size() % vector.size() == 0);
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    dim3 grid(cols / 1024 + 1, 1, 1);
    kernel::mat_vec_mult<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(matrix.data()),
        thrust::raw_pointer_cast(vector.data()),
        cols, rows
    );
}




}
}
}