#pragma once

#include <gtest/gtest.h>
#include <ctime>
#include <random>
#include <string>
#include <cstring>
#include <psrdada_cpp/testing_tools/streams.hpp>

#include "edd_dbbc/testing_tools.cuh"
#include "edd_dbbc/ddc/pipeline.cuh"