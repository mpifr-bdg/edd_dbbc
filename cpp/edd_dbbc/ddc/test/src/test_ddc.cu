#include "edd_dbbc/ddc/test/test_ddc.cuh"

namespace edd_dbbc {
namespace ddc {
namespace test {

// Define the different processor types
using DDCHostType = ddc::DDC<thrust::host_vector<float>, thrust::host_vector<float2>>;
using DDCDeviceType = ddc::DDC<thrust::device_vector<float>, thrust::device_vector<float2>>;
using PolyHostType = ddc::PolyphaseDownConverter<thrust::host_vector<float>, thrust::host_vector<float2>>;
using PolyDeviceType = ddc::PolyphaseDownConverter<thrust::device_vector<float>, thrust::device_vector<float2>>;
using FourierHostType = ddc::FourierDownConverter<thrust::host_vector<float>, thrust::host_vector<float2>>;
using FourierDeviceType = ddc::FourierDownConverter<thrust::device_vector<float>, thrust::device_vector<float2>>;


/**
 * @brief Construct a new TEST_P using the DDC on the GPU
 *        Output products are compared against a python implementation
 */
#ifdef COMPREHENSIVE_TESTS
TEST_P(DDCTester, TestDDCDevice)
{
  std::cout << std::endl
    << "-------------------------------" << std::endl
    << " Testing convential DDC on GPU " << std::endl
    << "-------------------------------" << std::endl;
  run_test<DDCDeviceType, thrust::device_vector<float>, thrust::device_vector<float2>>();
}
#endif
/**
 * @brief Construct a new TEST_P using the PolyphaseDownConverter on the GPU
 *        Output products are compared against a python implementation
 */
TEST_P(DDCTester, TestPolyphaseDownConverterDevice){
  std::cout << std::endl
    << "---------------------------------------" << std::endl
    << " Testing PolyphaseDownConverter on GPU " << std::endl
    << "---------------------------------------" << std::endl;
  run_test<PolyDeviceType, thrust::device_vector<float>, thrust::device_vector<float2>>();
}
/**
 * @brief Construct a new INSTANTIATE_TEST_SUITE_P object
 *        Parameterize the defined TEST_P objects
 */
INSTANTIATE_TEST_SUITE_P(DDCTesterInstantiation, DDCTester, ::testing::Values(
    ddc_t{6000, 750, 500, "300", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                 // 0
    ddc_t{640000, 32000, 750, "1000", false, {301, 0.05, 0.05, 0.45}, {0, 10, "rect", 0}},                // 1
    ddc_t{64000, 3200, 750, "1000", false, {301, 0.05, 0.05, 0.45}, {0, 1, "hamming", 0}},               // 2
    ddc_t{3500, 700, 500, "8000", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},               // 3
    ddc_t{132000, 40, 33, "15", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                   // 4
    ddc_t{32000, 32000, 4000, "8000", false, {301, 0.05, 0.05, 0.45}, {0, 8, "hamming", 0}},             // 5
    ddc_t{110000, 55000, 10000, "4300", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},           // 6
    ddc_t{30000, 250, 150, "75", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                 // 7
    ddc_t{7500,  250, 150, "1234,245,123,222", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},   // 8
    ddc_t{1320, 12, 11, "999", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                  // 9
    ddc_t{91800, 3400, 2700, "999", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                  // 9
    ddc_t{55000, 5000, 2750, "999", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},                  // 9
    ddc_t{32000, 32000, 2000, "0,2000,4000,6000", false, {301, 0.05, 0.05, 0.45}, {0, 7, "hamming", 0}},  // 10

#ifdef COMPREHENSIVE_TESTS
    ddc_t{40960000, 4096000000, 16000000, "238000000", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},  // 12
    ddc_t{409600000, 1024000000, 1000000, "256000000", false, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}},  // 13
#endif
    ddc_t{1280000, 32000, 2000, "6000,6000", false, {301, 0.05, 0.05, 0.45}, {0, 12, "hamming", 0}} // 11
));

}
}
}