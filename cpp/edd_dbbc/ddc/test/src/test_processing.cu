#include "edd_dbbc/ddc/test/test_processing.cuh"


namespace edd_dbbc {
namespace ddc{
namespace processing{
namespace test{

// Upsample tests
TEST(upsample, test_upsample_real_against_reference){
    test_upsample<float>(2049, 16);
}
TEST(upsample, test_upsample_cplx_against_reference){
    test_upsample<float2>(123456, 16);
}


// Downsample tests
TEST(downsample, test_downsample_real_against_reference){
    test_downsample<float>(16000, 16);
}
TEST(downsample, test_downsample_cplx_against_reference){
    test_downsample<float2>(32000, 16);
}


// Convolve tests
TEST(convolve, test_convolve_real_against_reference){
    test_convolve<float>(123456, 1234);
}
TEST(convolve, test_convolve_cplx_against_reference){
    test_convolve<float2>(123456, 1234);
}
TEST(convolve, test_convolve_real_multiple_signals_against_reference){
    test_convolve<float>(12345, 1234, 3);
}
TEST(convolve, test_convolve_cplx_multiple_signals_against_reference){
    test_convolve<float2>(12345, 1234, 3);
}


// Convolve hilbert tests
TEST(convolve, test_convolve_hilbert_against_reference){
    test_convolve_hilbert(123456, 301);
}
TEST(convolve, test_convolve_hilbert_multiple_signals_against_reference){
    test_convolve_hilbert(12345, 301, 3);
}


// Elementwise matrix-vector multiplication tests

// #ifdef COMPREHENSIVE_TESTS
// TEST(mat_vec_mult, test_mat_vec_mult_real_against_reference){
//     test_mat_vec_mult<float>(12345, 12);
// }
// TEST(mat_vec_mult, test_mat_vec_mult_cplx_against_reference){
//     test_mat_vec_mult<float2>(12345, 12);
// }
// #endif

/*---------------------------------------- */
/*  Parameterized tests for ResampleTester */
/*---------------------------------------- */

/**
 * @brief Construct a new TEST_P using the conventional resampler on the GPU
 *        Output products are compared against the same algorithm on the CPU
 */
#ifdef COMPREHENSIVE_TESTS

TEST_P(ResampleTester, TestConventionalResamplerDeviceFloat2){
  std::cout << std::endl
    << "-------------------------------------------------------------------" << std::endl
    << " Testing conventional resampler against convential on CPU (float2)" << std::endl
    << "-------------------------------------------------------------------" << std::endl;
  test_resample_gpu<float2, float>();
}

/**
 * @brief Construct a new TEST_P using the conventional resampler on the GPU
 *        Output products are compared against the same algorithm on the CPU
 */
TEST_P(ResampleTester, TestConventionalResamplerDeviceFloat){
  std::cout << std::endl
    << "------------------------------------------------------------------" << std::endl
    << " Testing conventional resampler against convential on CPU (float) " << std::endl
    << "------------------------------------------------------------------" << std::endl;
  test_resample_gpu<float, float>();
}

// /**
//  * @brief Construct a new TEST_P using the polyphase resampler on the CPU
//  *        Output products are compared against the convential algorithm on the CPU
//  */
TEST_P(ResampleTester, TestPolyResamplerHostFloat2){
  std::cout << std::endl
    << "-------------------------------------------------------------------" << std::endl
    << " Testing polyphase resampler against convential on CPU (float2)" << std::endl
    << "-------------------------------------------------------------------" << std::endl;
  test_poly_resample_against_resample<float2, float>();
}

// /**
//  * @brief Construct a new TEST_P using the polyphase resampler on the CPU
//  *        Output products are compared against the convential algorithm on the CPU
//  */
TEST_P(ResampleTester, TestPolyResamplerHostFloat){
  std::cout << std::endl
    << "---------------------------------------------------------------" << std::endl
    << " Testing polyphase resampler against convential on CPU (float) " << std::endl
    << "---------------------------------------------------------------" << std::endl;
  test_poly_resample_against_resample<float, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the GPU
 *        Output products are compared against the convential algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolyResamplerDeviceAgainstConventionalFloat2){
  std::cout << std::endl
    << "-----------------------------------------" << std::endl
    << " Testing polyphase resampler GPU (float2)" << std::endl
    << "-----------------------------------------" << std::endl;
  test_poly_resample_against_resample_gpu<float2, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the GPU
 *        Output products are compared against the convential algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolyResamplerDeviceAgainstConventionalFloat){
  std::cout << std::endl
    << "-----------------------------------------" << std::endl
    << " Testing polyphase resampler GPU (float) " << std::endl
    << "-----------------------------------------" << std::endl;
  test_poly_resample_against_resample_gpu<float, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the GPU
 *        Output products are compared against the polyphase algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolyResamplerDeviceFloat2){
  std::cout << std::endl
    << "-----------------------------------------------------------" << std::endl
    << " Testing poly_resample()  GPU against poly_resample()  CPU " << std::endl
    << "-----------------------------------------------------------" << std::endl;
  test_poly_resample_gpu<float2, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the GPU
 *        Output products are compared against the polyphase algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolyResamplerDeviceFloat){
  std::cout << std::endl
    << "-----------------------------------------------------------" << std::endl
    << " Testing poly_resample()  GPU against poly_resample()  CPU " << std::endl
    << "-----------------------------------------------------------" << std::endl;
  test_poly_resample_gpu<float, float>();
}
#endif


TEST_P(ResampleTester, TestMixPolyResamplerDeviceFloat){
  std::cout << std::endl
    << "---------------------------------------------------------------------------------- " << std::endl
    << " Testing mix_poly_resample() GPU against mat_vec() + poly_resample() CPU (T=float) " << std::endl
    << "---------------------------------------------------------------------------------- " << std::endl;
  test_mix_poly_resample_gpu(0.1, 0.001);
}


INSTANTIATE_TEST_SUITE_P(ResampleTesterInstantiation, ResampleTester, ::testing::Values(
    resample_t{750, 300, 750, 5},
    resample_t{32000, 500, 32000, 2},
    resample_t{7, 4, 280, 3},
    resample_t{11, 6, 660, 4},
    resample_t{7, 5, 350, 5},
    resample_t{7, 5, 350, 3},
    resample_t{9, 4, 360, 3},
    resample_t{4, 3, 24, 7},
    resample_t{3, 2, 24, 4},
    resample_t{550, 100, 2200, 10},
    resample_t{500, 100, 2000, 10},
    resample_t{650, 100, 13000, 5},
    resample_t{40, 33, 1320, 5},
    resample_t{750, 300, 1500, 10},
    /** Reduce number of tests uncomment when developing*/
#ifdef COMPREHENSIVE_TESTS
    resample_t{40000000, 1600000, 20000000, 10},
    resample_t{80000000, 1600000, 20000000, 10},
    resample_t{160000000, 1600000, 20000000, 11},
    resample_t{320000000, 1600000, 32000000, 10},
    resample_t{400000000, 1600000, 400000000, 1},
    resample_t{40000000, 1600000, 20000000, 15},
    resample_t{100000000, 1600000, 20000000, 10},
#endif
    resample_t{32000, 500, 320000, 1},
    resample_t{32000, 500, 320000, 10},
    resample_t{48000, 16000, 300000, 20},
    resample_t{3200, 75,  32000, 10},
    resample_t{3200, 750, 32000, 10}
));

}
}
}
}