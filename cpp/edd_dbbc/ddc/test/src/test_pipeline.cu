#include "edd_dbbc/ddc/test/test_pipeline.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace ddc {
namespace test {

const int header_block_size = 4096;

TEST(Test_Pipeline, test_sample_clock_conversion){
    pipe_config config;
    psr_testing::DummyStream<float> consumer;
    Pipeline<decltype(consumer), float> pipeline(config, consumer);

    char container[header_block_size] = {0};
    psrdada_cpp::RawBytes header(container, header_block_size, header_block_size);
    ascii_header_set(header.ptr(), "SAMPLE_CLOCK", "%lu", config.ddc.fs_in);

    std::size_t actual_sample_clock, actual_sample_clock_start, expected_sample_clock_start;
    std::size_t nosamples = config.output_size() / (sizeof(float) * config.ddc.nsignals() * 2);
    // Check different SAMPLE_CLOCK_START values
    for(std::size_t start = 0;
        start < config.ddc.fs_in * config.ddc.fs_dw * 10;
        start += config.ddc.fs_in * config.ddc.fs_dw)
    {

        ascii_header_set(header.ptr(), "SAMPLE_CLOCK_START", "%lu", start);
        pipeline.init(header);
        // Retrieve the header from the consumer object
        psrdada_cpp::RawBytes oblock(consumer.header_ptr(), consumer.header_size(), consumer.header_size());
        ascii_header_get(oblock.ptr(), "SAMPLE_CLOCK_START", "%lu", &actual_sample_clock_start);
        ascii_header_get(oblock.ptr(), "SAMPLE_CLOCK", "%lu", &actual_sample_clock);

        expected_sample_clock_start = start * config.ddc.up() / config.ddc.down();
        ASSERT_EQ(actual_sample_clock_start, expected_sample_clock_start);
        ASSERT_EQ(actual_sample_clock, config.ddc.fs_dw);
    }
}


TEST(Test_Pipeline, test_init_throw_bad_scs)
{
    pipe_config config;
    psr_testing::DummyStream<float> consumer;
    Pipeline<decltype(consumer), float> pipeline(config, consumer);
    char container[header_block_size] = {0};
    psrdada_cpp::RawBytes header(container, header_block_size, header_block_size);
    ascii_header_set(header.ptr(), "SAMPLE_CLOCK", "%lu", config.ddc.fs_in);
    ascii_header_set(header.ptr(), "SAMPLE_CLOCK_START", "%lu", 1); // Bad sample clock start value
    EXPECT_THROW(pipeline.init(header), std::runtime_error);
}


class PipelineStreamTester : public ::testing::TestWithParam<pipe_config>
{
protected:
  void SetUp(){};
  void TearDown(){};
public:
    PipelineStreamTester() : ::testing::TestWithParam<pipe_config>(),
    _config(GetParam()){};

    template<typename T>
    void test(std::size_t nbuffers=16)
    {
        psr_testing::DummyStream<char> consumer;
        Pipeline<decltype(consumer), T> pipeline(_config, consumer);
        std::size_t samp_size = _config.nbit() / 8;
        // Generate input testvectors
        thrust::host_vector<T> idata(_config.input_size() / sizeof(T) * nbuffers);
        thrust::host_vector<float> odata(_config.output_size() * nbuffers / sizeof(float));
        thrust::device_vector<float> ridata(_config.isamples() * nbuffers);
        thrust::device_vector<float> rodata(odata.size());
        // signal::chirp(idata, _config.ddc.fs_in, 1, _config.ddc.fs_in/2);
        if(_config.nbit() == 32){
            signal::random(idata, 0, 2000);
            // signal::chirp(idata, 0, 200);
            // signal::chirp(idata, _config.ddc.fs_in, 1.0, _config.ddc.fs_in/4);
            ridata = idata;
        }else if(_config.nbit() == 8){
            thrust::host_vector<int8_t> tmp(_config.input_size() * nbuffers);
            signal::random(tmp, 0, 64);
            std::memcpy((void*)&idata[0], &tmp[0], tmp.size());
            ridata = tools::conversion<int8_t, float>(tmp);
        }else{
            std::cout << "Test not implemented for bit size " << _config.nbit() << std::endl;
            return;
        }
        // initialize the pipeline under test
        char container[header_block_size] = {0};
        psrdada_cpp::RawBytes header(container, header_block_size, header_block_size);
        ascii_header_set(header.ptr(), "SAMPLE_CLOCK", "%lu", _config.ddc.fs_in);
        ascii_header_set(header.ptr(), "SAMPLE_CLOCK_START", "%lu", 0);
        pipeline.init(header);
        // Run the pipeline in a streaming fashion
        for(std::size_t i = 0; i < nbuffers; i++){
            // Create a data block and pass it to the pipeline under test
            psrdada_cpp::RawBytes block(
                reinterpret_cast<char *>(&idata[i * _config.input_size() / sizeof(T)]),
                _config.input_size(), _config.input_size());
            pipeline(block);
            // Copy data when the consumer got data
            if(consumer.call_count() > 0){
                std::memcpy(
                    (void*)&odata[(consumer.call_count()-1) * _config.output_size() / sizeof(float)],
                    (void*)consumer.data_ptr(), consumer.data_size());
            }
        }

        // Run a non-streaming implementation -> single processing without padding and pipelining. We use the conventional DDC!
        _config.ddc.input_size = ridata.size();
        ddc::DDC<thrust::device_vector<float>, thrust::device_vector<float2>> reference(_config.ddc);
        reference.allocate();
        reference.process(ridata, rodata);
        thrust::host_vector<float> ref_odata = rodata;

        // Compare the data
        std::size_t nbands = _config.ddc.nsignals() * 2;
        std::size_t nosamples = _config.output_size() / (sizeof(float) * nbands);
        // nbuffers-5 -> Needs 5 calls to operator()() to process the first output buffer
        for(std::size_t b = 0; b < nbuffers-5; b++){
            for(std::size_t n = 0; n < nbands; n++){
                for(std::size_t i = 0; i < nosamples; i++){
                    std::size_t test_idx = b * nbands * nosamples + n * nosamples + i;
                    if(_config.batch_size() > 0){
                        std::size_t div = i / _config.batch_size();
                        std::size_t mod = i % _config.batch_size();
                        test_idx = b * nbands * nosamples           // index position for sub-buffers
                            + div * nbands * _config.batch_size()   // index position of corresponding batch
                            + n * _config.batch_size() + mod;       // index position of the sample
                    }
                    std::size_t ref_idx = n * nosamples * nbuffers + b * nosamples + i;
                    if(b == 0 && i < 14){continue;} // Magic number (=14) because of transient response in the first block
                    ASSERT_NEAR(ref_odata[ref_idx], odata[test_idx], 1 + std::fabs(ref_odata[ref_idx]) * 0.01)
                        << "Position gold: " << ref_idx << "; Position test: " << test_idx << " N=" << n << " B=" << b << " I=" << i;
                }
            }
        }
    }

private:
    pipe_config _config;

};


TEST_P(PipelineStreamTester, test_streaming_float){
    std::cout << std::endl
        << "--------------------------------" << std::endl
        << " Testing DDC Pipeline with float" << std::endl
        << "--------------------------------" << std::endl;
    pipe_config conf = GetParam();
    if(conf.nbit() == 32)
    {
        test<float>();
    }
    else
    {
        test<uint64_t>();
    }
}

INSTANTIATE_TEST_SUITE_P(PipelineStreamTesterInstantiation, PipelineStreamTester, ::testing::Values(
    pipe_config({0, 32000, 2000, "4000,6000", true, {301, 0.05, 0.05, 0.45}, {0, 7, "hamming", 0}}, 32, 128000, 16000*2, 0),
    // pipe_config({0, 5000, 2750, "1111,2300", true, {301, 0.05, 0.05, 0.45}, {0, 7, "hamming", 0}}, 32, 55000, 30250*4, 0), // fails
    // pipe_config({0, 3400, 2700, "2333", true, {301, 0.05, 0.05, 0.45}, {0, 7, "hamming", 0}}, 32, 91800, 72900*2, 0), // fails
#ifdef COMPREHENSIVE_TESTS
    pipe_config({0, 800000000, 2000000, "3333,12014,2133,111", true, {301, 0.05, 0.05, 0.45}, {0, 7, "hamming", 0}}, 32, 3200000, 8000*4*2, 0),
    pipe_config({0, 4096000000, 2000000, "4000,7777", true, {301, 0.05, 0.05, 0.45}, {0, 7, "hamming", 0}}, 32, 33554432, 16384*2*2, 2048),
    pipe_config({0, 1024000000, 2000000, "4000,55322", true, {301, 0.05, 0.05, 0.45}, {0, 7, "hamming", 0}}, 8, 8192000, 256000, 4000),
#endif
    pipe_config({0, 128000, 32000, "0,1000", true, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}}, 32, 256000, 128000*2, 0),
    // pipe_config({0, 750, 300, "250", true, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}}, 32, 30000, 24000, 0),
    pipe_config({0, 1200, 1100, "333", false, {308, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}}, 32, 528000, 968000, 0),
    // Test with packetization
    // pipe_config({0, 4000000000, 256000000, "512000000,256000000", true, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}}, 32, 8000000, 512000*2*2, 8000),
    pipe_config({0, 32000, 2000, "4000", true, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}}, 32, 512000, 16000*4, 4000),
    // Test with packetization and 8bit
    pipe_config({0, 32000, 2000, "6000,12721", true, {301, 0.05, 0.05, 0.45}, {0, 12, "hamming", 0}}, 8, 131072, 32768*4, 2048),
    pipe_config({0, 32000, 2000, "4000", true, {301, 0.05, 0.05, 0.45}, {0, 10, "hamming", 0}}, 8, 131072, 16384*4, 0)
));

}
}
}