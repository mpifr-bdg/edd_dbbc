#include "edd_dbbc/ddc/test/test_signal.cuh"

namespace edd_dbbc {
namespace ddc{
namespace signal{
namespace test{


TEST(chirp, test_chirp_against_reference)
{
    float fs = 32000;
    int width = 32000;

    thrust::device_vector<float> d_gpu(width);
    thrust::host_vector<float> cpu(width);

    chirp( d_gpu, fs, 0, fs / 2 );
    chirp( cpu, fs, 0, fs / 2 );

    thrust::host_vector<float> gpu = d_gpu;

    EXPECT_EQ(cpu.size(), gpu.size());
    for(std::size_t i = 0; i < cpu.size(); i++){
        EXPECT_NEAR(gpu[i], cpu[i], 0.01);
    }
}

TEST(oscillate, test_oscillate_against_reference1)
{
    float fs = 400000;
    int width = 12345678;
    // Allocate
    thrust::device_vector<float> d_f_lo = {3, 777777};
    thrust::device_vector<float2> d_gpu(width * d_f_lo.size());
    thrust::host_vector<float> f_lo = d_f_lo;
    thrust::host_vector<float2> cpu(width * f_lo.size());
    // Process
    oscillate<double>( d_gpu, d_f_lo, fs );
    oscillate<double>( cpu, f_lo, fs );
    // Copy device memory to host
    thrust::host_vector<float2> gpu = d_gpu;
    // Compare
    EXPECT_EQ(cpu.size(), gpu.size());
    for(std::size_t i = 0; i < cpu.size(); i++){
        EXPECT_NEAR(gpu[i].x, cpu[i].x, 0.01);
        EXPECT_NEAR(gpu[i].y, cpu[i].y, 0.01);
    }
}

TEST(oscillate, test_oscillate_against_reference2)
{
    float fs = 32000;
    int width = 3200000;
    // Allocate
    thrust::device_vector<float> d_f_lo = {1, 9000};
    thrust::device_vector<float2> d_gpu(width * d_f_lo.size());
    thrust::host_vector<float> f_lo = d_f_lo;
    thrust::host_vector<float2> cpu(width * f_lo.size());
    // Process
    oscillate<double>(d_gpu, d_f_lo, fs );
    oscillate<double>(cpu, f_lo, fs );
    // Copy device memory to host
    thrust::host_vector<float2> gpu = d_gpu;
    // Compare
    EXPECT_EQ(cpu.size(), gpu.size());
    for(std::size_t i = 0; i < cpu.size(); i++){
        ASSERT_NEAR(gpu[i].x, cpu[i].x, 0.01) << " Position " << i;
        ASSERT_NEAR(gpu[i].y, cpu[i].y, 0.01) << " Position " << i;
    }
}


}
}
}
}