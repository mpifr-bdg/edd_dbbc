/**
 * @file    signal.cuh
 * @author  Niclas Esser (nesser@mpifr-bonn.mpg.de)
 * @brief   The signal.cuh files contains utilities to generate signals.
 *          Device code is implemented in src/signal.cu
 * @version 0.1
 * @date    2024-04-25
 *
 */
#pragma once
#include <cuda.h>
#include <random>
#include <cmath>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

namespace edd_dbbc {
namespace ddc {
namespace signal {
namespace kernel {
/**
 * @brief GPU kernel to generate a chirp / sweep
 *
 * @param data The data pointer to the array
 * @param fs The sampling frequency
 * @param f0 The start frequency of the chirp
 * @param f1 The end frequency of the chirp
 * @param gain The maximum amplitude of the chirp
 * @param t1 The time when f1 is reached
 * @param width The width of the data vector
 */
__global__ void chirp(
    float* data,
    float fs,
    float f0,
    float f1,
    float gain,
    float t1,
    int width);

/**
 * @brief GPU kernel to compute the phase offset for a set of mixing fequencies
 *
 * @param phases The data pointer to the array (phases are writen to this one)
 * @param freq The data pointer to all mixing frequencies
 * @param fs The sampling frequency
 * @param nsamples The number of samples for which to calculate the current phase
 * @param precision Clip the phase to zero (if phase < precision then phase = 0)
 */
__global__ void phase(float2* phases, float* freq, std::size_t fs, std::size_t nsamples, double precision=1e-9);

/**
 * @brief GPU kernel to generate an array of oscillation frequencies 1D or 2D
 *
 * @tparam T the data type to define the preicision (either float or double)
 * @param data The data pointer to the array (samples are written to this one)
 * @param lo The data pointer containing the frequencies of the oscillation
 * @param fs The sampling rate
 * @param width The length of the array in samples
 * @param height The number of frequencies
 * @param offset Additional offset to shift the time in number of samples
 * @return __global__
 */
template<typename T>
__global__ void oscillate(float2* data, float* lo, float fs, int width, int height, int offset=0);

}

/**
 * @brief Compute the sinc function for a given value. Can be executed on the host or device.
 *
 * @param x The value
 * @return float The resulting value of the sinc function
 */
__forceinline__ __host__ __device__ float sinc(float x);

/**
 * @brief Wrapper for the GPU kernel "chirp". This function computes the grid and block dimension and launches the kernel.
 *
 * @details Overloaded function: When a thrust::device_vector<float> is passed runs on the GPU
 *
 * @param data The device vector where data is written to. Must be allocated on the device.
 * @param fs The sampling frequency
 * @param f0 The start frequency of the chirp
 * @param f1 The end frequency of the chirp
 * @param gain The maximum amplitude of the chirp. Defaults to 1
 * @param t1 The time when f1 is reached. If set to -1, f1 is reached with the last sample in the vector. Defaults to -1.
 */
void chirp(
    thrust::device_vector<float>& data,
    float fs,
    float f0,
    float f1,
    float gain=1,
    float t1=-1);


/**
 * @brief Host implementation to generate a chirp / sweep.
 *
 * @details Overloaded function: When a thrust::host_vector<float> is passed runs on the CPU
 *
 * @param data The host vector where data is written to. Must be allocated on the host.
 * @param fs The sampling frequency
 * @param f0 The start frequency of the chirp
 * @param f1 The end frequency of the chirp
 * @param gain The maximum amplitude of the chirp. Defaults to 1
 * @param t1 The time when f1 is reached. If set to -1, f1 is reached with the last sample in the vector. Defaults to -1.
 */
void chirp(
    thrust::host_vector<float>& data,
    float fs,
    float f0,
    float f1,
    float gain=1,
    float t1=-1);


/**
 * @brief Host implementation to generate a complex chirp / sweep.
 *
 * @details Overloaded function: When a thrust::host_vector<float2> is passed runs on the CPU
 *
 * @param data The host vector where data is written to. Must be allocated on the host.
 * @param fs The sampling frequency
 * @param f0 The start frequency of the chirp
 * @param f1 The end frequency of the chirp
 * @param gain The maximum amplitude of the chirp. Defaults to 1
 * @param t1 The time when f1 is reached. If set to -1, f1 is reached with the last sample in the vector. Defaults to -1.
 */
void chirp(
    thrust::host_vector<float2>& data,
    float fs,
    float f0,
    float f1,
    float gain=1,
    float t1=-1);


/**
 * @brief Wrapper for the GPU kernel "phase". This function computes the grid and block dimension and launches the kernel.
 *
 * @details Overloaded function: When a thrust::device_vector<float2> is passed runs on the GPU
 *
 * @param phases The device vector where phases are written to this one
 * @param freq The device vector to all mixing frequencies
 * @param fs The sampling frequency
 * @param nsamples The number of samples for which to calculate the current phase
 * @param precision Clip the phase to zero (if phase < precision then phase = 0)
 */
void phase(
    thrust::device_vector<float2>& phases,
    thrust::device_vector<float>& freq,
    std::size_t fs,
    std::size_t nsamples,
    cudaStream_t stream = NULL);


/**
 * @brief Host implementation to compute the phase
 *
 * @details Overloaded function: When a thrust::host_vector<float2> is passed runs on the CPU
 *
 * @param phases The device vector where phases are written to this one
 * @param freq The device vector to all mixing frequencies
 * @param fs The sampling frequency
 * @param nsamples The number of samples for which to calculate the current phase
 * @param precision Clip the phase to zero (if phase < precision then phase = 0)
 */
void phase(
    thrust::host_vector<float2>& phases,
    thrust::host_vector<float>& freq,
    std::size_t fs,
    std::size_t nsamples);


/**
 * @brief Generates a signal with random samples on the CPU
 *
 * @note Overloaded function: When a thrust::host_vector<T> is passed runs on the CPU
 *
 * @tparam T type of the resulting signal
 * @param data The thrust::host_vector where data is written to
 * @param mean The mean
 * @param sigma The variance
 */
template<typename T>
void random(
    thrust::host_vector<T>& data,
    int mean,
    int sigma);


/**
 * @brief Generates a signal with random samples on the CPU but copies the result to the GPU.
 *
 * @note Overloaded function: When a thrust::host_device<T> is passed the data is available on the GPU
 *
 * @tparam T type of the resulting signal
 * @param data The thrust::host_vector where data is written to
 * @param mean The mean
 * @param sigma The variance
 */
template<typename T>
void random(
    thrust::device_vector<T>& data,
    int mean,
    int sigma);


/**
 * @brief Wrapper for the GPU kernel to generate an array of oscillation frequencies 1D or 2D
 *
 * @tparam T the data type to define the preicision (either float or double)
 * @param data The data pointer to the array (samples are written to this one)
 * @param lo The data pointer containing the frequencies of the oscillation
 * @param fs The sampling rate
 * @param offset The time offset in samples
 */
template<typename T>
void oscillate(
    thrust::device_vector<float2>& data,
    thrust::device_vector<float>& f_lo,
    std::size_t fs,
    int offset=0);


/**
 * @brief The host implementation of the oscillation function
 *
 * @tparam T the data type to define the preicision (either float or double)
 * @param data The data pointer to the array (samples are written to this one)
 * @param lo The data pointer containing the frequencies of the oscillation
 * @param fs The sampling rate
 * @param offset The time offset in samples
 */
template<typename T>
void oscillate(
    thrust::host_vector<float2>& data,
    thrust::host_vector<float>& f_lo,
    std::size_t fs,
    int offset=0);

}
}
}

#include "edd_dbbc/ddc/detail/signal.cu"