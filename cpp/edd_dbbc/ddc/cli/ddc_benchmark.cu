#include <benchmark/benchmark.h>
#include <cmath>
#include <psrdada_cpp/testing_tools/streams.hpp>
#include <psrdada_cpp/raw_bytes.hpp>
#include "edd_dbbc/ddc/pipeline.cuh"
#include "edd_dbbc/ddc/ddc.cuh"

#include "psrdada_cpp/testing_tools/benchmark_tools.hpp"
#include "psrdada_cpp/testing_tools/streams.hpp"

using namespace edd_dbbc::ddc;
namespace psr_testing = psrdada_cpp::testing_tools;

std::size_t current_rate = 0;
bool bottleneck = false;

std::size_t nsamples(std::size_t fs_in, std::size_t fs_dw, std::size_t mult=256, std::size_t mini=16777216)
{
    std::size_t up = std::lcm(fs_in, fs_dw) / fs_in;
    std::size_t dw = std::lcm(fs_in, fs_dw) / fs_dw;
    std::size_t nsamples = std::lcm(up, dw);
    while(nsamples < mini){
        nsamples *= 2;
    }
    return nsamples;
}

std::string getLocalOsci(int count)
{
    std::string result = "";
    for (int i = 0; i < count; i++)
    {
        result += std::to_string(4000+i);
        if (i < count-1)
            result += ",";
    }
    return result;
}

void mix_poly_resample_benchmark(benchmark::State& state)
{
    ddc_t config;
    cudaEvent_t start, stop;
    cudaStream_t stream;
    config.fs_in = state.range(0);
    config.fs_dw = state.range(1);
    config.f_lo = getLocalOsci(state.range(2));
    config.input_size = nsamples(config.fs_in, config.fs_dw, 16, 16777216);
    config.lowpass.f_cutoff = 1.0 / config.down();
    int mult = 5;
    if(current_rate != config.fs_in)
    {
        bottleneck = false;
        current_rate = config.fs_in;
    }
    if(bottleneck)
    {
        return;
    }
    while(config.down() * mult > 4096)
    {
        mult -= 1;
    }
    config.lowpass.ntaps = config.down() * mult;
    if(config.lowpass.ntaps%2 == 0){config.lowpass.ntaps -= 1;}
    thrust::device_vector<float> idata(config.input_size);
    thrust::device_vector<float> filter(config.lowpass.ntaps);
    thrust::device_vector<float> lo = config.lo_list();
    thrust::device_vector<float2> oscillation(config.input_size * lo.size());
    thrust::device_vector<float2> odata(config.output_size());
    filter::firwin(filter, config.lowpass);
    filter::reshape_poly_fir(filter, config.up(), config.down());
    CUDA_ERROR_CHECK(cudaStreamCreate(&stream));
    CUDA_ERROR_CHECK(cudaEventCreate(&start));
    CUDA_ERROR_CHECK(cudaEventCreate(&stop));
    float milliseconds = 0;
    std::vector<double> vec(0);
    // Warm up
    for(int i = 0; i < 5; i++)
    {
        processing::mix_poly_resample(
            idata,
            filter,
            lo,
            oscillation,
            odata,
            config.up(), config.down(),
            config.lowpass.ntaps,
            .0, stream);
    }
    for (auto _ : state)
    {
        CUDA_ERROR_CHECK(cudaEventRecord(start, stream));
        processing::mix_poly_resample(
            idata,
            filter,
            lo,
            oscillation,
            odata,
            config.up(), config.down(),
            config.lowpass.ntaps,
            .0, stream);
        CUDA_ERROR_CHECK(cudaEventRecord(stop, stream));
        CUDA_ERROR_CHECK(cudaEventSynchronize(stop));
        CUDA_ERROR_CHECK(cudaEventElapsedTime(&milliseconds, start, stop));
        state.SetIterationTime(milliseconds / 1000);
        vec.push_back(idata.size() / (milliseconds / 1000));
    }
    state.counters["rate"] = benchmark::Counter(idata.size(), benchmark::Counter::kIsIterationInvariantRate);
    state.counters["byte-size"] = idata.size();
    state.counters["fs_in"] = config.fs_in;
    state.counters["fs_dw"] = config.fs_dw;
    // state.counters["f_up"] = config.up();
    // state.counters["f_down"] = config.down();
    // state.counters["ntaps"] = config.lowpass.ntaps;
    state.counters["bands"] = config.nsignals();
    CUDA_ERROR_CHECK(cudaStreamDestroy(stream));

    if(benchmark::Counter(idata.size(), benchmark::Counter::kIsIterationInvariantRate) < config.fs_in)
    {
        bottleneck = true;
    }
}


void BM_MIX_POLY_RESAMPLE(benchmark::State& state)
{
    mix_poly_resample_benchmark(state);
}

BENCHMARK(BM_MIX_POLY_RESAMPLE)->ArgsProduct({
    {1024000000, 2000000000, 3600000000, 4096000000},
    // {1000000000, 1024000000, 1800000000, 2000000000, 2048000000, 3600000000, 4000000000, 4096000000},
    {1000000, 4000000, 16000000, /*64000000, 128000000,*/ 256000000},
    {1/*,2,4*/}})->Iterations(25)->UseManualTime();


void BM_DDC(benchmark::State& state)
{
    cudaEvent_t start, stop;
    pipe_config config({0, 1024000000, 1000000, "4000", true, {301, 0.05, 0.05, 0.45}, {0, 2, "hamming", 0}}, 8, 134217728, 1048576, 0);

    config.ddc.fs_in = state.range(0);
    config.ddc.fs_dw = state.range(1);
    config.ddc.f_lo = getLocalOsci(state.range(2));
    // config.nbit = state.range(3);
    std::size_t nsample = nsamples(config.ddc.fs_in, config.ddc.fs_dw, 16, 16777216);
    config.input_size(nsample * config.nbit() / 8);
    config.output_size(nsample * config.ddc.up() * config.ddc.nsignals() * 2 * sizeof(float) / config.ddc.down());
    std::size_t buffer_size = config.input_size();
    char* hdata = new char[4096];
    char* idata = nullptr;
    CUDA_ERROR_CHECK(cudaMallocHost((void**)&idata, buffer_size));

    psrdada_cpp::RawBytes header_raw(hdata, 4096, 4096);
    psrdada_cpp::RawBytes rawdata(idata, buffer_size, buffer_size);
    psr_testing::DummyStream<float> dummy(false);

    CUDA_ERROR_CHECK(cudaEventCreate(&start));
    CUDA_ERROR_CHECK(cudaEventCreate(&stop));
    float milliseconds = 0;
    std::vector<double> vec(0);
    // Warm up
    try{
        Pipeline<decltype(dummy), uint64_t> test_object(config, dummy);
        while(dummy.call_count() < 5)
        {
            test_object(rawdata);
        }
        for (auto _ : state)
        {
            CUDA_ERROR_CHECK(cudaEventRecord(start));
            test_object(rawdata);
            CUDA_ERROR_CHECK(cudaEventRecord(stop));
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&milliseconds, start, stop);
            state.SetIterationTime(milliseconds / 1000);
            vec.push_back(buffer_size / (milliseconds / 1000));
        }
        state.counters["rate"] = benchmark::Counter(buffer_size, benchmark::Counter::kIsIterationInvariantRate);
        state.counters["byte-size"] = buffer_size;
        state.counters["fs_in"] = test_object.ddc_config().fs_in;
        state.counters["fs_dw"] = test_object.ddc_config().fs_dw;
        state.counters["ntaps"] = test_object.ddc_config().lowpass.ntaps;
        state.counters["nlo"] = test_object.ddc_config().nsignals();
        psr_testing::getStats(vec, state);
    }catch(std::exception& e){
        state.counters["rate"] = 0;
        state.counters["byte-size"] = 0;
        state.counters["fs_in"] = 0;
        state.counters["fs_dw"] = 0;
        state.counters["ntaps"] = 0;
        state.counters["nlo"] = 0;
        psr_testing::getStats(vec, state);
    }
    sleep(2);
    CUDA_ERROR_CHECK(cudaFreeHost(static_cast<void*>(idata)));
    delete[] hdata;
}

BENCHMARK(BM_DDC)->ArgsProduct({
    {1024000000, 2000000000, 3600000000, 4096000000},
    // {1000000000, 1024000000, 1800000000, 2000000000, 2048000000, 3600000000, 4000000000, 4096000000},
    {1000000, 4000000, 16000000, /*64000000, 128000000,*/ 256000000},
    {1/*,2,4*/}})->Iterations(25)->UseManualTime();

int main(int argc, char** argv) {
    char arg0_default[] = "benchmark";
    char* args_default = arg0_default;
    psrdada_cpp::set_log_level("ERROR");
    if (!argv)
    {
        argc = 1;
        argv = &args_default;
    }
    ::benchmark::Initialize(&argc, argv);
    if (::benchmark::ReportUnrecognizedArguments(argc, argv)) return 1;
    ::benchmark::RunSpecifiedBenchmarks();
    ::benchmark::Shutdown();
    return 0;
}
