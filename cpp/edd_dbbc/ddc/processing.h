/**
 * @file    processing.h
 * @author  Niclas Esser (nesser@mpifr-bonn.mpg.de)
 * @brief   The processing.h file contains algorithms used to run digital downconversion.
 *          Template functions are implemented in edd_dbbc/ddc/detail/processing.cu 
 *          Non-template functions are implemented in edd_dbbc/ddc/src/processing.cu 
 * @version 0.1
 * @date    2024-04-25
 *
 */
#pragma once

#include <cuda.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <psrdada_cpp/cuda_utils.hpp>


namespace edd_dbbc {
namespace ddc {
namespace processing {

// Constants
const unsigned N_SM = 512;
const unsigned N_THREADS = 1024;
const unsigned WARP_SIZE = 32;
const unsigned N_WARPS = 32;
const unsigned MAX_SMEM_SIZE = 49152;

// GPU kernel defintion
namespace kernel {

template<typename T>__global__ void upsample(const T* idata, T* odata, unsigned up, unsigned width);
template<typename T>__global__ void downsample(const T* idata, T* odata, unsigned down, unsigned width);
template<typename T>__global__ void poly_resample(const T* idata, const T* lfilter, T* odata, unsigned up, unsigned down, unsigned width, unsigned ntaps);
template<typename T> __global__ void mat_vec_mult(T* matrix, const T* vector, unsigned width, unsigned height);

__global__ void convolve(const float* idata, const float* lfilter, float* odata, unsigned filter_size, unsigned width);
__global__ void convolve(const float2* idata, const float* lfilter, float2* odata, unsigned filter_size, unsigned width);
__global__ void convolve_hilbert(const float2* idata, const float* lfilter, float* odata, unsigned filter_size, unsigned width);
__global__ void mat_vec_mult(const float2* matrix, const float* vector, float2* odata, float2* alpha, unsigned width, unsigned height);
__global__ void poly_resample(const float2* idata, const float* lfilter, float2* odata, unsigned up, unsigned down, unsigned width, unsigned ntaps);
__global__ void mix_poly_resample(const float* idata, const float* filter, const float* f_lo, const float2* oscillations,
    float2* odata, const int up, const int down, const int width, const int height, const int half_fir, const int ntaps_pad, const float t0);

} // namespace kernel

__host__ __device__ float2 cmult(float2 a, float2 b, float c);

/*------------------------*/
/*  Convolution functions */
/*------------------------*/
/**
 * @brief Convolves input with filter of the same type using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional
 * @param filter The second signal - 1 dimension
 * @param output The convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 */
void convolve(
    const thrust::device_vector<float>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float>& output,
    std::size_t n_signals=1,
    cudaStream_t stream=NULL);


/**
 * @brief Convolves input with filter of the same type using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional
 * @param filter The second signal - 1 dimension
 * @param output The convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 */
void convolve(
    const thrust::host_vector<float>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float>& output,
    std::size_t n_signals=1);


/**
 * @brief Complex-real convolution using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param filter The second signal - 1 dimension of real type
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 */
void convolve(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float2>& output,
    std::size_t n_signals=1,
    cudaStream_t stream=NULL);


/**
 * @brief Complex-real convolution using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param filter The second signal - 1 dimension of real type
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 */
void convolve(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    std::size_t n_signals=1);


/**
 * @brief Complex-real convolution using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param bfilter The hilbert bandpass filter
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 */
void convolve_hilbert(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& bfilter,
    thrust::device_vector<float>& output,
    std::size_t n_signals=1,
    cudaStream_t stream=NULL);


/**
 * @brief Complex-real convolution using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param bfilter The hilbert bandpass filter
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 */
void convolve_hilbert(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& bfilter,
    thrust::host_vector<float>& output,
    std::size_t n_signals=1);


/*--------------------------*/
/*  Up/Downsample functions */
/*--------------------------*/
/**
 * @brief Upsamples a signal by zero filling on the GPU
 *
 * @param input The input signal to upsample
 * @param output The upsampled output signal
 * @param up The upsample factor
 */
template<typename T>
void upsample(
    const thrust::device_vector<T>& input,
    thrust::device_vector<T>& output,
    std::size_t up,
    std::size_t n_signals = 1,
    cudaStream_t stream=NULL);


/**
 * @brief Upsamples a signal by zero filling on the CPU
 *
 * @param input The input signal to upsample
 * @param output The upsampled output signal
 * @param up The upsample factor
 */
template<typename T>
void upsample(
    const thrust::host_vector<T>& input,
    thrust::host_vector<T>& output,
    std::size_t up,
    std::size_t n_signals = 1);


/**
 * @brief Downsamples a signal by skipping on the GPU
 *
 * @param input The input signal to downsample
 * @param output The downsampled output signal
 * @param up The downsample factor
 */
template<typename T>
void downsample(
    const thrust::device_vector<T>& input,
    thrust::device_vector<T>& output,
    std::size_t down,
    std::size_t n_signals = 1,
    cudaStream_t stream=NULL);


/**
 * @brief Downsamples a signal by skipping on the GPU
 *
 * @param input The input signal to downsample
 * @param output The downsampled output signal
 * @param up The downsample factor
 */
template<typename T>
void downsample(
    const thrust::host_vector<T>& input,
    thrust::host_vector<T>& output,
    std::size_t down,
    std::size_t n_signals = 1);


/*---------------------*/
/*  Resample functions */
/*---------------------*/

/**
 * @brief Naive rational resampling
 *
 * @note This is a very naive and slow resampling approach. It will allocate temporary buffers
 *
 * @param input The complex input signal
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
template<typename VectorType, typename FilterType>
void resample(
    const VectorType& input,
    const FilterType& lfilter,
    VectorType& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1);


/**
 * @brief Rational resampling using a polyphase filter on the CPU - faster and efficent
 *
 * @param input The complex input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
void poly_resample(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1);


/**
 * @brief Rational resampling using a polyphase filter on the CPU - faster and efficent
 *
 * @param input The real input signal - 1 or 2 dimensional
 * @param filter The real FIR filter
 * @param output The real output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
template<typename T>
void poly_resample(
    const thrust::host_vector<T>& input,
    const thrust::host_vector<T>& lfilter,
    thrust::host_vector<T>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1);


/**
 * @brief Rational resampling using a polyphase filter on the GPU - faster and efficent
 *
 * @param input The complex input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
void poly_resample(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& lfilter,
    thrust::device_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1,
    cudaStream_t stream=NULL);


/**
 * @brief Rational resampling using a polyphase filter on the GPU - faster and efficent
 *
 * @param input The real input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The real output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
template<typename T>
void poly_resample(
    const thrust::device_vector<T>& input,
    const thrust::device_vector<T>& lfilter,
    thrust::device_vector<T>& output,
    std::size_t up,
    std::size_t down,
    std::size_t n_signal = 1,
    cudaStream_t stream=NULL);


/**
 * @brief Rational resampling and mixing using a polyphase filter on the GPU - faster and efficent
 *
 * @param input The real input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The real output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
void mix_poly_resample(
    const thrust::device_vector<float>& input,
    const thrust::device_vector<float>& lfilter,
    const thrust::device_vector<float>& f_lo,
    const thrust::device_vector<float2>& oscillations,
    thrust::device_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    std::size_t ntaps,
    double t0 = .0,
    cudaStream_t stream=NULL);


/**
 * @brief Elementwise multiplication of matrix and vector on the GPU
 *
 * @note Overloaded function: Either runs on host or GPU - depending on the thrust vectors
 *
 * @param matrix the input matrix
 * @param vector the input vector
 * @param output the output matrix
 * @param alpha  the scalar to multiply element with
 * @param stream the cudaStream_t used to process. Defaults to NULL.
 */
void mat_vec_mult(
    thrust::device_vector<float2>& matrix,
    thrust::device_vector<float>& vector,
    thrust::device_vector<float2>& output,
    thrust::device_vector<float2>& alpha,
    cudaStream_t stream=NULL);


/**
 * @brief elementwise multiplication of matrix and vector (in-place) on the GPU
 *
 * @tparam T the type of items in matrix and vector
 * @param matrix the input matrix
 * @param vector the input vector
 * @param stream the cudaStream_t used to process. Defaults to NULL.
 */
template<typename T>
void mat_vec_mult(
    thrust::device_vector<T>& matrix,
    thrust::device_vector<T>& vector,
    cudaStream_t stream=NULL);


/**
 * @brief Elementwise multiplication of matrix and vector on the CPU
 *
 * @note Overloaded function: Either runs on host or GPU - depending on the thrust vectors
 *
 * @param matrix the input matrix
 * @param vector the input vector
 * @param output the output matrix
 * @param alpha  the scalar to multiply element with
 * @param stream the cudaStream_t used to process. Defaults to NULL.
 */
void mat_vec_mult(
    thrust::host_vector<float2>& matrix,
    thrust::host_vector<float>& vector,
    thrust::host_vector<float2>& output,
    thrust::host_vector<float2>& alpha);


/**
 * @brief elementwise multiplication of matrix and vector (in-place) on the CPU
 *
 * @tparam T the type of items in matrix and vector
 * @param matrix the input matrix
 * @param vector the input vector
 */
void mat_vec_mult(
    thrust::host_vector<float2>& matrix,
    thrust::host_vector<float>& vector);


}
}
}

#include "edd_dbbc/ddc/detail/processing.cu"