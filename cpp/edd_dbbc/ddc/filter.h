/**
 * @file    filter.h
 * @author  Niclas Esser (nesser@mpifr-bonn.mpg.de)
 * @brief   The filter.h file contains algorithms used to run digital downconversion.
 *          Template functions are implemented in edd_dbbc/ddc/detail/filter.cu 
 *          Non-template functions are implemented in edd_dbbc/ddc/src/filter.cu 
 * @version 0.1
 * @date    2024-04-25
 *
 */
#pragma once

#include <cuda.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include "edd_dbbc/ddc/signal.cuh"


namespace edd_dbbc {
namespace ddc {
namespace filter {
namespace kernel {

__global__ void firhilbert(float* data, int length, float a, float w1, float w2);
template<typename T>__global__ void firwin(T* data, T* window, std::size_t length, float f_cutoff);
template<typename T>__global__ void window_hamming(T* idata, int length);
template<typename T>__global__ void window_hann(T* idata, int length);
template<typename T>__global__ void window_bartlett(T* idata, int length);
template<typename T>__global__ void window_blackman(T* idata, int length);
}

struct hilbert_t
{
    std::size_t ntaps = 301;
    float a = 0.05f;
    float w1 = 0.05f;
    float w2 = 0.45f;
};

struct lowpass_t
{
    std::size_t ntaps = 0;
    std::size_t mult = 10;
    std::string window = "hamming";
    double f_cutoff;
};


void firhilbert(
    thrust::device_vector<float>& data,
    float a=0.05,
    float w1=0.05,
    float w2=0.45);


void firhilbert(
    thrust::host_vector<float>& data,
    float a=0.05,
    float w1=0.05,
    float w2=0.45);


void reshape_poly_fir(
    thrust::host_vector<float>& fir,
    const std::size_t up,
    const std::size_t down,
    const std::size_t pad=32);


void reshape_poly_fir(
    thrust::device_vector<float>& fir,
    const std::size_t up,
    const std::size_t down,
    const std::size_t pad=32);


template<typename VectorType>
void firhilbert(
    VectorType& data, 
    hilbert_t conf);


template<typename T>
void window(
    thrust::device_vector<T>& data,
    std::string window_name);


template<typename T>
void window(
    thrust::host_vector<T>& data,
    std::string window_name);


template<typename T>
void firwin(
    thrust::device_vector<T>& data,
    float f_cutoff=0.25,
    std::string window="hamming");


template<typename T>
void firwin(
    thrust::host_vector<T>& data,
    float f_cutoff=0.25,
    std::string window="hamming");


template<typename VectorType>
void firwin(
    VectorType& data,
    lowpass_t conf);


}
}
}

#include "edd_dbbc/ddc/detail/filter.cu"