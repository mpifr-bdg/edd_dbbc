#pragma once

#include <random>
#include <cmath>
#include <cassert>
#include <vector>
#include <cuda.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform_reduce.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>

#include <psrdada_cpp/raw_bytes.hpp>

namespace edd_dbbc{
namespace tools{


/**
 * @brief A thrust unary function to convert between data types
 *
 * @tparam T Input type
 * @tparam U Output type
 */
template<typename T, typename U>
struct conversion_unary : public thrust::unary_function<T, U>
{
    __host__ __device__
    U operator()(const T& x) const
    {
        return static_cast<U>(x);
    }
};

/**
 * @brief Converts a thrust::host_vector from type T to type U.
 *          The function allocates a new thrust::host_vector and returns it
 *
 * @tparam T Input type
 * @tparam U Output type
 * @param idata The vector to convert from
 * @return thrust::host_vector<U>
 */
template<typename T, typename U>
thrust::host_vector<U> conversion(thrust::host_vector<T> idata)
{
    thrust::host_vector<U> odata(idata.size());
    thrust::transform(idata.begin(), idata.end(), odata.begin(), conversion_unary<T, U>());
    return odata;
}

/**
 * @brief Converts a thrust::device_vector from type T to type U.
 *          The function allocates a new thrust::device_vector and returns it
 *
 * @tparam T Input type
 * @tparam U Output type
 * @param idata The vector to convert from
 * @return thrust::device_vector<U>
 */
template<typename T, typename U>
thrust::device_vector<U> conversion(thrust::device_vector<T> idata)
{
    thrust::device_vector<U> odata(idata.size());
    thrust::transform(idata.begin(), idata.end(), odata.begin(), conversion_unary<T, U>());
    return odata;
}

/**
 * @brief Shifts or ravels the items of a vector. The function allocates a new vector which is returned as output
 *
 * @tparam T The data type of the vector items
 * @param input The input vector
 * @param shift The shift size
 * @param right Shift direction if true shifts to the right, false shift to the left. Default is true
 * @return thrust::host_vector<T>
 */
template <typename T>
thrust::host_vector<T> shift(const thrust::host_vector<T>& input, int shift, bool right=true)
{
    if(shift == 0){
        return input;
    }
    int size = input.size();
    shift = shift % size; // Ensure shift is within the valid range
    thrust::host_vector<T> output(size);
    if(right){
        thrust::copy(input.end() - shift, input.end(), output.begin());
        thrust::copy(input.begin(), input.end() - shift, output.begin() + shift);
    }else{
        thrust::copy(input.begin() + shift, input.end(), output.begin());
        thrust::copy(input.begin(), input.begin() + shift, output.begin() + size - shift);
    }
    return output;
}


/**
 * @brief Load data into a given vector from disk
 *
 * @tparam T Type oh the data vector
 * @param data Vector to load the data
 * @param fname file name to read
 * @param nsample number of samples to read, defaults to -1 (= whole file)
 * @return true on success
 * @return false on fail
 */
template<typename T>
bool load_vector(thrust::host_vector<T>& data, std::string fname, int nsample=-1)
{
    std::ifstream file(fname, std::ios::binary);
    if (!file.is_open()){
        std::cerr << "Failed to open file for writing" << std::endl;
        return false;
    }
    file.seekg(0, std::ios::end);
    std::streampos file_size = file.tellg();
    file.seekg(0, std::ios::beg);
    std::size_t tot_nsample = file_size / sizeof(T);
    if(nsample < 0){
        nsample = tot_nsample;
    }
    if(nsample > (int)tot_nsample){
        std::cerr << "File " << fname << " contains only " << tot_nsample << std::endl;
        return false;
    }
    if(data.size() != (std::size_t)nsample){
        data.resize(nsample);
    }
    std::cout << "Reading " << nsample << "/" << tot_nsample << " samples from " << fname << std::endl;
    file.read(reinterpret_cast<char*>(data.data()), nsample * sizeof(T));
    file.close();
    return true;
}

/**
 * @brief Stores a thrust::host_vector to disk as binary
 *
 * @tparam T Type of the vector
 * @param data Vector containing the data
 * @param fname file name to store
 * @return true on success
 * @return false on fail
 */
template<typename T>
bool save_vector(thrust::host_vector<T>& data, std::string fname="data.bin")
{
    std::ofstream file(fname, std::ios::binary);
    if (!file.is_open())
    {
        std::cerr << "Failed to open file for writing" << std::endl;
        return false;
    }
    std::cout << "Writing " << data.size() * sizeof(T) / 1024 << " KB to " << fname << std::endl;
    file.write(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(T));
    file.close();
    return true;
}

/**
 * @brief Stores a thrust::host_vector to disk as binary
 *
 * @tparam T Type of the vector
 * @param data Vector containing the data
 * @param fname file name to store
 * @return true on success
 * @return false on fail
 */
template<typename T>
bool save_vector(thrust::device_vector<T>& data, std::string fname="data.bin")
{
    thrust::host_vector<T> idata = data;
    return save_vector<T>(idata, fname);
}


}
}