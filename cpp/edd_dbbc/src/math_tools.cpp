#include "edd_dbbc/math_tools.cuh"

namespace edd_dbbc {
namespace tools {


bool is_fractional(double value, double epsilon)
{
    return std::abs(value - std::round(value)) > epsilon;
}

}
}