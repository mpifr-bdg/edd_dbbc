#pragma once

#include <gtest/gtest.h>
#include <cuda.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <psrdada_cpp/testing_tools/tools.cuh>

#include "edd_dbbc/testing_tools.cuh"
#include "edd_dbbc/math_tools.cuh"