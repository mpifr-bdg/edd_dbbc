#include "edd_dbbc/test/test_math_tools.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace tools {
namespace test {

TEST(mean, mean_test_double)
{
  int mean = 27; int sigma = 13;
  thrust::device_vector<double> data = psr_testing::random_vector<double>(mean, sigma, 1000);
  double result = edd_dbbc::tools::mean(data);
  ASSERT_TRUE(result + result * 0.1 >= mean && result - result * 0.1 <= mean)
    << "Expected mean is " << mean << ", but actual value is " << result << std::endl;
}

TEST(mean, mean_test_float)
{
  int mean = -11; int sigma = 3;
  thrust::device_vector<float> data = psr_testing::random_vector<float>(mean, sigma, 1000);
  float result = edd_dbbc::tools::mean(data); // Execute function under test
  ASSERT_TRUE(abs(result + result * 0.1) >= abs(mean) && abs(result - result * 0.1) <= abs(mean))
    << "Expected mean is " << mean << ", but actual value is " << result << std::endl;
}

TEST(mean, mean_test_int)
{
  int mean = 9; int sigma = 7;
  thrust::device_vector<int> data = psr_testing::random_vector<int>(mean, sigma, 1000);
  int result = edd_dbbc::tools::mean(data);
  ASSERT_TRUE(result == mean)
    << "Expected mean is " << mean << ", but actual value is " << result << std::endl;
}

TEST(mean, mean_test_double_multi_channel)
{
    int nsamples = 10000; int nchan = 4;
    thrust::host_vector<int>mean = psr_testing::random_vector<int>(0, 100, nchan);
    thrust::host_vector<int>sigma = psr_testing::random_vector<int>(0, 100, nchan);
    thrust::device_vector<double> data(nsamples * nchan);
    thrust::device_vector<double> result(nchan);
    for(int i = 0; i < nchan; i++)
    {
        thrust::device_vector<double> tmp = psr_testing::random_vector<double>(mean[i], sigma[i], nsamples);
        thrust::copy(tmp.begin(), tmp.end(), data.begin() + nsamples * i);
    }
    edd_dbbc::tools::mean(data, result);
    for(int i = 0; i < nchan; i++)
    {
        ASSERT_TRUE(abs(result[i] + result[i] * 0.1) >= abs(mean[i])
                && abs(result[i] - result[i] * 0.1) <= abs(mean[i]))
            << "Expected mean is " << mean[i] << ", but actual value is " << result[i] << "(i = "
            << i << ")" << std::endl;
    }
}

TEST(mean, mean_test_float_multi_channel)
{
    int nsamples = 10000; int nchan = 4;
    thrust::host_vector<int>mean = psr_testing::random_vector<int>(0, 100, nchan);
    thrust::host_vector<int>sigma = psr_testing::random_vector<int>(0, 100, nchan);
    thrust::device_vector<float> data(nsamples * nchan);
    thrust::device_vector<float> result(nchan);
    for(int i = 0; i < nchan; i++)
    {
        thrust::device_vector<float> tmp = psr_testing::random_vector<float>(mean[i], sigma[i], nsamples);
        thrust::copy(tmp.begin(), tmp.end(), data.begin() + nsamples * i);
    }
    edd_dbbc::tools::mean(data, result);
    for(int i = 0; i < nchan; i++)
    {
        ASSERT_TRUE(abs(result[i] + result[i] * 0.1) >= abs(mean[i])
                && abs(result[i] - result[i] * 0.1) <= abs(mean[i]))
            << "Expected mean is " << mean[i] << ", but actual value is " << result[i] << "(i = "
            << i << ")" << std::endl;
    }
}

TEST(mean, mean_test_int_multi_channel)
{
    int nsamples = 10000; int nchan = 4;
    thrust::host_vector<int>mean = psr_testing::random_vector<int>(20, 10, nchan);
    thrust::host_vector<int>sigma = psr_testing::random_vector<int>(20, 10, nchan);
    thrust::device_vector<int> data(nsamples * nchan);
    thrust::device_vector<int> result(nchan);
    for(int i = 0; i < nchan; i++)
    {
        thrust::device_vector<int> tmp = psr_testing::random_vector<int>(mean[i], sigma[i], nsamples);
        thrust::copy(tmp.begin(), tmp.end(), data.begin() + nsamples * i);
    }
    edd_dbbc::tools::mean(data, result);
    for(int i = 0; i < nchan; i++)
    {
        ASSERT_TRUE(result[i] == mean[i]) << "Expected mean is " << mean[i]
            << ", but actual value is " << result[i] << "(i = " << i << ")" << std::endl;
    }
}

TEST(variance, variance_test_float)
{
  int mean = 5; int sigma = 10;
  thrust::device_vector<float> data = psr_testing::random_vector<float>(mean, sigma, 1000);
  float result = edd_dbbc::tools::variance(data, (float)(mean));
  ASSERT_TRUE(result + result * 0.1 >= sigma*sigma && result - result * 0.1 <= sigma*sigma)
    << "Expected variance is " << sigma*sigma << ", but actual value is " << result << std::endl;
}

TEST(variance, variance_test_double)
{
  int mean = 1000; int sigma = 200;
  thrust::device_vector<double> data = psr_testing::random_vector<double>(mean, sigma, 1000);
  double result = edd_dbbc::tools::variance(data, (double)(mean));
  ASSERT_TRUE(result + result * 0.1 >= sigma*sigma && result - result * 0.1 <= sigma*sigma)
    << "Expected variance is " << sigma*sigma << ", but actual value is " << result << std::endl;
}

TEST(variance, variance_test_int)
{
  int mean = 10; int sigma = 3;
  thrust::device_vector<int> data = psr_testing::random_vector<int>(mean, sigma, 1000);
  int result = edd_dbbc::tools::variance(data, mean) + 1;
  ASSERT_TRUE(result == sigma*sigma)
    << "Expected variance is " << sigma*sigma << ", but actual value is " << result << std::endl;
}

}
}
}