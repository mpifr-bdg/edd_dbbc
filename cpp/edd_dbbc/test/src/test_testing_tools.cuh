#include <gtest/gtest.h>
#include <cuda.h>
#include <filesystem>
#include <thrust/host_vector.h>
#include <psrdada_cpp/testing_tools/tools.cuh>

#include "edd_dbbc/testing_tools.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace tools {
namespace test {

TEST(shift, shift_right_test)
{
  float arr[] = {0,1,2,3,4,5,6,7,8,9};
  float expect[] = {7,8,9,0,1,2,3,4,5,6};
  thrust::host_vector<float> idata(arr, arr + 10);
  thrust::host_vector<float> actual = tools::shift(idata, 3);
  for(std::size_t i = 0; i < actual.size(); i++){
    ASSERT_TRUE(expect[i] == actual[i])
        << "Expected value is " << expect[i] << " but actual value is " << actual[i] << std::endl;
  }
}

TEST(shift, shift_left_test)
{
  float arr[] = {0,1,2,3,4,5,6,7,8,9};
  float expect[] = {3,4,5,6,7,8,9,0,1,2};
  thrust::host_vector<float> idata(arr, arr + 10);
  thrust::host_vector<float> actual = tools::shift(idata, 3, 0);
  for(std::size_t i = 0; i < actual.size(); i++){
    ASSERT_TRUE(expect[i] == actual[i])
        << "Expected value is " << expect[i] << " but actual value is " << actual[i] << std::endl;
  }
}


TEST(store_load, store_load_float)
{
    std::size_t size = 2000;
    thrust::host_vector<float> o_vector(size);
    thrust::host_vector<float> i_vector = psr_testing::random_vector<float>(10, 20, size);
    ASSERT_TRUE(tools::save_vector(i_vector, "tmp.dat"));
    ASSERT_TRUE(tools::load_vector(o_vector, "tmp.dat"));
    std::filesystem::remove("tmp.dat");
    for(std::size_t i = 0; i < i_vector.size(); i++){
        ASSERT_EQ(i_vector[i], o_vector[i]);
    }
}

// TEST(store_load, store_load_float2)
// {
//     std::size_t size = 2000;
//     thrust::host_vector<float2> o_vector(2000);
//     thrust::host_vector<float2> i_vector = psr_testing::random_vector<float2>(10, 20, size);
//     ASSERT_TRUE(tools::save_vector(i_vector, "tmp.dat"));
//     ASSERT_TRUE(tools::load_vector(o_vector, "tmp.dat"));
//     std::filesystem::remove("tmp.dat");
//     for(std::size_t i = 0; i < i_vector.size(); i++){
//         ASSERT_EQ(i_vector[i].x, o_vector[i].x);
//         ASSERT_EQ(i_vector[i].y, o_vector[i].y);
//     }
// }

TEST(store_load, store_load_int)
{
    std::size_t size = 2000;
    thrust::host_vector<int> o_vector(2000);
    thrust::host_vector<int> i_vector = psr_testing::random_vector<int>(10, 20, size);
    ASSERT_TRUE(tools::save_vector(i_vector, "tmp.dat"));
    ASSERT_TRUE(tools::load_vector(o_vector, "tmp.dat"));
    std::filesystem::remove("tmp.dat");
    for(std::size_t i = 0; i < i_vector.size(); i++){
        ASSERT_EQ(i_vector[i], o_vector[i]);
    }
}

TEST(store_load, store_load_int_half_vector)
{

    std::size_t size = 2000;
    thrust::host_vector<int> o_vector(size/2);
    thrust::host_vector<int> i_vector = psr_testing::random_vector<int>(10, 20, size);
    ASSERT_TRUE(tools::save_vector(i_vector, "tmp.dat"));
    ASSERT_TRUE(tools::load_vector(o_vector, "tmp.dat", size/2));
    std::filesystem::remove("tmp.dat");
    for(std::size_t i = 0; i < o_vector.size(); i++){
        ASSERT_EQ(i_vector[i], o_vector[i]);
    }
}

TEST(store_load_int_half_vector, store_load_int)
{
    std::size_t size = 2000;
    thrust::host_vector<int> o_vector(size/2);
    thrust::host_vector<int> i_vector = psr_testing::random_vector<int>(10, 20, size);
    ASSERT_TRUE(tools::save_vector(i_vector, "tmp.dat"));
    ASSERT_FALSE(tools::load_vector(o_vector, "tmp.dat", size+1));
    std::filesystem::remove("tmp.dat");
}


}
}
}