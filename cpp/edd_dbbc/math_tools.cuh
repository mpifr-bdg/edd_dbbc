#pragma once

#include <cuda.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform_reduce.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>
#include <thrust/iterator/iterator_traits.h>

#include <functional>
#include <cmath>

namespace edd_dbbc {
namespace tools {


bool is_fractional(double value, double epsilon = 1e-9);

/**
 * @brief Computes the mean by given Iterator objects
 *
 * @tparam T        The type of the vector
 * @tparam Iterator The Iterator type
 * @param first     Start iterator
 * @param last      End iterator
 * @param val       Addtional mean offset, defaults to 0
 * @return T        Returns the mean
 */
template<typename T, typename Iterator>
T mean(Iterator first, Iterator last, T val=0)
{
    T acc = thrust::reduce(first, last, val, thrust::plus<T>());
    return acc / thrust::distance(first, last);
}


/**
 * @brief Computes the mean of a 1D vector
 *
 * @tparam T    the type of the vector
 * @param vec   the vector
 * @param val   offset the mean
 * @return T    the mean of the vector
 */
template<typename T>
T mean(thrust::device_vector<T>& vec, T val=0)
{
    return mean<T, decltype(vec.begin())>(vec.begin(), vec.end(), val);
}

/**
 * @brief Computes the mean of a 1D or 2D vector
 *
 * @tparam T the type of the vector
 * @param idata the input vector
 * @param odata the output vector, the size of the output vector is the size of the outer dimension
 * @param val offset the mean
 */
template<typename T>
void mean(
    thrust::device_vector<T>& idata,
    thrust::device_vector<T>& odata,
    T val=0)
{
    assert(idata.size() % odata.size() == 0);
    std::size_t length = idata.size() / odata.size();
    for(std::size_t i = 0; i < odata.size(); i++)
    {
        odata[i] = mean<T, decltype(idata.begin())>(
            idata.begin() + i*length,
            idata.begin() + (i+1)*length, val);
    }
}


template<typename T>
void mean(
    thrust::host_vector<T>& idata,
    thrust::host_vector<T>& odata,
    T val=0)
{
    assert(idata.size() % odata.size() == 0);
    std::size_t length = idata.size() / odata.size();
    for(std::size_t i = 0; i < odata.size(); i++)
    {
        odata[i] = mean<T, decltype(idata.begin())>(
            idata.begin() + i*length,
            idata.begin() + (i+1)*length, val);
    }
}

/**
 * @brief Unary struct used for computing the variance of a vector
 *
 * @tparam T the type of the vector
 */
template<typename T>
struct variance_unary
{
    const T mean;
    variance_unary(T m) : mean(m){}
    __host__ __device__ T operator()(const T &data) const
    {
        return (data - mean) * (data - mean);
    }
};


/**
 * @brief Computes and returns the variance of a vector
 *
 * @tparam T        The type of the vector
 * @tparam Iterator The Iterator type
 * @param first     Start iterator
 * @param last      End iterator
 * @param mean      The mean value of the vector
 * @return T        The resulting variance
 */
template<typename T, typename Iterator>
T variance(Iterator first, Iterator last, T mean)
{

    T var = thrust::transform_reduce(
        first, last, variance_unary<T>(mean), 0.0f,
        thrust::plus<T>());
    return var / thrust::distance(first, last);
}


/**
 * @brief Computes and returns the variance of a vector
 *
 * @tparam T    The type of the vector
 * @param vec   The vector
 * @param mean  The mean value of the vector
 * @return T    The resulting variance
 */
template<typename T>
T variance(thrust::device_vector<T>& vec, T mean)
{
    return variance<T, decltype(vec.begin())>(vec.begin(), vec.end(), mean);
}

/**
 * @brief Computes the variance of a 1D or 2D vector (GPU)
 *
 * @tparam T    The type of the vector
 * @param idata The input vector
 * @param odata The output vector
 * @param mean  The mean value of the vector
 */
template<typename T>
void variance(
    thrust::device_vector<T>& idata,
    thrust::device_vector<T>& odata,
    thrust::device_vector<T>& mean)
{
    // assert(mean.size() == odata.size())
    assert(idata.size() % odata.size() == 0);
    std::size_t length = idata.size() / odata.size();
    for(std::size_t i = 0; i < odata.size(); i++)
    {
        odata[i] = variance<T, decltype(idata.begin())>(
            idata.begin() + i*length,
            idata.begin() + (i+1)*length,
            mean[i]);
    }
}

/**
 * @brief Computes the variance of a 1D or 2D vector (CPU)
 *
 * @tparam T    The type of the vector
 * @param idata The input vector
 * @param odata The output vector
 * @param mean  The mean value of the vector
 */
template<typename T>
void variance(
    thrust::host_vector<T>& idata,
    thrust::host_vector<T>& odata,
    thrust::host_vector<T>& mean)
{
    // assert(mean.size() == odata.size())
    assert(idata.size() % odata.size() == 0);
    std::size_t length = idata.size() / odata.size();
    for(std::size_t i = 0; i < odata.size(); i++)
    {
        odata[i] = variance<T, decltype(idata.begin())>(
            idata.begin() + i*length,
            idata.begin() + (i+1)*length,
            mean[i]);
    }
}


}
}