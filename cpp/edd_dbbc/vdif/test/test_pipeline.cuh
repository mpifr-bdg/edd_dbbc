#pragma once

#include <gtest/gtest.h>
#include <ctime>
#include <random>
#include <string>

#include <psrdada_cpp/testing_tools/streams.hpp>
#include <psrdada_cpp/testing_tools/tools.cuh>
#include "edd_dbbc/testing_tools.cuh"
#include "edd_dbbc/vdif/pipeline.cuh"
#include "edd_dbbc/vdif/test/test_packer.cuh"


namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace vdif {
namespace test {

template<typename InputType>
class Test_Pipeline
{
public:
    Test_Pipeline(pipe_config &config) : _config(config)
    {
        test_object.reset(
            new Pipeline<psr_testing::DummyStream<char>, InputType>(_config, vdif_header, handler));
    }
    ~Test_Pipeline(){};

    void init(psrdada_cpp::RawBytes &header)
    {
        test_object->init(header);
    }

    void init(std::size_t sample_clock_start=0, std::size_t sync_time=0)
    {
        // Create the DADA header and pass it to the pipeline under test
        if(sync_time == 0)
        {
            sync_time = static_cast<std::size_t>(std::time(nullptr));
        }
        char header[header_size] = {0};
        std::string hstring = "SAMPLE_CLOCK_START " + std::to_string(sample_clock_start) +
            "\nSYNC_TIME " + std::to_string(sync_time) + "\n";
        std::copy(hstring.cbegin(), hstring.cend(), header);
        psrdada_cpp::RawBytes bytes(header, header_size, header_size);
        // Initalize the pipeline under test with the DADA header
        test_object->init(bytes);
    }

    psrdada_cpp::RawBytes output_block()
    {
        return psrdada_cpp::RawBytes(
            reinterpret_cast<char *>(odata.data()),
            odata.size() * sizeof(uint32_t),
            odata.size() * sizeof(uint32_t)
        );
    }

    psrdada_cpp::RawBytes input_block()
    {
        return psrdada_cpp::RawBytes(
            reinterpret_cast<char *>(idata.data()),
            idata.size() * sizeof(InputType),
            idata.size() * sizeof(InputType)
        );
    }
    void stream_until_first_output_block(std::size_t nsamples)
    {
        // For the reference processing we use the PackerTester object, which implements a CPU reference packing method
        PackerTester test_packer(nsamples, _config.n_channels, _config.digitizer_threshold);
        this->init();
        // If type is not float, we need to create a random int8_t array and cast it to a uint64_t array.
        if constexpr (std::is_same<InputType, uint64_t>::value)
        {
            thrust::host_vector<int8_t> vec = psr_testing::random_vector<int8_t>(0, 32, nsamples);
            thrust::host_vector<float> input = tools::conversion<int8_t, float>(vec);
            test_packer.set_input(input);
            // reinterpret the data as uint64_t
            idata = thrust::host_vector<InputType>(
                reinterpret_cast<InputType*>(thrust::raw_pointer_cast(vec.data())),
                reinterpret_cast<InputType*>(thrust::raw_pointer_cast(vec.data())) + nsamples / 8);
        }
        else
        {
            idata = test_packer.get_input();
        }
        odata = test_packer.get_output();
        psrdada_cpp::RawBytes input_block = this->input_block();
        // Iterate until count count is 5 to get the first
        while(handler.call_count() < 1)
        {
            ASSERT_FALSE((*test_object)(input_block));
        }
    }

    void stream_delta_impulse(std::size_t nsamples)
    {
        std::size_t frame_id = 50;
        std::size_t frame_size = 8000;
        std::size_t position = frame_id * 4 * frame_size; // 16th data frame, 4 samples per 8bit, first sample of the frame
        std::size_t sync_time = 1721648808;
        std::size_t sample_clock_start = 32000000;
        double expected_time = sync_time + (sample_clock_start + position) / static_cast<double>(_config.sample_rate);
        idata = thrust::host_vector<float>(nsamples, 0);
        idata[position] = 10000000000;
        init(sample_clock_start, sync_time);
        psrdada_cpp::RawBytes input_block = this->input_block();
        while(handler.call_count() < 1)
        {
            ASSERT_FALSE((*test_object)(input_block));
        }
        char* frame_start = handler.data_ptr(frame_id*(frame_size + 32));
        vdif::Header header(reinterpret_cast<uint32_t*>(frame_start));
        double actual_time = header.getTimestamp() + header.getDataFrameNumber() * frame_size * 4 / static_cast<double>(_config.sample_rate);
        char res = frame_start[32] & 0x03;
        ASSERT_EQ(expected_time, actual_time);
        ASSERT_EQ(res, 3);
    }

    void compare_payload()
    {
        char expected, actual;
        char* reference = output_block().ptr();
        int cnt = 0;
        for(std::size_t i = 0; i < output_block().used_bytes() / _config.n_bytes_payload; i++)
        {
            for(std::size_t ii = 0; ii < _config.n_bytes_payload; ii++)
            {
                expected = reference[i * _config.n_bytes_payload + ii];
                actual = *(handler.data_ptr(i * _config.n_bytes_payload + ii + (i+1) * vdif_header_size));
                if(expected != actual){cnt++;}
                ASSERT_EQ(expected, actual)
                    << "Expected " << std::to_string(expected) << ", got " << std::to_string(actual)
                    << " at position " << std::to_string(i + ii) << std::endl;
            }
        }
        std::cout << "Unequal elements " << cnt << "/"
            << output_block().used_bytes() << " "
            << ((float)cnt)/output_block().used_bytes()*100 << "%" << std::endl;
    }

    psr_testing::DummyStream<char>& consumer()
    {
        return handler;
    }

    Pipeline<psr_testing::DummyStream<char>, InputType>& pipeline()
    {
        return *(test_object);
    }

public:
    Header vdif_header;
private:
    psr_testing::DummyStream<char>  handler;
    thrust::host_vector<InputType> idata;
    thrust::host_vector<uint32_t> odata;
    std::unique_ptr<Pipeline<psr_testing::DummyStream<char>, InputType>> test_object;
    pipe_config _config;
};


}
}
}