#include "edd_dbbc/vdif/test/test_header.h"

namespace edd_dbbc {
namespace vdif {
namespace test {

TEST(Header, getSetWord0)
{
	Header header;
	EXPECT_TRUE(header.isValid());
	header.setValid(false);
	EXPECT_FALSE(header.isValid());

	EXPECT_EQ(header.getSecondsFromReferenceEpoch(), 0u);
	header.setSecondsFromReferenceEpoch(12345);
	EXPECT_EQ(header.getSecondsFromReferenceEpoch(), 12345u);
}

TEST(Header, getSetWord1)
{
	Header header;

	EXPECT_EQ(header.getReferenceEpoch(), 0u);
	header.setReferenceEpoch(16);
	EXPECT_EQ(header.getReferenceEpoch(), 16u);

	EXPECT_EQ(header.getDataFrameNumber(), 0u);
	header.setDataFrameNumber(5);
	EXPECT_EQ(header.getDataFrameNumber(), 5u);
}

TEST(Header, getSetWord2)
{
	Header header;
	EXPECT_EQ(header.getVersionNumber(), 0u);
	header.setVersionNumber(1);
	EXPECT_EQ(header.getVersionNumber(), 1u);

	EXPECT_EQ(header.getDataFrameLength(), 0u);
	header.setDataFrameLength(1024);
	EXPECT_EQ(header.getDataFrameLength(), 1024u);

	EXPECT_EQ(header.getNumberOfChannels(), 0u);
	header.setNumberOfChannels(10);
	EXPECT_EQ(header.getNumberOfChannels(), 10u);
}

TEST(Header, getSetWord3)
{
	Header header;
	EXPECT_FALSE(header.isComplexDataType());
	header.setComplexDataType(true);
	EXPECT_TRUE(header.isComplexDataType());

	EXPECT_EQ(header.getBitsPerSample(), 0u);
	header.setBitsPerSample(13);
	EXPECT_EQ(header.getBitsPerSample(), 13u);

	EXPECT_EQ(header.getThreadId(), 0u);
	header.setThreadId(23);
	EXPECT_EQ(header.getThreadId(), 23u);

	EXPECT_EQ(header.getStationId(), 0u);
	header.setStationId(42);
	EXPECT_EQ(header.getStationId(), 42u);
}

TEST(Header, getSetWord4)
{
	Header header;

	EXPECT_EQ(header.getEdv(), 0u);
	header.setEdv(2);
	EXPECT_EQ(header.getEdv(), 2u);

	EXPECT_EQ(header.getUserWord4(), 0u);
	header.setUserWord4(1024);
	EXPECT_EQ(header.getUserWord4(), 1024u);
}

TEST(Header, getSetWord5)
{
	Header header;

	EXPECT_EQ(header.getUserWord5(), 0u);
	header.setUserWord5(24);
	EXPECT_EQ(header.getUserWord5(), 24u);
}

TEST(Header, getSetWord6)
{
	Header header;

	EXPECT_EQ(header.getUserWord6(), 0u);
	header.setUserWord6(27);
	EXPECT_EQ(header.getUserWord6(), 27u);
}

TEST(Header, getSetWord7)
{
	Header header;

	EXPECT_EQ(header.getUserWord7(), 0u);
	header.setUserWord7(54678);
	EXPECT_EQ(header.getUserWord7(), 54678u);
}

TEST(Header, testTimeStampConversion)
{
  Header header;
  size_t currentTime = 1554915838;
  header.setTimeReferencesFromTimestamp(currentTime);

  EXPECT_EQ(currentTime, header.getTimestamp()) << "Reference epoch: " << header.getReferenceEpoch() << " + " << header.getSecondsFromReferenceEpoch() << " s";

  header.setTimeReferencesFromTimestamp(946684800);
  EXPECT_EQ(0u, header.getReferenceEpoch());
  EXPECT_EQ(0u, header.getSecondsFromReferenceEpoch());

  header.setTimeReferencesFromTimestamp(962409600);
  EXPECT_EQ(1u, header.getReferenceEpoch());
  EXPECT_EQ(0u, header.getSecondsFromReferenceEpoch());

  header.setTimeReferencesFromTimestamp(962409600 + 100);
  EXPECT_EQ(1u, header.getReferenceEpoch());
  EXPECT_EQ(100u, header.getSecondsFromReferenceEpoch());
}

}
}
}