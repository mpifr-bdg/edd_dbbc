#include "edd_dbbc/vdif/test/test_packer.cuh"

namespace edd_dbbc {
namespace vdif {
namespace test {

TEST(PackerTester, reference_test_size_32768_channel_1)
{
    PackerTester tester(32768, 1, 0.5);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_32768_channel_2)
{
    PackerTester tester(32768, 2, 0.5);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_32768_channel_4)
{
    PackerTester tester(32768, 4, 0.5);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_32768_channel_8)
{
    PackerTester tester(32768, 8, 0.5);
    tester.reference_test();
};
#ifdef COMPREHENSIVE_TESTS
TEST(PackerTester, reference_test_size_65536_channel_1)
{
    PackerTester tester(65536, 1, 5.9);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_65536_channel_2)
{
    PackerTester tester(65536, 2, 5.9);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_65536_channel_4)
{
    PackerTester tester(65536, 4, 5.9);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_65536_channel_8)
{
    PackerTester tester(65536, 8, 5.9);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_65536_channel_16_expect_throw)
{
    PackerTester tester(65536, 16, 5.9);
    EXPECT_THROW(tester.reference_test(), std::runtime_error); // Not implemented
};
TEST(PackerTester, reference_test_size_2kk_channel_1)
{
    PackerTester tester(131072*16, 1, 1.2);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_2kk_channel_2)
{
    PackerTester tester(131072*16, 2, 1.2);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_2kk_channel_4)
{
    PackerTester tester(131072*16, 4, 1.2);
    tester.reference_test();
};
TEST(PackerTester, reference_test_size_2kk_channel_8)
{
    PackerTester tester(131072*16, 8, 1.2);
    tester.reference_test();
};
#endif


}
}
}
