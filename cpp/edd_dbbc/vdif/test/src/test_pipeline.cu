#include "edd_dbbc/vdif/test/test_pipeline.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace vdif {
namespace test {

void contructor_throws(pipe_config& config)
{
    Header header;
    psr_testing::DummyStream<char> handler;
    Pipeline<decltype(handler), float> pipeline(config, header, handler);
}


TEST(Test_Pipeline, test_constructor_throw)
{
    pipe_config config;
    // Channels not a power of two
    config.n_channels = 3;
    EXPECT_THROW(contructor_throws(config), std::runtime_error);
    // Bit depth and buffer bytes are not matching
    config.n_channels = 2;
    config.buffer_bytes = 27;
    config.bit_depth = 32;
    EXPECT_THROW(contructor_throws(config), std::runtime_error);
    // Buffer size is not a multiple of VDIF payload
    config.n_channels = 2;
    config.buffer_bytes = 8001*4;
    config.n_bytes_payload = 8000;
    EXPECT_THROW(contructor_throws(config), std::runtime_error);
}

TEST(Test_Pipeline, test_init_method_proof_timestamp)
{
    pipe_config config;
    Test_Pipeline<float> tester(config);
    char header[header_size] = {0};
    std::string hstring = "SAMPLE_CLOCK_START 320000000\nSYNC_TIME 1720448439\n";
    std::copy(hstring.cbegin(), hstring.cend(), header);
    psrdada_cpp::RawBytes bytes(header, header_size, header_size);
    tester.init(bytes);
    ASSERT_EQ(49, tester.pipeline().packet_header().getReferenceEpoch());
    ASSERT_EQ(656449, tester.pipeline().packet_header().getSecondsFromReferenceEpoch());
    ASSERT_TRUE(hstring == std::string(tester.consumer().header_ptr(), hstring.size()));
    ASSERT_TRUE(tester.pipeline().initialized());
}

TEST(Test_Pipeline, test_init_method_no_sample_clock_start_and_sync_time_provided)
{
    pipe_config config;
    Test_Pipeline<float> tester(config);
    char header[header_size] = {0};
    psrdada_cpp::RawBytes bytes(header, header_size, header_size);
    tester.init(bytes);
    ASSERT_FALSE(tester.pipeline().initialized());
}

TEST(Test_Pipeline, test_init_method_sample_clock_start_not_full_second_aligned)
{
    pipe_config config;
    Test_Pipeline<float> tester(config);
    char header[header_size] = {0};
    std::string hstring = "SAMPLE_CLOCK_START 64022222\nSYNC_TIME 1720448439\n";
    std::copy(hstring.cbegin(), hstring.cend(), header);
    psrdada_cpp::RawBytes bytes(header, header_size, header_size);
    EXPECT_THROW(tester.init(bytes), std::runtime_error);
}



TEST(Test_Pipeline, test_operator_method_float_channel_1)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 320000;
    config.bit_depth = 32;
    config.buffer_bytes = nsamples * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}
TEST(Test_Pipeline, test_operator_method_float_channel_2)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 320000;
    config.bit_depth = 32;
    config.n_channels = 2;
    config.buffer_bytes = nsamples * config.n_channels * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}

TEST(Test_Pipeline, test_operator_method_float_channel_4)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 320000;
    config.bit_depth = 32;
    config.n_channels = 4;
    config.buffer_bytes = nsamples * config.n_channels * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}

TEST(Test_Pipeline, test_operator_method_float_channel_8)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 320000;
    config.bit_depth = 32;
    config.n_channels = 8;
    config.buffer_bytes = nsamples * config.n_channels * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}

TEST(Test_Pipeline, test_operator_method_float_multi_second_buffer)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 320000*2;
    config.bit_depth = 32;
    config.buffer_bytes = nsamples * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}

#ifdef COMPREHENSIVE_TESTS
TEST(Test_Pipeline, test_operator_method_float_multi_second_buffer_channel_2)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 320000*10;
    config.bit_depth = 32;
    config.n_channels = 2;
    config.buffer_bytes = nsamples * config.n_channels * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}

TEST(Test_Pipeline, test_operator_method_float_multi_second_buffer_channel_4)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 320000*10;
    config.bit_depth = 32;
    config.n_channels = 4;
    config.buffer_bytes = nsamples * config.n_channels * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}

TEST(Test_Pipeline, test_operator_method_float_multi_second_buffer_channel_8)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 320000*5;
    config.bit_depth = 32;
    config.n_channels = 8;
    config.buffer_bytes = nsamples * config.n_channels * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}

TEST(Test_Pipeline, test_operator_method_float_less_second_buffer)
{
    pipe_config config;
    config.sample_rate = 320000;
    std::size_t nsamples = 8000*666;
    config.bit_depth = 32;
    config.buffer_bytes = nsamples * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}

#endif

TEST(Test_Pipeline, test_operator_method_int8_16000)
{
    pipe_config config;
    std::size_t nsamples = 8000*333;
    config.bit_depth = 8;
    config.buffer_bytes = nsamples * sizeof(int8_t);
    Test_Pipeline<uint64_t> tester(config);
    tester.stream_until_first_output_block(nsamples);
    tester.compare_payload();
}


TEST(Test_Pipeline, test_pipeline_timestamp_with_delta_impulse)
{
    pipe_config config;
    std::size_t nsamples = 32000000;
    config.bit_depth = 8;
    config.buffer_bytes = nsamples * sizeof(float);
    Test_Pipeline<float> tester(config);
    tester.stream_delta_impulse(nsamples);
}

}
}
}