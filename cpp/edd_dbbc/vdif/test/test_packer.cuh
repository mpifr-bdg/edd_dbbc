#pragma once

#include <gtest/gtest.h>
#include <numeric>

#include <psrdada_cpp/testing_tools/tools.cuh>

#include <psrdada_cpp/cuda_utils.hpp>
#include "edd_dbbc/vdif/packer.cuh"
#include "edd_dbbc/testing_tools.cuh"
#include "edd_dbbc/math_tools.cuh"

namespace psr_testing = psrdada_cpp::testing_tools;

namespace edd_dbbc {
namespace vdif {
namespace test {


struct packer_test_conf
{
    std::size_t size;
    std::size_t nchannel;
    float threshold;
};

class PackerTester
{
public:
    PackerTester(std::size_t size, std::size_t nchannel, float threshold)
    :   _size(size),
        _nchannel(nchannel),
        _threshold(threshold)
    {
        h_idata = psr_testing::random_vector<float>(0, 32, _size*_nchannel);
        stddev.resize(_nchannel);
        mean.resize(_nchannel);
        d_stddev.resize(_nchannel);
        d_mean.resize(_nchannel);
        d_idata = h_idata;
        h_odata.resize(_size*_nchannel / 16);
        d_odata.resize(_size*_nchannel / 16);
        CUDA_ERROR_CHECK(cudaStreamCreate(&_stream));
    }

    ~PackerTester()
    {
        CUDA_ERROR_CHECK(cudaStreamDestroy(_stream));
    }

    void set_input(thrust::host_vector<float> &vec)
    {
        h_idata = vec;
        d_idata = h_idata;
    }

    void cpu_process()
    {
        std::cout << "Computing mean and stddev" << std::endl;
        // We have to use the same compute hardware for mean and stddev
        // Otherwise results are slightly different
        edd_dbbc::tools::mean(d_idata, d_mean);
        edd_dbbc::tools::variance(d_idata, d_stddev, d_mean);
        mean = d_mean;
        stddev = d_stddev;
        for(std::size_t i = 0; i < _nchannel; i++)
        {
            std::cout << "CPU: channel " << i << " mean " << mean[i] << " std " << stddev[i];
            stddev[i] = _threshold * sqrt(stddev[i]);
            std::cout << " stdroot " << stddev[i] << std::endl;
        }
        uint32_t clip;
        uint8_t var; // The 2bit container
        std::size_t npack = 16/_nchannel; // A single uint32_t value contains 16x2bit values
        for(std::size_t i = 0; i < _size; i+=npack)
        {
            clip = 0;
            for(std::size_t j = 0; j < npack; j++)
            {
                for(std::size_t k = 0; k < _nchannel; k++)
                {
                    std::size_t idx = k * _size + i + j;
                    h_idata[idx] -= mean[k];
                    var = 0;
                    // Compute the two bits                                             -> 0 0
                    var += (h_idata[idx] >= (-1. * stddev[k])); // greater than -std    -> 0 1
                    var += (h_idata[idx] >= (0.));              // greater than mean    -> 1 0
                    var += (h_idata[idx] >= (stddev[k]));       // greater than + std   -> 1 1 highest value
                    // h_idata[j=15] gets the 2 MSB; h_idata[j=0] gets the 2 LSB
                    clip += (var << ((j * _nchannel + k) * 2)); //
                }
            }
            h_odata[i / npack] = clip;
        }
        tested = true;
    }

    void gpu_process()
    {
        vdif::Packer packer(_nchannel, _threshold);
        packer.process(d_idata, d_odata, _stream);
        CUDA_ERROR_CHECK(cudaStreamSynchronize(_stream));
    }

    void reference_test()
    {
        cpu_process();
        gpu_process();
        thrust::host_vector<uint32_t> gpu_out = d_odata;
        for(std::size_t i=0; i < gpu_out.size(); i++){
            // Compare results
            EXPECT_EQ(h_odata[i], gpu_out[i])
                << "CPU and GPU results for index " << i << " unequal. CPU = "
                << h_odata[i] << "; GPU = " << gpu_out[i];// << " GPU old = " << gpu_out2[i];
        }
    }

    thrust::host_vector<uint32_t> get_output()
    {
        if(!tested){cpu_process();}
        return h_odata;
    }
    thrust::host_vector<float> get_input()
    {
        return h_idata;
    }


private:
    const std::size_t _size;
    const std::size_t _nchannel;
    float _threshold;
    cudaStream_t _stream;
    bool tested = false;
    thrust::host_vector<float> h_idata;
    thrust::host_vector<uint32_t> h_odata;
    thrust::host_vector<float> mean;
    thrust::host_vector<float> stddev;
    thrust::device_vector<float> d_mean;
    thrust::device_vector<float> d_stddev;
    thrust::device_vector<float> d_idata;
    thrust::device_vector<uint32_t> d_odata;
};

}
}
}
