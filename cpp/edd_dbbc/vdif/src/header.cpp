#include "edd_dbbc/vdif/header.h"

namespace edd_dbbc {
namespace vdif {

Header::Header()
{
    _hdr_data = new uint32_t[n_words];
    for (int i = 0; i < 8; i++)
    {
        _hdr_data[i] = 0x00000000;
    }
}

Header::Header(const Header &v)
{
    _hdr_data = new uint32_t[n_words];
    for (int i = 0; i < 8; i++)
    {
        _hdr_data[i] = v.getData()[i];
    }
}

Header::Header(uint32_t* data)
{
    setData(data);
}

Header& Header::operator=(const Header& other)
{
    for (int i = 0; i < 8; i++)
    {
        _hdr_data[i] = other.getData()[i];
    }
    return *this;
}

uint32_t *Header::getData() const
{
    return _hdr_data;
}

void Header::setData(uint32_t* data)
{
  _hdr_data = data;
}

void Header::setTimeReferencesFromTimestamp(std::size_t sync_time)
{
    boost::posix_time::ptime pt = boost::posix_time::from_time_t(sync_time);

    boost::gregorian::date epochBegin(pt.date().year(),
        ((pt.date().month() <= 6) ? 1 : 7), 1
    );
    int refEpoch = (epochBegin.year() - 2000) * 2 + (epochBegin.month() >= 7);
    if (refEpoch < 0)
    {
        BOOST_LOG_TRIVIAL(error) << "Cannot encode time before 1 Jan 2000 - received " << pt;
    }
    setReferenceEpoch(refEpoch);

    boost::posix_time::time_duration delta = pt - boost::posix_time::ptime(epochBegin);
    setSecondsFromReferenceEpoch(delta.total_seconds());

    BOOST_LOG_TRIVIAL(info) << "Setting time reference from timestamp: " << sync_time;
    BOOST_LOG_TRIVIAL(info) << "\tposix_time:  " << pt;
    BOOST_LOG_TRIVIAL(info) << "\tepochBegin: " << epochBegin;
    BOOST_LOG_TRIVIAL(info) << "\treference epoch: " << refEpoch;
    BOOST_LOG_TRIVIAL(info) << "\ttime delta since epoch begin: " << delta << " = "  << delta.total_seconds();
    BOOST_LOG_TRIVIAL(info) << "Time stamp: " << sync_time << " => VDIF Reference epoch: "
        << getReferenceEpoch() << " at " << getSecondsFromReferenceEpoch() << " s";
}

std::size_t Header::getTimestamp() const
{
    boost::gregorian::date vdifEpochBegin(getReferenceEpoch() / 2 + 2000,
        ((getReferenceEpoch() % 2) * 6) + 1, 1);
    boost::posix_time::ptime pt =  boost::posix_time::ptime(vdifEpochBegin)
        + boost::posix_time::seconds(getSecondsFromReferenceEpoch());
    boost::posix_time::ptime unixEpoch =
        boost::posix_time::time_from_string("1970-01-01 00:00:00.000");
    boost::posix_time::time_duration delta = pt - unixEpoch;
    return delta.total_seconds();
}


// -------------------------- //
//  SETTER & GETTER - WORD 0  //
// -------------------------- //
void Header::setValid(bool value)
{
  if(value)
  {
    _hdr_data[0] = (_hdr_data[0] & ~0x80000000) | 0x00000000;
  }
  else
  {
    _hdr_data[0] = (_hdr_data[0] & ~0x80000000) | 0x80000000;
  }
}

void Header::setSecondsFromReferenceEpoch(uint32_t value)
{
  //setBitsWithValue(_hdr_data[0], 0, 29, value);
  _hdr_data[0] = (_hdr_data[0] & ~0x3FFFFFFF) | (0x3FFFFFFF & value);
}

bool Header::isValid() const
{
  return ((_hdr_data[0] & 0x80000000) == 0);
}
uint32_t Header::getSecondsFromReferenceEpoch() const {
  return _hdr_data[0] & 0x3FFFFFFF;
}

// -------------------------- //
//  SETTER & GETTER - WORD 1  //
// -------------------------- //
void Header::setUnassigned(uint32_t value) {
  _hdr_data[1] = (_hdr_data[1] & ~0xC0000000) | (0xC0000000 & (value<<30));
}
void Header::setReferenceEpoch(uint32_t value) {
  _hdr_data[1] = (_hdr_data[1] & ~0x3F000000) | (0x3F000000 & (value<<24));
}
void Header::setDataFrameNumber(uint32_t value) {
  _hdr_data[1] = (_hdr_data[1] & ~0x00FFFFFF) | (0x00FFFFFF & value);
}
uint32_t Header::getUnassigned() const {
  return (_hdr_data[1] & 0xC0000000)>>30;
}
uint32_t Header::getReferenceEpoch() const {
  return (_hdr_data[1] & 0x3F000000)>>24;
}
uint32_t Header::getDataFrameNumber() const {
  return _hdr_data[1] & 0x00FFFFFF;
}

// -------------------------- //
//  SETTER & GETTER - WORD 2  //
// -------------------------- //
void Header::setVersionNumber(uint32_t value) {
  _hdr_data[2] = (_hdr_data[2] & ~0xE0000000) | (0xE0000000 & (value<<29));
}
void Header::setNumberOfChannels(uint32_t value) {
  _hdr_data[2] = (_hdr_data[2] & ~0x1F000000) | (0x1F000000 & (value<<24));
}
void Header::setDataFrameLength(uint32_t value) {
  _hdr_data[2] = (_hdr_data[2] & ~0x00FFFFFF) | (0x00FFFFFF & value);
}
uint32_t Header::getVersionNumber() const {
  return (_hdr_data[2] & 0xE0000000)>>29;
}
uint32_t Header::getNumberOfChannels() const {
  return (_hdr_data[2] & 0x1F000000)>>24;
}
uint32_t Header::getDataFrameLength() const {
  return _hdr_data[2] & 0x00FFFFFF;
}

// -------------------------- //
//  SETTER & GETTER - WORD 3  //
// -------------------------- //
void Header::setComplexDataType(bool value) {
  if(value){
    _hdr_data[3] = (_hdr_data[3] & ~0x80000000) | 0x80000000;
  }else{
    _hdr_data[3] = (_hdr_data[3] & ~0x80000000) | 0x00000000;
  }
}
void Header::setBitsPerSample(uint32_t value) {
  _hdr_data[3] = (_hdr_data[3] & ~0x7C000000) | (0x7C000000 & (value<<26));
}
void Header::setThreadId(uint32_t value) {
  _hdr_data[3] = (_hdr_data[3] & ~0x03FF0000) | (0x03FF0000 & (value<<16));
}
void Header::setStationId(uint32_t value) {
  _hdr_data[3] = (_hdr_data[3] & ~0x0000FFFF) | (0x0000FFFF & value);
}
bool Header::isComplexDataType() const {
  return ((_hdr_data[3] & 0x80000000) != 0);
}
uint32_t Header::getBitsPerSample() const {
  return (_hdr_data[3] & 0x7C000000)>>26;
}
uint32_t Header::getThreadId() const {
  return (_hdr_data[3] & 0x03FF0000)>>16;
}
uint32_t Header::getStationId() const {
  return _hdr_data[3] & 0x0000FFFF;
}

// --------------------------  //
//  SETTER & GETTER - WORD 4-7 //
// --------------------------  //
void Header::setEdv(uint32_t value) {
  _hdr_data[4] = (_hdr_data[4] & ~0xFF000000) | (0xFF000000 & (value<<24));
}
void Header::setUserWord4(uint32_t value) {
  _hdr_data[4] = (_hdr_data[4] & ~0x00FFFFFF) | (0x00FFFFFF & value);
}
void Header::setUserWord5(uint32_t value) {
  _hdr_data[5] = (_hdr_data[5] & ~0xFFFFFFFF) | (0xFFFFFFFF & value);
}
void Header::setUserWord6(uint32_t value) {
  _hdr_data[6] = (_hdr_data[6] & ~0xFFFFFFFF) | (0xFFFFFFFF & value);
}
void Header::setUserWord7(uint32_t value) {
  _hdr_data[7] = (_hdr_data[7] & ~0xFFFFFFFF) | (0xFFFFFFFF & value);
}
uint32_t Header::getEdv(){
  return (_hdr_data[4] & 0xFF000000)>>24;
}
uint32_t Header::getUserWord4(){
  return _hdr_data[4] & 0x00FFFFFF;
}
uint32_t Header::getUserWord5(){
  return _hdr_data[5] & 0xFFFFFFFF;
}
uint32_t Header::getUserWord6(){
  return _hdr_data[6] & 0xFFFFFFFF;
}
uint32_t Header::getUserWord7(){
  return _hdr_data[7] & 0xFFFFFFFF;
}

} // namespace vdif
} // namespace edd_dbbc