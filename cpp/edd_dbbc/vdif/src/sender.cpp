#include "edd_dbbc/vdif/sender.h"

namespace edd_dbbc {
namespace vdif {

Sender::Sender(
    const std::string& source,
    const std::string& destination,
    int port,
    double max_rate,
    boost::asio::io_service& service
) :
    source_ip(source),
    destination_ip(destination),
    port(port),
    max_rate(max_rate),
    socket(service),
    currentSecondFromReferenceEpoch(0),
    noOfSendFrames(0)
{
  BOOST_LOG_TRIVIAL(debug) << "Instantiated Sender-object";
}

Sender::~Sender()
{
  BOOST_LOG_TRIVIAL(debug) << "Destroying Sender-object";
}

void Sender::init(psrdada_cpp::RawBytes &block)
{
  BOOST_LOG_TRIVIAL(debug) << "Preparing socket for communication from " << source_ip << " to " << destination_ip << " port: " << port;
  // drop header as not needed, only open socket.
  remote_endpoint = boost::asio::ip::udp::endpoint(boost::asio::ip::address::from_string(destination_ip), port);
  boost::asio::ip::address_v4 local_interface = boost::asio::ip::address_v4::from_string(source_ip);
  boost::asio::ip::multicast::outbound_interface option(local_interface);

  socket.open(boost::asio::ip::udp::v4());
  socket.set_option(option);
  BOOST_LOG_TRIVIAL(debug) << "Got header of size " << block.used_bytes();
}

bool Sender::operator()(psrdada_cpp::RawBytes &block)
{
  if (block.used_bytes() == 0)
  {
    BOOST_LOG_TRIVIAL(info) << "Received empty block, exiting.";
    return false;
  }
  boost::system::error_code err;
  Header header(reinterpret_cast<uint32_t*>(block.ptr()));

  size_t blockSize = block.used_bytes();

  BOOST_LOG_TRIVIAL(debug) << " Length of first frame: " << header.getDataFrameLength() * 8  << " bytes";
  size_t counter = 0;
  size_t invalidFrames = 0;

  auto start = std::chrono::high_resolution_clock::now();
  for(char* frame_start = block.ptr(); frame_start < block.ptr() + blockSize; frame_start += header.getDataFrameLength() * 8)
  {
    header.setData(reinterpret_cast<uint32_t*>(frame_start));
    // skip invalid blocks
    if (!header.isValid())
    {
      invalidFrames++;
      continue;
    }
    if (header.getSecondsFromReferenceEpoch() > currentSecondFromReferenceEpoch)
    {
      BOOST_LOG_TRIVIAL(info) << " New second frome reference epoch: " << header.getSecondsFromReferenceEpoch() << ", send " << noOfSendFrames << " in previous second.";

      BOOST_LOG_TRIVIAL(debug) <<     "  Previous second from refEpoch " << currentSecondFromReferenceEpoch << " delta = " << header.getSecondsFromReferenceEpoch() - currentSecondFromReferenceEpoch;
      currentSecondFromReferenceEpoch = header.getSecondsFromReferenceEpoch();
      noOfSendFrames = 0;
    }

    uint32_t frameLength = header.getDataFrameLength() * 8; // in units of 8 bytes

    socket.send_to(boost::asio::buffer(frame_start, frameLength), remote_endpoint, 0, err);
    noOfSendFrames++;
    counter++;

    size_t processed_bytes = (frame_start - block.ptr()) + frameLength;
    auto elapsed_time = std::chrono::high_resolution_clock::now() - start;

    double current_rate = processed_bytes / (std::chrono::duration_cast<std::chrono::nanoseconds>(elapsed_time).count() * 1E-9);
    if (current_rate > max_rate)
    {
      std::chrono::duration<double, std::nano> expected_time(processed_bytes / max_rate * 1E9);

      auto delay = expected_time  - elapsed_time;
      std::this_thread::sleep_for(delay);

      BOOST_LOG_TRIVIAL(debug) << counter << " Set delay to " << delay.count()<< " ns. Current rate " << current_rate << ", processed_bytes: " << processed_bytes;
    }
    if (counter < 5)
      BOOST_LOG_TRIVIAL(debug) << counter << " Send - FN: " << header.getDataFrameNumber() <<  ", Sec f. E.: " << header.getSecondsFromReferenceEpoch() << " Get TS.: " << header.getTimestamp();

  }
  BOOST_LOG_TRIVIAL(debug) << "Send " << counter << " frames of " << block.used_bytes() << " bytes total size. " << invalidFrames << " invalid frames in block.";
  return false;
}

}
}