#include "edd_dbbc/vdif/packer.cuh"

namespace edd_dbbc {
namespace vdif {

Packer::Packer(std::size_t nchannel, float digitizer_threshold)
    :
    _nchannel(nchannel),
    _digitizer_threshold(digitizer_threshold)
{
    mean.resize(_nchannel);
    stddev.resize(_nchannel);
}


Packer::~Packer()
{

}

void Packer::process(
    thrust::device_vector<float>& idata,
    thrust::device_vector<uint32_t>& odata,
    cudaStream_t& stream)
{
    edd_dbbc::tools::mean(idata, mean);
    edd_dbbc::tools::variance(idata, stddev, mean);
    for(std::size_t i = 0; i < _nchannel; i++)
    {
        std::cout << "channel " << i << " mean " << mean[i] << " std " << stddev[i];
        stddev[i] = _digitizer_threshold * sqrt(stddev[i]);
        std::cout << " stdroot " << stddev[i] << std::endl;
    }
    switch(_nchannel)
    {
        case 1:
            pack2bit_non_linear<1><<<n_blocks, n_threads, 0, stream>>>(
                thrust::raw_pointer_cast(idata.data()),
                thrust::raw_pointer_cast(odata.data()),
                idata.size(),
                thrust::raw_pointer_cast(stddev.data()),
                thrust::raw_pointer_cast(mean.data()));
            break;
        case 2:
            pack2bit_non_linear<2><<<n_blocks, n_threads, 0, stream>>>(
                thrust::raw_pointer_cast(idata.data()),
                thrust::raw_pointer_cast(odata.data()),
                idata.size()/2,
                thrust::raw_pointer_cast(stddev.data()),
                thrust::raw_pointer_cast(mean.data()));
            break;
        case 4:
            pack2bit_non_linear<4><<<n_blocks, n_threads, 0, stream>>>(
                thrust::raw_pointer_cast(idata.data()),
                thrust::raw_pointer_cast(odata.data()),
                idata.size()/4,
                thrust::raw_pointer_cast(stddev.data()),
                thrust::raw_pointer_cast(mean.data()));
            break;
        case 8:
            pack2bit_non_linear<8><<<n_blocks, n_threads, 0, stream>>>(
                thrust::raw_pointer_cast(idata.data()),
                thrust::raw_pointer_cast(odata.data()),
                idata.size()/8,
                thrust::raw_pointer_cast(stddev.data()),
                thrust::raw_pointer_cast(mean.data()));
            break;
        // case 16:
        //     pack2bit_non_linear<16><<<n_blocks, n_threads, 0, stream>>>(
        //         thrust::raw_pointer_cast(idata.data()),
        //         thrust::raw_pointer_cast(odata.data()),
        //         idata.size()/16, _digitizer_threshold,
        //         thrust::raw_pointer_cast(stddev.data()),
        //         thrust::raw_pointer_cast(mean.data()));
        //     break;
        default:
            std::string err_str = "Packing for " + std::to_string(_nchannel)
                + " channel not implemented";
            BOOST_LOG_TRIVIAL(error) << err_str;
            throw std::runtime_error(err_str);
    }
}


// void Packer::process(
//     thrust::device_vector<float>& idata,
//     thrust::device_vector<uint32_t>& odata,
//     std::size_t nchannels,
//     cudaStream_t& stream)
// {
//     std::size_t nsamples = idata.size() / nchannels;

//     float mean = edd_dbbc::tools::mean(idata);
//     BOOST_LOG_TRIVIAL(debug)
//         << "\tCalculated mean " << std::to_string(mean);
//     float sigma2 = edd_dbbc::tools::variance(idata, mean);
//     BOOST_LOG_TRIVIAL(debug)
//         << "\tCalculated sigma2 " << std::to_string(sigma2);
//     pack2bit_non_linear<<<n_blocks, n_threads, 0, stream>>>(
//       thrust::raw_pointer_cast(idata.data()),
//       thrust::raw_pointer_cast(odata.data()),
//       idata->size(), _digitizer_threshold, sigma2, mean);
// }

}
}