#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/dada_input_stream.hpp>
#include <psrdada_cpp/multilog.hpp>

#include <boost/program_options.hpp>
#include <boost/asio.hpp>

#include <iostream>
#include <string>
#include <chrono>

#include "edd_dbbc/vdif/sender.h"

using namespace psrdada_cpp;

const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t SUCCESS = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

int main(int argc, char **argv) {
  try {
    key_t input_key;

    int destination_port;
    std::string destination_ip, source_ip;
    double max_rate;

    /** Define and parse the program options
    */
    namespace po = boost::program_options;
    po::options_description desc("Options");

    desc.add_options()("help,h", "Print help messages");
    desc.add_options()("input_key,i",
      po::value<std::string>()->default_value("dada")->notifier(
        [&input_key](std::string in) { input_key = string_to_key(in); }),
      "The shared memory key for the dada buffer to connect to (hex "
      "string)");
    desc.add_options()("dest_ip", po::value<std::string>(&destination_ip)->required(),
      "Destination IP");
    desc.add_options()("if_ip", po::value<std::string>(&source_ip)->required(),
      "IP of the interface to use");
    desc.add_options()("port",
      po::value<int>()->default_value(8125)->notifier(
        [&destination_port](int in) { destination_port = in; }),
      "Destination PORT");
    desc.add_options()("max_rate",
      po::value<double>()->default_value(1024*1024*5)->notifier(
        [&max_rate](double in) { max_rate = in; }),
      "Limit the output rate to [max_rate] byte/s");
    desc.add_options()(
      "log_level", po::value<std::string>()->default_value("info")->notifier(
         [](std::string level) { set_log_level(level); }),
      "The logging level to use "
      "(trace, debug, info, warning, "
      "error)");

    po::variables_map vm;
    try {
      po::store(po::parse_command_line(argc, argv, desc), vm);
      if (vm.count("help")) {
        std::cout << "vdif_Send -- send vidf frames in a dada buffer to an ip via UDP."
                  << std::endl << desc << std::endl;
        return SUCCESS;
      }
      po::notify(vm);

    } catch (po::error &e) {
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << desc << std::endl;
      return ERROR_IN_COMMAND_LINE;
    }

    MultiLog log("edd::vdif_send");
    DadaClientBase client(input_key, log);

    boost::asio::io_service io_service;
    edd_dbbc::vdif::Sender sender(source_ip, destination_ip, destination_port, max_rate, io_service);

    DadaInputStream<decltype(sender)> istream(input_key, log, sender);
    istream.start();


  } catch (std::exception &e) {
    std::cerr << "Unhandled Exception reached the top of main: " << e.what()
              << ", application will now exit" << std::endl;
    return ERROR_UNHANDLED_EXCEPTION;
  }
  return SUCCESS;
}
