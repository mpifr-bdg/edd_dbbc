#include <boost/program_options.hpp>

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/cli_utils.hpp>
#include <psrdada_cpp/dada_input_stream.hpp>
#include <psrdada_cpp/dada_null_sink.hpp>
#include <psrdada_cpp/dada_output_stream.hpp>
#include <psrdada_cpp/multilog.hpp>
#include <psrdada_cpp/simple_file_writer.hpp>

#include <ctime>
#include <iostream>
#include <time.h>
#include <bits/stdc++.h>

#include "edd_dbbc/vdif/header.h"
#include "edd_dbbc/vdif/pipeline.cuh"

using namespace psrdada_cpp;

const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t SUCCESS = 0;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

template<typename InputType>
void launch_pipeline(key_t input_key, key_t output_key, edd_dbbc::vdif::pipe_config config)
{
  MultiLog log("edd_dbbc::vdif_packer");
  DadaClientBase client(input_key, log);
  config.buffer_bytes = client.data_buffer_size();
  DadaOutputStream sink(output_key, log);
  edd_dbbc::vdif::Header header;
  edd_dbbc::vdif::Pipeline<decltype(sink), InputType> pipeline(config, header, sink);
  DadaInputStream<decltype(pipeline)> istream(input_key, log, pipeline);
  istream.start();
}


int main(int argc, char **argv) {
  try {
    key_t input_key; key_t output_key;
    std::size_t device;
    std::time_t now = std::time(NULL);
    std::tm *ptm = std::localtime(&now);
    char buffer[32];
    std::strftime(buffer, 32, "%Y-%m-%d-%H:%M:%S.bp", ptm);
    std::string filename(buffer);
    edd_dbbc::vdif::pipe_config config;

    /** Define and parse the program options
    */
    namespace po = boost::program_options;
    po::options_description desc("Options");

    desc.add_options()("help,h", "Print help messages");
    desc.add_options()(
      "input_key,i",
      po::value<std::string>()->default_value("dada")->notifier(
          [&input_key](std::string in) { input_key = string_to_key(in); }),
      "The shared memory key for the input dada buffer to connect to (hex "
      "string)");
    desc.add_options()(
      "output_key,o",
      po::value<std::string>()->default_value("dadc")->notifier(
          [&output_key](std::string val) { output_key = string_to_key(val); }),
      "The shared memory key for the output dada buffer to connect to (hex "
      "string)");
    desc.add_options()(
      "bit_depth", po::value<std::size_t>(&config.bit_depth)->default_value(32),
      "The bit depth of the input data (8,10,12 are assumed to be integer, while 32 is assumed to be float)");
    desc.add_options()("thread_id",
      po::value<std::size_t>(&config.thread_id)->default_value(0),
      "Thread ID for the VDIF header");
    desc.add_options()("device,g", po::value<std::size_t>(&device)->default_value(0),
      "The ID of the GPU device");
    desc.add_options()("station_id",
      po::value<std::size_t>(&config.station_id)->default_value(0),
      "Station ID for the VDIF header");
    desc.add_options()("channels",
      po::value<std::size_t>(&config.n_channels)->default_value(1),
      "Number of channels for VDIF packer");
    desc.add_options()("input_batch",
      po::value<std::size_t>(&config.input_batch)->default_value(0),
      "If there is a second time dimension, specify the number"
      " of consecutive samples of the same channel, otherwise 0.");
    desc.add_options()("payload_size",
      po::value<std::size_t>(&config.n_bytes_payload)->default_value(8192),
      "The payload size of a VDIF packet[bytes]");
    desc.add_options()("digitizer_threshold",
      po::value<double>(&config.digitizer_threshold)->default_value(0.981599),
      "Digitizer threshold in levels of the rms of the signal."
      "The default v = 0.981599 is set according to "
      "L. Kogan, Correction functions for digital correlators"
      " with two and four quantization levels, "
      "Radio Science 33 (1998), 1289-1296");
    desc.add_options()("sample_rate",
      po::value<std::size_t>(&config.sample_rate)->default_value(32E6),
      "Sample rate of the input signal");

    po::variables_map vm;
    try {
      po::store(po::parse_command_line(argc, argv, desc), vm);
      if (vm.count("help")) {
        std::cout << "VLBI -- Read EDD data from a DADA buffer "
                     "and convert it to 2 bit VLBI data in VDIF format"
                  << std::endl
                  << desc << std::endl;
        return SUCCESS;
      }

      po::notify(vm);

    } catch (po::error &e) {
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
      std::cerr << desc << std::endl;
      return ERROR_IN_COMMAND_LINE;
    }
    CUDA_ERROR_CHECK(cudaSetDevice(device));
    if(config.bit_depth == 32){
      launch_pipeline<float>(input_key, output_key, config);
    }else if(config.bit_depth == 8 || config.bit_depth == 10 || config.bit_depth == 12){
      launch_pipeline<uint64_t>(input_key, output_key, config);
    }else{
      throw std::runtime_error("Unsupported bit depth");
    }



  } catch (std::exception &e) {
    std::cerr << "Unhandled Exception reached the top of main: " << e.what()
              << ", application will now exit" << std::endl;
    return ERROR_UNHANDLED_EXCEPTION;
  }
  return SUCCESS;
}
