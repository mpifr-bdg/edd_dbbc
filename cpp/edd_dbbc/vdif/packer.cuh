#pragma once

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/raw_bytes.hpp>
// CUDA dependencies
#include <cuda.h>
#include <thrust/device_vector.h>

#include "edd_dbbc/math_tools.cuh"
#include <psrdada_cpp/cuda_utils.hpp>

namespace edd_dbbc {
namespace vdif {

const std::size_t n_threads = 1024;
const std::size_t n_blocks = 128;
const std::size_t max_channel = 8;

/**
 * CUDA Kernel for conversion from float to 2-bit data
*/
template<int nchannel>
__global__ void pack2bit_non_linear(
    const float *__restrict__ input,
    uint32_t *__restrict__ output,
    std::size_t width,
    float* sigma2,
    float* mean
);

class Packer
{
public:
    Packer(std::size_t nchannel, float digitizer_threshold);
    ~Packer();

    void process(
        thrust::device_vector<float>& idata,
        thrust::device_vector<uint32_t>& odata,
        cudaStream_t& stream);
private:
    std::size_t _nchannel;
    float _digitizer_threshold;

    thrust::device_vector<float> mean;
    thrust::device_vector<float> stddev;
};


}
}

#include "edd_dbbc/vdif/detail/kernels.cu"