#pragma once

#include <cstdint>
#include <psrdada_cpp/common.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

namespace edd_dbbc {
namespace vdif {

using namespace std;

const size_t vdif_header_size = 8 * 32 / 8; // bytes [8 words a 32 bit]
const size_t n_words = 8;
/// class Header stores a VDIF compliant header block with conveniant
/// setters and getters. See https://vlbi.org/vlbi-standards/vdif/
/// specification 1.1.1 from June 2014 for details.
class Header
{
  private:
    uint32_t *_hdr_data;

  public:
    Header();
    Header(const Header &v);
    Header(uint32_t* data);
    Header& operator=(const Header& other);

    uint32_t* getData() const;
    void setData(uint32_t* data);

    void setTimeReferencesFromTimestamp(size_t);
    size_t getTimestamp() const;

    // WORD 0 setters / getters
    void setValid(bool value=true);
    void setSecondsFromReferenceEpoch(uint32_t value);
    bool isValid() const;
    uint32_t getSecondsFromReferenceEpoch() const;

    // WORD 1 setters / getters
    void setUnassigned(uint32_t value);
    void setReferenceEpoch(uint32_t value);
    void setDataFrameNumber(uint32_t value);
    uint32_t getUnassigned() const;
    uint32_t getReferenceEpoch() const;
    uint32_t getDataFrameNumber() const;

    // WORD 2 setters
    void setVersionNumber(uint32_t value);
    void setNumberOfChannels(uint32_t value);
    void setDataFrameLength(uint32_t value);
    uint32_t getVersionNumber() const;
    uint32_t getDataFrameLength() const;
    uint32_t getNumberOfChannels() const;

    // WORD 3 setters and getters
    void setComplexDataType(bool value=false);
    void setBitsPerSample(uint32_t value);
    void setThreadId(uint32_t value);
    void setStationId(uint32_t value);
    bool isComplexDataType() const;
    uint32_t getBitsPerSample() const;
    uint32_t getThreadId() const;
    uint32_t getStationId() const;

    // WORD 4-7 setters
    void setEdv(uint32_t value);
    void setUserWord4(uint32_t value);
    void setUserWord5(uint32_t value);
    void setUserWord6(uint32_t value);
    void setUserWord7(uint32_t value);
    uint32_t getEdv();
    uint32_t getUserWord4();
    uint32_t getUserWord5();
    uint32_t getUserWord6();
    uint32_t getUserWord7();
};

} // namespace vdif
} // namespace edd_dbbc