#pragma once

// psrdada_cpp dependencies
#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/raw_bytes.hpp>
#include <psrdada_cpp/double_device_buffer.cuh>
#include <psrdada_cpp/double_host_buffer.cuh>
#include <psrdada_cpp/dada_input_stream.hpp>
#include <psrdada_cpp/common/unpacker.cuh>

// CUDA dependencies
#include <cuda.h>
#include <cublas_v2.h>
#include <thrust/device_vector.h>

// Common libs
#include <cmath>
#include <psrdada/ascii_header.h> // dada
#include <cstring>
#include <iostream>
#include <sstream>

#include <psrdada_cpp/cuda_utils.hpp>
#include "edd_dbbc/vdif/header.h"
#include "edd_dbbc/vdif/packer.cuh"

namespace edd_dbbc {
namespace vdif {

const std::size_t header_size = 4096;

struct pipe_config
{
    std::size_t buffer_bytes = 2048*8000;
    std::size_t station_id = 0;
    std::size_t thread_id = 0;
    std::size_t n_channels = 1;
    std::size_t n_bytes_payload = 8000;
    std::size_t input_batch = 2048;
    std::size_t bit_depth = 32;
    std::size_t sample_rate = 32000000;
    double digitizer_threshold = 0.981599;
};

/**
 @class Pipeline
 @brief Convert data to 2bit data in VDIF format.
 */
template <typename HandlerType, typename InputType>
class Pipeline
{
public:
  /**
   * @brief      Constructor
   *
   * @param      config The pipeline config
   * @param      Header Header of the VDIF output to be sed. Must contain size of the output VDIF payload in bytes.
   * @param      handler Output handler
   *
   */
  Pipeline(
    const pipe_config& config,
    const Header header,
    HandlerType &handler
  );


  ~Pipeline();

  /**
   * @brief      A callback to be called on connection
   *             to a ring buffer.
   *
   * @details     The first available header block in the
   *             in the ring buffer is provided as an argument.
   *             It is here that header parameters could be read
   *             if desired.
   *
   * @param      block  A RawBytes object wrapping a DADA header buffer
   */
  void init(psrdada_cpp::RawBytes &block);

  /**
   * @brief      A callback to be called on acqusition of a new
   *             data block.
   *
   * @param      block  A RawBytes object wrapping a DADA data buffer output
   *             are the integrated specttra with/without bit set.
   */
  bool operator()(psrdada_cpp::RawBytes &block);

  std::size_t get_sample_per_block(){return _n_samples_per_block;}
  std::size_t get_output_bytes(){return output_buffer.a().size();}
  std::size_t call_count(){return _call_count;}
  bool initialized(){return _initialized;}
  Header& packet_header(){return vdif_header;}

private:
    /**
     * @brief    Updates the vdif headers in the host buffer
    */
    void update_vdif_headers();

    static_assert(
        !std::is_same<float, InputType>::value ||
        !std::is_same<uint64_t, InputType>::value,
        "Unsupported template type for Pipeline"
    );

private:
  const std::size_t _output_bit_depth = 2;
  std::size_t _input_bit_depth;
  std::size_t _buffer_bytes;
  std::size_t _station_id;
  std::size_t _thread_id;
  std::size_t _n_bytes_payload;
  std::size_t _input_batch;
  std::size_t _n_channels;
  std::size_t _n_bytes_per_frame;
  std::size_t _n_samples_per_chan;
  std::size_t _n_samples_per_block;
  std::size_t _n_samples_per_frame;
  std::size_t _n_frames_per_block;
  std::size_t _output_block_size;
  std::size_t _call_count;
  std::size_t _sample_rate;
  bool _initialized = false;
  double _digitizer_threshold;

  HandlerType &_handler;

  // Sub-processor to execute the processing chain
  Header vdif_header;
  std::unique_ptr<edd_dbbc::vdif::Packer> vdif_packer;
  std::unique_ptr<psrdada_cpp::Unpacker> unpacker;

  // Data arrays / buffer;
  thrust::device_vector<float> unpacked_buffer;
  psrdada_cpp::DoubleDeviceBuffer<InputType> input_buffer;
  psrdada_cpp::DoubleDeviceBuffer<uint32_t> packetized_buffer;
  psrdada_cpp::DoublePinnedHostBuffer<uint8_t> output_buffer;

  cudaStream_t host2dev_stream;
  cudaStream_t processing_stream;
  cudaStream_t dev2host_stream;
};


} //namespace vdif
} //namespace edd_dbbc


#include "edd_dbbc/vdif/detail/pipeline.cu"
