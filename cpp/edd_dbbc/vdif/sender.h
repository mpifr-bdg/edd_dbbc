#pragma once

#include <psrdada_cpp/common.hpp>
#include <psrdada_cpp/raw_bytes.hpp>

#include <boost/asio.hpp>

#include <iostream>
#include <string>
#include <chrono>
#include <thread>

#include "edd_dbbc/vdif/header.h"

namespace edd_dbbc {
namespace vdif {

class Sender
{

public:
  /**
     * @brief      Constructor.
     *
     * @param      destination_ip Address to send the udp packages to.
     * @param      port           Port to use.
     * @param      max_rate       Output data rate - Usefull to avoid burst
     * @param      io_service     boost::asio::io_Service instance to use for
     *                            communication.
     */
  Sender(
    const std::string& source,
    const std::string& destination,
    int port,
    double max_rate,
    boost::asio::io_service& service
  );

  ~Sender();

  /**
   * @brief      A callback to be called on connection
   *             to a ring buffer.
   *
   * @details     The first available header block in the
   *             in the ring buffer is provided as an argument.
   *             It is here that header parameters could be read
   *             if desired.
   *
   * @param      block  A RawBytes object wrapping a DADA header buffer
   */
  void init(psrdada_cpp::RawBytes &block);

  /**
   * @brief      A callback to be called on acqusition of a new
   *             data block.
   *
   * @param      block  A RawBytes object wrapping a DADA data buffer
   */
  bool operator()(psrdada_cpp::RawBytes &block);


private:
    std::string source_ip;
    std::string destination_ip;
    int port;
    double max_rate;
    boost::asio::ip::udp::socket socket;
    boost::asio::ip::udp::endpoint remote_endpoint;
    uint32_t currentSecondFromReferenceEpoch;
    size_t noOfSendFrames;      // frames in last second

};

} // namespace edd_dbbc
} // namespace vdif
