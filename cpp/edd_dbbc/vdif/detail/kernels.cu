#pragma once

namespace edd_dbbc {
namespace vdif {

template<int nchannel>
__global__ void pack2bit_non_linear(
    const float *__restrict__ input,
    uint32_t *__restrict__ output,
    std::size_t width,
    float* sigma2,
    float* mean)
{
    // number of values to pack into one output element, use 32 bit here to
    // maximize number of threads
    const int NPACK = 16/nchannel;
    // bit mask: Thread 0 always first input_bit_depth bits, thread 1 always
    // second input_bit_depth bits, ...
    const uint32_t mask = ((1 << 2*nchannel) - 1) << (2 * nchannel * (threadIdx.x % NPACK));

    uint32_t p;
    uint32_t out = 0;
    float inp;

    __shared__ uint32_t tmp[n_threads];

    for (int i = NPACK * blockIdx.x * blockDim.x + threadIdx.x;
        (i < width); i += blockDim.x * gridDim.x * NPACK)
    {
        uint32_t reg_tmp = 0;

#pragma unroll
        for (int j = 0; j < NPACK; j++)
        {
#pragma unroll
            for(int chan = 0; chan < nchannel; chan++)
            {
                float sig = sigma2[chan];
                // Load new input value, clip and convert to Nbit integer
                inp = input[chan * width + i + j * blockDim.x] - mean[chan];
                p = 0;
                p += (inp >= (-1. * sig));
                p += (inp >= (0));
                p += (inp >= (1. * sig));
                // store temporary in register
                reg_tmp += p << (2 * (j * nchannel + chan));
            }
        }
        // Write the temporary register value to shared memory
        tmp[threadIdx.x] = reg_tmp;
__syncthreads();

        // load value from shared memory and rearange to output - the read value is
        // reused per warp
        out = 0;

#pragma unroll
        for (int j = 0; j < NPACK; j++)
        {
            uint32_t v = tmp[(threadIdx.x / NPACK) * NPACK + j] & mask;
            // retrieve correct bits
            v = v >> (2 * nchannel * (threadIdx.x % NPACK));
            v = v << (2 * nchannel * j);
            out += v;
        }

        std::size_t oidx = threadIdx.x / NPACK
            + (threadIdx.x % NPACK) * (blockDim.x / NPACK)
            + (i - threadIdx.x) / NPACK;
        output[oidx] = out;
__syncthreads();
    }
}


}
}
