#pragma once

namespace edd_dbbc {
namespace vdif {

template <typename HandlerType, typename InputType>
Pipeline<HandlerType, InputType>::Pipeline(
    const pipe_config& config,
    const Header header,
    HandlerType &handler)
  :
    _buffer_bytes(config.buffer_bytes),
    _station_id(config.station_id),
    _thread_id(config.thread_id),
    _n_channels(config.n_channels),
    _n_bytes_payload(config.n_bytes_payload),
    _input_batch(config.input_batch),
    _sample_rate(config.sample_rate),
    _digitizer_threshold(config.digitizer_threshold),
    _input_bit_depth(config.bit_depth),
    vdif_header(header),
    _handler(handler),
    _call_count(0)
{
    BOOST_LOG_TRIVIAL(info) << "Creating new Pipeline instance";
    // Sanity checks
    // Check if the number of channels is power of 2
    if(!(_n_channels > 0 && ((_n_channels & (_n_channels-1)) == 0)))
    {
        BOOST_LOG_TRIVIAL(error) << "The number of channels is not a power of 2";
        throw std::runtime_error("The number of channels is not a power of 2");
    }
    // Check if the the buffer is sized properly to the input bit depth
    if(_buffer_bytes * 8 % _input_bit_depth)
    {
        BOOST_LOG_TRIVIAL(error) << "The byte size of the input buffer does not match with the input bit depth";
        throw std::runtime_error("The byte size of the input buffer does not match with the input bit depth");
    }// Check if the the buffer is sized properly sized to the input bit depth
    if(_buffer_bytes % _n_bytes_payload != 0)
    {
        BOOST_LOG_TRIVIAL(error) << "The input buffer size must be a multiple of the VDIF frame size";
        throw std::runtime_error("The input buffer size must be a multiple of the VDIF frame size");
    }

    // check for any device errors
    CUDA_ERROR_CHECK(cudaDeviceSynchronize());

    // Calculate parameters
    _n_bytes_per_frame = _n_bytes_payload + vdif_header_size;
    _n_samples_per_block = _buffer_bytes * 8 / _input_bit_depth;
    _n_samples_per_frame = _n_bytes_payload * 8 / _output_bit_depth;
    _n_samples_per_chan = _n_samples_per_block / _n_channels;
    _n_frames_per_block = _n_samples_per_block / _n_samples_per_frame;

    // Allocate buffers
    std::size_t required_memory = _buffer_bytes * 2;
    required_memory += _n_samples_per_block * _output_bit_depth / 8 * sizeof(uint32_t) * 2;
    required_memory += _n_frames_per_block * _n_bytes_per_frame * 2;
    // In case the InputType is uint64_t, we need a secondary buffer to unpack the data to float
    if(std::is_same<uint64_t, InputType>::value)
    {
        required_memory += _n_samples_per_block * sizeof(float);
        unpacked_buffer.resize(_n_samples_per_block);
    }
    BOOST_LOG_TRIVIAL(info) << "Required GPU memory: " << required_memory / (1024*1024) << " MiB";
    input_buffer.resize(_buffer_bytes / sizeof(InputType));
    packetized_buffer.resize(_n_samples_per_block * _output_bit_depth / 8);
    output_buffer.resize(_n_frames_per_block * _n_bytes_per_frame);

    // Create the CUDA streams
    CUDA_ERROR_CHECK(cudaStreamCreate(&host2dev_stream));
    CUDA_ERROR_CHECK(cudaStreamCreate(&processing_stream));
    CUDA_ERROR_CHECK(cudaStreamCreate(&dev2host_stream));

    // Initialized the Header object with constant values
    vdif_header.setDataFrameLength(_n_bytes_per_frame / 8); // In units of 8 bytes (SIC) according to VDIF spec.
    vdif_header.setNumberOfChannels((uint32_t)(log2(_n_channels)));  // log_2 (channel number)
    vdif_header.setThreadId(_thread_id);
    vdif_header.setStationId(_station_id);
    vdif_header.setBitsPerSample(_output_bit_depth - 1); // bits per sample - 1
    vdif_header.setComplexDataType(false);
    vdif_header.setVersionNumber(1);

    // Initialize the Packer object which performas the float to 2bit conversion
    vdif_packer.reset(new edd_dbbc::vdif::Packer(_n_channels, _digitizer_threshold));
    unpacker.reset(new psrdada_cpp::Unpacker(processing_stream));

    BOOST_LOG_TRIVIAL(info) << "Stream properties:";
    BOOST_LOG_TRIVIAL(info) << "\t" << vdif_header_size << " header [bytes]";
    BOOST_LOG_TRIVIAL(info) << "\t" << _n_bytes_payload << " payload [bytes]";
    BOOST_LOG_TRIVIAL(info) << "\t" << _n_bytes_per_frame << " data frame [bytes]";
    BOOST_LOG_TRIVIAL(info) << "\t" << _sample_rate << " sample rate [Hz/channel]";
    BOOST_LOG_TRIVIAL(info) << "\t" << _n_channels << " number of channels";
    BOOST_LOG_TRIVIAL(info) << "\t" << _n_frames_per_block << " number of frames per DADA block";
} // constructor



template <typename HandlerType, typename InputType>
Pipeline<HandlerType, InputType>::~Pipeline()
{
    BOOST_LOG_TRIVIAL(info) << "Destroying Pipeline";
    CUDA_ERROR_CHECK(cudaStreamDestroy(host2dev_stream));
    CUDA_ERROR_CHECK(cudaStreamDestroy(processing_stream));
    CUDA_ERROR_CHECK(cudaStreamDestroy(dev2host_stream));
}


template <typename HandlerType, typename InputType>
void Pipeline<HandlerType, InputType>::init(psrdada_cpp::RawBytes &block)
{
    BOOST_LOG_TRIVIAL(info) << "Pipeline init called";
    std::size_t sync_time = 0;
    std::size_t sample_clock_start = 0;
    std::size_t timestamp = 0;
    // Get the sync time of the digitizer
    if (ascii_header_get(block.ptr(), "SYNC_TIME", "%ld", &sync_time) != 1)
    {
        BOOST_LOG_TRIVIAL(warning)
            << "No or multiple SYNC_TIME parameters in header stream! Not setting reference";
        return;
    }
    // Get the sample count of the first arrived/accepted heap
    if (ascii_header_get(block.ptr(), "SAMPLE_CLOCK_START", "%ld", &sample_clock_start) != 1)
    {
        BOOST_LOG_TRIVIAL(warning)
            << "No or multiple SAMPLE_CLOCK_START in header stream! Not setting reference time";
        return;
    }
    // Proof that the sample count is a multiple of sample rate - It is a full second
    if (sample_clock_start % static_cast<int>(_sample_rate) != 0)
    {
        BOOST_LOG_TRIVIAL(error) << "SAMPLE_CLOCK_START does not start at a full second";
        throw std::runtime_error("SAMPLE_CLOCK_START does not start at a full second");
    }
    // Calculate and set the timestamp of the first VDIF packet
    timestamp = sync_time + sample_clock_start / _sample_rate;
    BOOST_LOG_TRIVIAL(info) << "POSIX timestamp for first VDIF package: " << timestamp;
    vdif_header.setTimeReferencesFromTimestamp(timestamp);
    _handler.init(block);
    _initialized = true;
}


template <typename HandlerType, typename InputType>
bool Pipeline<HandlerType, InputType>::operator()(psrdada_cpp::RawBytes &block)
{
    if(!_initialized)
    {
        BOOST_LOG_TRIVIAL(error) << "Pipeline::init(RawBytes&) not called or not successfully initialized";
        return true;
    }
    /* Unexpected buffer size */
    if (block.used_bytes() != _buffer_bytes)
    {
        BOOST_LOG_TRIVIAL(error) << "Unexpected Buffer Size - Got " << block.used_bytes()
            << " bytes, expected " << _buffer_bytes << " byte)";
        CUDA_ERROR_CHECK(cudaDeviceSynchronize());
        return true;
    }
    BOOST_LOG_TRIVIAL(debug) << "Pipeline::operator(RawBytes&) called " << _call_count;
    BOOST_LOG_TRIVIAL(debug) << "Processing " << block.used_bytes() << " bytes";
    ++_call_count;

    // ----------------------
    // Actions in first call:
    // ----------------------
    // - Synchronize the host to device stream
    // - Handle spillover data
    // - Swap the input buffer (copy destination)
    // - Make an async copy from the host to device -> input data comes from DADA buffer
    CUDA_ERROR_CHECK(cudaStreamSynchronize(host2dev_stream));
    input_buffer.swap();

    // Copy new data from host to device
    CUDA_ERROR_CHECK(cudaMemcpyAsync(
        static_cast<void *>(reinterpret_cast<char *>(input_buffer.a_ptr())),
        static_cast<void *>(block.ptr()),
        _buffer_bytes, cudaMemcpyHostToDevice, host2dev_stream)
    );

    if (_call_count == 1) {return false;} // Return if it is the first call

    // -----------------------
    // Actions in second call:
    // ----------------------
    // - Swap the packetized buffer
    // - Process the input data -> convert from float to 2bit
    CUDA_ERROR_CHECK(cudaStreamSynchronize(processing_stream));

    packetized_buffer.swap();

    if constexpr (std::is_same<uint64_t, InputType>::value)
    {
        if(_input_bit_depth == 8)
        {
            unpacker->unpack<8>(input_buffer.b(), unpacked_buffer);
        }
        else if(_input_bit_depth == 10)
        {
            unpacker->unpack<10>(input_buffer.b(), unpacked_buffer);
        }
        else if(_input_bit_depth == 12)
        {
            unpacker->unpack<12>(input_buffer.b(), unpacked_buffer);
        }
        else
        {
            throw std::runtime_error("Unsupported bit depth with tempalte typename uint64_t");
        }
        vdif_packer->process(unpacked_buffer, packetized_buffer.a(), processing_stream);
    }
    if constexpr (std::is_same<float, InputType>::value)
    {
        vdif_packer->process(input_buffer.b(), packetized_buffer.a(), processing_stream);
    }

    if ((_call_count == 2)) { return false; }

    // ----------------------
    // Actions in third call:
    // ----------------------
    // - Swap the output buffer
    // - Copy the 2bit payload data back to the host with a 2D async copy.
    //    leaving space between payloads for header data
    // - Compute headers and copy them to the host memory
    CUDA_ERROR_CHECK(cudaStreamSynchronize(dev2host_stream));
    output_buffer.swap();
    CUDA_ERROR_CHECK(cudaMemcpy2DAsync(
        (void *)(output_buffer.a_ptr() + vdif_header_size),
        _n_bytes_payload + vdif_header_size,
        (void *)thrust::raw_pointer_cast(packetized_buffer.b_ptr()),
        _n_bytes_payload, _n_bytes_payload,
        _n_frames_per_block, cudaMemcpyDeviceToHost, dev2host_stream));

    update_vdif_headers();

    if (_call_count == 3) {return false;}

    // -----------------------------------
    // Actions in fourth call and onwards:
    // -----------------------------------
    // - Wrap the output buffer data into a RawBytes object (prepare for DADA copy)
    // - Copy data to the DADA buffer
    psrdada_cpp::RawBytes bytes(
        reinterpret_cast<char *>(output_buffer.b_ptr()),
        output_buffer.b().size(), output_buffer.b().size());

    BOOST_LOG_TRIVIAL(debug) << "Copy " << output_buffer.b().size() * 4<< " bytes to DADA buffer ";
    _handler(bytes);
    return false;
}

template <typename HandlerType, typename InputType>
void Pipeline<HandlerType, InputType>::update_vdif_headers()
{
    // fill in header data
    const uint32_t samples_per_dataframe = _n_bytes_payload * 8 / _output_bit_depth;
    const uint32_t data_frames_per_second = _sample_rate * _n_channels / samples_per_dataframe;

    BOOST_LOG_TRIVIAL(debug) << "Samples per data frame: " << samples_per_dataframe;
    BOOST_LOG_TRIVIAL(debug) << "Dataframes per second: " << data_frames_per_second;

    for (uint32_t ib = 0; ib < output_buffer.a().size(); ib += vdif_header.getDataFrameLength() * 8)
    {
        size_t i = ib / vdif_header.getDataFrameLength() / 8;
        // copy header to correct position
        std::copy(
            reinterpret_cast<uint8_t*>(vdif_header.getData()),
            reinterpret_cast<uint8_t*>(vdif_header.getData()) + vdif_header_size,
            output_buffer.a().begin() + ib);
        // DEBUG log
        if (i < 5)
        {
            BOOST_LOG_TRIVIAL(debug) << i << " Dataframe Number: " << vdif_header.getDataFrameNumber()
                << "\tTimestamp: " << vdif_header.getTimestamp();
        }
        // invalidate rest of data so it can be dropped later.
        // Needed so that the outputbuffer can have always the same size
        if (i < _n_frames_per_block)
        {
            vdif_header.setValid(true);
        }
        else
        {
            vdif_header.setValid(false);
            continue;
        }
        // increase dataframenumber
        vdif_header.setDataFrameNumber(vdif_header.getDataFrameNumber() + 1);
        // update header
        if (vdif_header.getDataFrameNumber() >= data_frames_per_second)
        {
            vdif_header.setDataFrameNumber(0);
            vdif_header.setSecondsFromReferenceEpoch(vdif_header.getSecondsFromReferenceEpoch() + 1);
            BOOST_LOG_TRIVIAL(debug) << "After " << i+1 << " frames, beginning new second after epoch: "
                << vdif_header.getSecondsFromReferenceEpoch();
        }
    }
}

} // edd_dbbc
} // vdif
