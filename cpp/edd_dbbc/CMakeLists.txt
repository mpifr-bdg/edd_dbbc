file(GLOB EDD_DBBC_INC "${CMAKE_CURRENT_SOURCE_DIR}/*.h" "${CMAKE_CURRENT_SOURCE_DIR}/*.cuh")
file(GLOB EDD_DBBC_SRC "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp" "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cu")
include_directories(${PARENT_CPP_DIR})

add_library(${CMAKE_PROJECT_NAME} ${EDD_DBBC_SRC})
target_link_libraries(${CMAKE_PROJECT_NAME} PUBLIC ${DEPENDENCY_LIBRARIES})

add_subdirectory(ddc)
add_subdirectory(vdif)

if(ENABLE_TESTING)
  add_subdirectory(test)
endif()

if(ENABLE_BENCHMARK)
  add_custom_target(run_benchmark
    COMMAND ddc_benchmark --benchmark_counters_tabular=true
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
endif()


# --------------------------------------------------------------------- #
# Install targets: lib, bin and include - Also export the cmake project #
# --------------------------------------------------------------------- #
file(GLOB EDD_DBBC_INC "${CMAKE_CURRENT_SOURCE_DIR}/*.h" "${CMAKE_CURRENT_SOURCE_DIR}/*.cuh")
install (TARGETS ${CMAKE_PROJECT_NAME}
    EXPORT ${CMAKE_PROJECT_NAME}Targets
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    INCLUDES DESTINATION include/)
install(FILES ${EDD_DBBC_INC} DESTINATION include/edd_dbbc)

# Generate the config file for find_package()
include(CMakePackageConfigHelpers)
set(GENEREATED_DIR "${CMAKE_BINARY_DIR}/generated" CACHE INTERNAL "")
set(CMAKE_FILES_INSTALL_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/${CMAKE_PROJECT_NAME}")
set(VERSION_FILE "${GENEREATED_DIR}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake")

# Use CMake generator to create version and config files
write_basic_package_version_file(${VERSION_FILE} VERSION ${PROJECT_VERSION} COMPATIBILITY AnyNewerVersion)
set(CONFIG_FILE "${GENEREATED_DIR}/${CMAKE_PROJECT_NAME}Config.cmake")
configure_package_config_file("${CMAKE_SOURCE_DIR}/cmake/${CMAKE_PROJECT_NAME}Config.cmake.in" "${CONFIG_FILE}" INSTALL_DESTINATION lib/cmake/${CMAKE_PROJECT_NAME})

# Create cmake folder and export the cmake project
install(EXPORT ${CMAKE_PROJECT_NAME}Targets
    FILE ${CMAKE_PROJECT_NAME}Targets.cmake
    NAMESPACE ${CMAKE_PROJECT_NAME}::
    DESTINATION lib/cmake/${CMAKE_PROJECT_NAME})

# Install the generated files in the cmale folder
install(FILES ${VERSION_FILE} ${CONFIG_FILE}
  COMPONENT "${PROJECT_NAME}"
  DESTINATION lib/cmake/${CMAKE_PROJECT_NAME})
