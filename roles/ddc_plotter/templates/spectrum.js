result = {}
data.series[0].fields.forEach((object) => {
  result[object["name"]] = object["values"].buffer[0]
});

const uint8Array = new Uint8Array(atob(result["data"]).split('').map(char => char.charCodeAt(0)));
// Create a DataView on the Uint8Array
const dataView = new DataView(uint8Array.buffer);
// Get the number of elements in the array (1024 in this example)
const numElements = dataView.byteLength / 4;
// Create a new Float32Array with the same number of elements
const x = new Float32Array(numElements);

const v_min = result["f_min"];
const v_max = result["f_max"];
const bw = v_max - v_min
const res = bw / numElements;


const y = new Float32Array(numElements);

for (let i = 0; i < numElements; i++) {
  y[i] = dataView.getFloat32(i * 4, true); // true specifies little-endian byte order
  x[i] = v_min + i * res
}
var trace = {
  y: Array.from(y),
  x: Array.from(x)
};

return {data:[trace]};
