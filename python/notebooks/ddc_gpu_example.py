"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Proof of concept of the DDC algorithm implemented using python and CUDA
"""

import cupy as cp
import numpy as np
import cusignal
import scipy.signal as ss
from time import time

def hilbert_fir(ntaps,a,w1,w2,d):
    """
    Description: Calculates filter coeffiecents for an Hilbert bandpass filter
    """
    from math import sin, cos, pi, sqrt
    output=[]

    for i in range(ntaps):
        t = 2 * pi * (i - (ntaps-1)/2)
        if(t == 0):
            o = sqrt(2)*(w2-w1)
        elif(t == pi/2*a):
            o = a*(sin(pi/4*((a+2*w2)/a))-sin(pi/4*((a+2*w1)/a)))
        elif(t == -pi/2*a):
            o = a*(sin(pi/4*((a-2*w1)/a))-sin(pi/4*((a-2*w2)/a)))
        else:
            o = 2*(pi**2)*cos(a*t)/(t*(4*(a**2)*(t**2)-pi**2))*(sin(w1*t+pi/4)-sin(w2*t+pi/4))
        output.append(o)
    if(d==1):
        output.reverse()
    return output


h_coeff_r = hilbert_fir(301, 0.05, 0.05, 0.45,0)
h_coeff_i = hilbert_fir(301, 0.05, 0.05, 0.45,1)
up = 1
down = 16
f_lo = [4e5]#,2e5,5e5,7e5,2e5,9e5,1e5,2e5] # Frequency of the local oscillator
fs_ini = 4e6   # Sample frequency of our initial signal
sig_dur = 1 # Duration of the signal
fs_out = fs_ini * up / down
t_ini = np.linspace(0,sig_dur, int(fs_ini*sig_dur)) # time vector for inital time series
f0 = 0 # Start freq of the chirp
f1 = fs_ini//4 # End freq of the chirp
t1 = sig_dur # Time to which end freq is reached
sig = ss.chirp(t_ini, f0=f0, f1=f1, t1=sig_dur)
osc = np.zeros((len(f_lo), len(t_ini)),dtype='complex64')


for i,lo in enumerate(f_lo):
    osc[i] = np.exp(-2j*np.pi*lo*t_ini)

si = cp.asarray(sig)
lo = cp.asarray(osc)

for run in range(15):
    start = time()

    # mx = cp.dot(lo, si.T)
    mx = si * lo
    rs = cusignal.resample_poly(mx, up, down, window=('kaiser', 0.5), axis=1)
    re = cusignal.lfilter(h_coeff_r, 1, rs.real)
    im = cusignal.lfilter(h_coeff_i, 1, rs.imag)
    ls_gpu = re + im
    us_gpu = re - im

    stop = time()
    print("cuSig: Time:" + str(stop - start) + "s; rate " +str(len(t_ini)/((stop - start)*1e9))+" GS/s")

for run in range(1):
    start = time()

    # mx = np.dot(osc, sig.T)
    mx = sig * osc
    rs = ss.resample_poly(mx, up, down, window=('kaiser', 0.5), axis=1)
    re = ss.lfilter(h_coeff_r, 1, rs.real)
    im = ss.lfilter(h_coeff_i, 1, rs.imag)
    ls_cpu = re + im
    us_cpu = re - im

    stop = time()
    print("scipy: Time:" + str(stop - start) + "s; rate " +str(len(t_ini)/((stop - start)*1e9))+" GS/s")
print(mx.shape, rs.shape, re.shape, ls_cpu.shape)

ls_gpu = cp.asnumpy(ls_gpu)
us_gpu = cp.asnumpy(us_gpu)

lsb_fft_cpu = np.abs(np.fft.rfft(ls_cpu))
usb_fft_cpu = np.abs(np.fft.rfft(us_cpu))
lsb_fft_gpu = np.abs(np.fft.rfft(cp.asnumpy(ls_gpu)))
usb_fft_gpu = np.abs(np.fft.rfft(cp.asnumpy(us_gpu)))

import matplotlib.pyplot as plt
for i in range(len(ls_cpu)):
    x_time = np.linspace(0, sig_dur, len(ls_cpu[i]))
    x_freq_lsb = np.linspace(-f_lo[i],fs_out//2-f_lo[i], len(lsb_fft_cpu[i]))
    x_freq_usb = np.linspace(f_lo[i], fs_out//2+f_lo[i], len(lsb_fft_cpu[i]))
    fig = plt.figure(figsize=(12,8), tight_layout=True)
    sub = plt.subplot(221)
    sub.plot(x_time, ls_cpu[i], label="Lower")
    sub.plot(x_time, us_cpu[i], label="Upper")
    sub.set_title("CPU")
    sub.legend()
    sub = plt.subplot(222)
    sub.plot(x_time, ls_gpu[i], label="Lower")
    sub.plot(x_time, us_gpu[i], label="Upper")
    sub.set_title("GPU")
    sub.legend()
    sub = plt.subplot(223)
    sub.plot(-x_freq_lsb, lsb_fft_cpu[i])
    sub.plot(x_freq_usb, usb_fft_cpu[i])
    sub.grid(which='both', axis='both')
    sub = plt.subplot(224)
    sub.plot(-x_freq_lsb, lsb_fft_gpu[i])
    sub.plot(x_freq_usb, usb_fft_gpu[i])
    sub.grid(which='both', axis='both')
    plt.savefig("dbbc_output " +str(i)+ ".png")
