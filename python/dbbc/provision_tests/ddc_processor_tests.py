import asyncio
import json

from mpikat.core import logger
from mpikat.utils.testing.provision_tests import register, Stage, EDDTestContext

_LOG = logger.getLogger('mpikat.dbbc.testing.provision_tests.ddc_processor')

@register(stage=Stage.capture_start, class_name='DigitalDownConverter')
async def test_is_sending_data(context: EDDTestContext):

    """
    The pipeline should send data.
    """
    # Check number of expected polarizations
    _LOG.debug('Checking output data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))

    active_polarizations = [i['polarization'] for i in cfg['input_data_streams']]
    _LOG.debug('Active polarizations: %s', active_polarizations)
    await asyncio.sleep(1)

    i1 = {}
    for p in active_polarizations:
        _, results = await context.pipeline.request("sensor-value", f'output-buffer-total-read-polarization-{p}')
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _LOG.debug('Received output buffer reads: %s', i1)

    for p in i1:
        assert i1[p] > 0, f"No output buffers processed for polarization {p}, got {i1[p]}  and {i1[p]} buffers "


@register(stage=Stage.capture_start, class_name='DigitalDownConverter')
async def test_is_receiving_data(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    # Check number of expected polarizations
    _LOG.debug('Checking input data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))
    active_polarizations = [i['polarization'] for i in cfg['input_data_streams']]
    _LOG.debug('Active polarizations: %s', active_polarizations)
    await asyncio.sleep(1)

    i1 = {}
    for p in active_polarizations:
        _, results = await context.pipeline.request("sensor-value", f'input-buffer-total-write-polarization-{p}')
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _LOG.debug('Received input buffer reads: %s', i1)

    for p in i1:
        assert i1[p] > 0, f"No input buffers processed for polarization {p}, got {i1[p]}  and {i1[p]} buffers "


@register(stage=Stage.capture_start, class_name='DigitalDownConverter')
async def test_is_bottleneck(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    # Check number of expected polarizations
    _LOG.debug('Checking buffer fill levels')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))
    active_polarizations = [i['polarization'] for i in cfg['input_data_streams']]
    _LOG.debug('Active polarizations: %s', active_polarizations)

    await asyncio.sleep(10)

    i1 = {}
    for p in active_polarizations:
        _, results = await context.pipeline.request("sensor-value", f'input-buffer-fill-level-polarization-{p}')
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _LOG.debug('Received input buffer reads: %s', i1)

    for p in i1:
        assert i1[p] < 0.1, f"Buffer fill level higher than allowed for polarization{p}"
