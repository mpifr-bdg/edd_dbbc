import asyncio
import json
import os
import numpy as np
import matplotlib.pyplot as plt
from mpikat.core import logger
from mpikat.utils.testing.provision_tests import register, Stage, EDDTestContext
from mpikat.pipelines.gated_spectrometer.plotter import encodeb64_figure

from dbbc.vdif import Reader, HEADER_SIZE

_LOG = logger.getLogger('mpikat.dbbc.testing.provision_tests.vdif_packer')

def ascii_get_header(hdr: str, name: str) -> str:
    """Retrieve the data from a DADA header parameter

    Args:
        hdr (str): The header string
        name (str): The parameters name

    Returns:
        str: The value of the parameter
    """
    for line in hdr:
        l = line.split(" ")
        l = [x for x in l if x]
        if len(l) < 2:
            continue
        if l[0] == name:
            return l[1]
    return None

def get_subdirs(config: dict, resp: dict) -> list:
    """Retrieve the sub directories from the pipeline after a measurement-stop

    Args:
        config (dict): The configuration dictioanry
        resp (dict): The pipeline response on measurement-stop

    Returns:
        list: List of subdirs
    """
    subdirs: list = []
    for path, meta in resp.items():
        if config["id"] == meta["pipeline_id"]:
            subdirs.append(path)
    return subdirs

def fold(data: np.ndarray, fs: int, nbin: int) -> np.ndarray:
    """Fold the data aligned to 1 second

    Args:
        data (np.ndarray): Raw voltage data
        fs (int): sampling rate
        nbin (int): binning -> number of points per second

    Returns:
        np.ndarray: 2D array slow time vs fast time -> detected power
    """
    inte = fs // nbin
    l = len(data) // fs
    return (np.abs(data.reshape(-1, inte))**2).sum(axis=-1).reshape(l, nbin)


async def get_pipeline_config(context: EDDTestContext) -> dict:
    """Get the configuration of the pipeline

    Args:
        context (EDDTestContext): The Test context

    Returns:
        dict: The configuration of the pipeline
    """
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    return json.loads(result[0].arguments[4].decode('ascii'))


@register(stage=Stage.measurement_start, class_name='VDIFPacker')
async def test_is_receiving_data(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    # Check number of expected polarizations
    _LOG.debug('Checking input data streams')
    _, result = await context.pipeline.request("sensor-value", 'current-config')
    cfg = json.loads(result[0].arguments[4].decode('ascii'))
    active_polarizations = set([i['polarization'] for i in cfg['input_data_streams']])
    _LOG.debug('Active polarizations: %s', active_polarizations)

    await asyncio.sleep(cfg["file_duration"] * 2)

    i1 = {}
    for p in active_polarizations:
        _LOG.debug("Requesting sensor %s", f'input-buffer-total-write')
        _, results = await context.pipeline.request("sensor-value", f'input-buffer-total-write')
        _LOG.debug("Results %s", results)
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _LOG.debug('Received input buffer reads: %s', i1)

    for p in i1:
        assert i1[p] > 0, f"No input buffers processed for polarization {p}, got {i1[p]}  and {i1[p]} buffers "

@register(stage=Stage.measurement_start, class_name='VDIFPacker')
async def test_create_file(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    cfg = await get_pipeline_config(context)
    await asyncio.sleep(cfg["file_duration"] * 3)

@register(stage=Stage.measurement_stop, class_name='VDIFPacker')
async def test_file_size(context: EDDTestContext):
    cfg = await get_pipeline_config(context)
    resp: dict = context.stage_response
    if cfg["output_type"] == "disk":
        subdirs = get_subdirs(cfg, resp)
        duration = int(cfg["file_duration"])
        # Iterate over subdirs, every VDIF file gets processed
        for path in subdirs:
            fname = os.path.join(path, sorted(os.listdir(path))[0])
            fsize = os.path.getsize(fname)
            # Open the first file in the subdir
            with open(fname, "rb") as f:
                reader = Reader(f, 4096)
                frames_per_file = reader.frames_per_second * duration
                expected_size = frames_per_file * reader.frame_size + reader.offset
                _LOG.info("Filename %s, size: %d", fname, fsize)
                _LOG.info("Expected size: %d", expected_size)
            assert(fsize == expected_size), f"Expected size {expected_size} not equal to actual size {fsize}"
    else:
        _LOG.warning("No file written because 'output_type' is %s and not 'disk'", cfg["output_type"])


@register(stage=Stage.measurement_stop, class_name='VDIFPacker')
async def test_number_of_directories(context: EDDTestContext):
    """Test the number of directories -> must match number of output stream

    Args:
        context (EDDTestContext): _description_
    """
    cfg = await get_pipeline_config(context)
    if cfg["output_type"] == "disk":
        subdirs: list = get_subdirs(cfg, context.stage_response)
        _LOG.info("subdirs %s, ostreams: %d", str(subdirs), len(cfg["output_data_streams"]))
        assert(len(subdirs) == len(cfg["output_data_streams"])), "Number of output data streams does not match number of subdirectories"
    else:
        _LOG.warning("No file written because 'output_type' is %s and not 'disk'", cfg["output_type"])


@register(stage=Stage.measurement_stop, class_name='VDIFPacker', tbc=True)
async def test_folding_noise_diode(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    cfg = await get_pipeline_config(context)
    resp: dict = context.stage_response
    subdirs = get_subdirs(cfg, resp)
    odata = []
    # Iterate over subdirs, every VDIF file gets processed
    for path in subdirs:
        fname = os.path.join(path, sorted(os.listdir(path))[0])
        fsize = os.path.getsize(fname)
        _LOG.info("Filename %s, size: %d", fname, fsize)
        # Open the first file in the subdir
        with open(fname, "rb") as f:
            # Retrieve the DADA header
            hdr = f.read(4096).decode("utf-8").split("\n")
            scs = int(float(ascii_get_header(hdr, "SAMPLE_CLOCK_START")))
            ts = int(float(ascii_get_header(hdr, "SYNC_TIME")))
            fs = int(float(ascii_get_header(hdr, "SAMPLE_CLOCK")))
            _LOG.info("SCS %d, TS %d, FS %d", scs, ts, fs)
            start_time = ts + scs / fs
            # Read out the data
            _LOG.info("Opening file")
            reader = Reader(f, 4096)
            _LOG.info("First VDIF header %s", reader.header)
            _LOG.info("Reading file")
            data = reader.read_data()[0]
            _LOG.info("Data length %d", len(data))
            cutoff = len(data) // fs
            _LOG.info("Processing file")
            odata.append(fold(data[:cutoff*fs], fs, 100))
            _LOG.info("Processed %s", fname)
    # Plot the data
    fig = plt.figure(num=len(subdirs), figsize=(16,12))
    fig.suptitle(f"SYNC_TIME: {ts} SAMPLE_CLOCK: {fs} SAMPLE_CLOCK_START {scs} -> First timestamp: {start_time}")
    for i in range(len(odata)):
        sub = fig.add_subplot(len(odata), 1, i+1)
        sub.imshow(odata[i], aspect="auto", interpolation="nearest")
        sub.set_xlabel('ms')
        sub.set_ylabel('s')
        sub.set_title(f"{path.split('/')[0]}")
    plt.subplots_adjust(top=0.85)  # Increase this value if needed
    context.output.append(encodeb64_figure(fig).decode('ascii'))


@register(stage=Stage.measurement_start, class_name='VDIFPacker')
async def test_is_bottleneck(context: EDDTestContext):
    """
    The pipeline should send data.
    """
    # Check number of expected polarizations
    _LOG.debug('Checking input data streams')
    cfg = await get_pipeline_config(context)
    active_polarizations = set([i['polarization'] for i in cfg['input_data_streams']])
    _LOG.debug('Active polarizations: %s', active_polarizations)

    await asyncio.sleep(cfg["file_duration"] * 2)

    i1 = {}
    for p in active_polarizations:
        _, results = await context.pipeline.request("sensor-value", f'input-buffer-fill-level')
        i1[p] = float(results[0].arguments[4].decode('ascii'))
    _LOG.debug('Received input fille level: %s', i1)

    for p in i1:
        assert i1[p] < 0.1, f"Buffer fill level higher than allowed for polarization{p}"
