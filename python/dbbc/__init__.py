from mpikat.utils import dada_tools as dada
import datetime


def get_unit(num: float) -> tuple:
    """_summary_

    Args:
        num (float): _description_

    Returns:
        tuple: _description_
    """
    if num > 1e9:
        return 1e9, "G"
    elif num > 1e6:
        return 1e6, "M"
    elif num > 1e3:
        return 1e3, "k"
    return 1, ""

def next_pot(val: float) -> int:
    """Get the next power of two.

    Args:
        val (float): value to evaluate

    Returns:
        int: next power of 2
    """
    result = 1
    while result <= val:
        result *= 2
    return int(result)

def next_smallest_pot(val: float) -> int:
    """Get the next smallest power of two.

    Args:
        val (float): value to evaluate

    Returns:
        int: next smallest power of 2
    """
    return int(next_pot(val)//2)

def nearest_pot(val: float) -> int:
    """Get the nearest power of two, either the next smaller or the next greater.

    Args:
        val (float): value to evaluate

    Returns:
        int: next power of 2
    """
    great = next_pot(val)
    small = next_smallest_pot(val)
    if val - small < great - val:
        return small
    return great

def round_nacc2time(sample_rate: float, interval: float, nfft: int) -> int:
    """ Returns the number of fft accumulations, for a given sample rate and interval.
        The returned number is a power of two which is next to the desired time interval.

    Args:
        sample_rate (float): sampling rate of the time domain data
        interval (float): The desired interval to round to
        nfft (int): the number of FFT points

    Returns:
        int: nearest power of two the desired interval
    """
    return nearest_pot(sample_rate * interval / (nfft * 2))

def sanitize_dictionary(conf: dict, default: dict) -> dict:
    """sanitizes a dictionary, meaning that param conf is filled with missing
    keys from param "default"

    Args:
        conf (dict): actual dictionary
        default (dict): dictionary with desired keys and default values

    Returns:
        dict: sanitzied dictionary
    """

    if not isinstance(conf, dict):
        conf = {}
    for key, val in default.items():
        if key not in conf:
            conf[key] = val
    return conf


def folder_string(timestamp: float, prefix: str="/mnt", suffix: str="") -> str:
    """_summary_

    Args:
        timestamp (float): _description_
        prefix (str, optional): _description_. Defaults to "/mnt".
        suffix (str, optional): _description_. Defaults to "".

    Returns:
        str: _description_
    """
    time = datetime.datetime.fromtimestamp(timestamp)
    return f"{prefix}/{time.year}/{time.month}/{time.day}/{time.hour}/{time.minute}/{suffix}"


def make_default_header(kwargs: dict) -> str:
    """Create a default header file by the given dictionary

    Args:
        kwargs (dict): The dictionary

    Returns:
        str: The filename
    """
    default_header = dada.DadaHeader()
    default_header.add(dada.DadaBaseHeader())
    for k, v in kwargs.items():
        default_header.set(k, v)
    return default_header.write()