#!@Python3_EXECUTABLE@
"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

Limitations

Configuration Settings

    output_type (string):
        Options are 'network', 'disk' or any.
            1. Use 'network' for streaming VDIF to the network.
            2. Use 'disk' to store the data. Related params are 'file_size' and
                'output_directory'. The latter should not be set.
            3. Use any arbitary stream to forward the output to /dev/null
"""
# Python natives
import time
import os
import json
import asyncio
from typing import List
# Third party
from aiokatcp.sensor import Sensor
from aiokatcp.connection import FailReply
import numpy as np
# EDD modules
import mpikat.utils.async_util # pylint: disable=unused-import
from mpikat.utils.process_tools import ManagedProcess, ManagedProcessPool
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog, conditional_update
from mpikat.utils.core_manager import CoreManager
from mpikat.utils import numa
from mpikat.utils import mk_tools as mk
from mpikat.utils import dada_tools as dada
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
import mpikat.core.logger as logging

from dbbc import sanitize_dictionary, folder_string, make_default_header
from dbbc import vdif
from dbbc.data_streams import DDCDataStreamFormat as DDCFormat
from dbbc.data_streams import VDIFDataStreamFormat as VDIFFormat

# Logging for VDIFPacker
_LOG = logging.getLogger("mpikat.dbbc.pipeline.vdif_packer")

# The default config
_DEFAULT_CONFIG = {

    "id": "VDIFPacker",
    "type": "VDIFPacker",
    "samples_per_block": "auto",
    "minimum_size":2**16,
    "file_duration":60, # in seconds
    "log_level": "debug",

    "output_directory": "/mnt",
    "output_type": 'disk', # ['network', 'disk', 'null']
    "input_type": 'network', # Use dummy input instead of mkrecv process.
    "output_rate_factor": 1.10,
    "nonfatal_numacheck":False,
    "merge_type":"single", # or "polarization", "thread"
    "thread_ids":[0,1,2,3],
    "station_id": 0,
    "input_data_streams":
    [
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.123",
            "port": "7148",
            "name": "polarization_0_0_lower",
            "bit_depth" : 32,
            "sample_rate" : int(32e6),
            "samples_per_heap": 2048,
            "heap_size": 8192,
            "polarization":0,
            "channel":0
        },
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.124",
            "port": "7148",
            "name": "polarization_0_0_upper",
            "bit_depth" : 32,
            "sample_rate" : int(32e6),
            "samples_per_heap": 2048,
            "heap_size": 8192,
            "polarization":0,
            "channel":1
        },
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.125",
            "port": "7148",
            "name": "polarization_1_0_lower",
            "bit_depth" : 32,
            "sample_rate" : int(32e6),
            "samples_per_heap": 2048,
            "heap_size": 8192,
            "polarization":1,
            "channel":0
        },
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.126",
            "port": "7148",
            "name": "polarization_1_0_upper",
            "bit_depth" : 32,
            "sample_rate" : int(32e6),
            "samples_per_heap": 2048,
            "heap_size": 8192,
            "polarization":1,
            "channel":1
        }
    ],
    "output_data_streams":
    {
        "polarization_0_0_lower" :
        {
            "format": "VDIF:1",
            "ip": "auto",
        },
        "polarization_0_0_upper" :
        {
            "format": "VDIF:1",
            "ip": "auto",
        },
        "polarization_1_0_lower" :
        {
            "format": "VDIF:1",
            "ip": "auto",
        },
        "polarization_1_0_upper" :
        {
            "format": "VDIF:1",
            "ip": "auto",
        }
    }
}

def get_vdifsend_command(desc, physcpu=None):
    """
    Creates the command for the VDIF send
    """
    cmd = []
    if physcpu:
        cmd.append(f'taskset -c {physcpu} vdif_send')
    else:
        cmd.append('vdif_send')
    cmd.append(f'--input_key {desc["input_key"]}')
    cmd.append(f'--if_ip {desc["nic"]}')
    cmd.append(f'--dest_ip {desc["ip"]}')
    cmd.append(f'--port {desc["port"]}')
    cmd.append(f'--max_rate {desc["data_rate"]}')
    _LOG.debug("VDIF send command %s", ' '.join(cmd))
    return ' '.join(cmd)


def get_vdifpacker_command(desc, physcpu=None):
    """
    Creates the command for the VDIF packer
    """
    cmd = []
    if physcpu:
        cmd.append(f'taskset -c {physcpu} vdif_packer')
    else:
        cmd.append('vdif_packer')
    cmd.append(f'--input_key {str(desc["input_key"])}')
    cmd.append(f'--output_key {str(desc["output_key"])}')
    cmd.append(f'--channels {int(desc["channels"])}')
    cmd.append(f'--thread_id {int(desc["thread_id"])}')
    cmd.append(f'--station_id {int(desc["station_id"])}')
    cmd.append(f'--payload_size {int(desc["payload_size"])}')
    cmd.append(f'--input_batch {int(desc["input_batch"])}')
    cmd.append(f'--sample_rate {int(desc["sample_rate"])}')
    _LOG.debug("VDIF Packer command %s", ' '.join(cmd))
    return ' '.join(cmd)


def get_samples_per_block(vdif_payload: int, spead_payload: int, fs: int) -> int:
    """_summary_

    Args:
        vdif_payload (int): _description_
        spead_payload (int): _description_
        fs (int): _description_

    Returns:
        int: _description_
    """
    return np.lcm(np.lcm(int(vdif_payload), int(spead_payload)), fs)



class VDIFPacker(EDDPipeline):
    """VDIFPacker pipeline class."""

    DATA_FORMATS = [VDIFFormat.stream_meta]

    def __init__(self, ip: str, port: int, loop: asyncio.AbstractEventLoop=None):
        """
        Description:
            Construct the VDIFPacker pipeline.

        Args:
            - ip (str): The ip of the KATCP server
            - port (int): The port of the KATCP server
            - loop (asyncio.AbstractEventLoop, optinal): Defaults to None
        """
        # Call to the base class for constructing the pipeline
        super().__init__(ip, port, default_config=dict(_DEFAULT_CONFIG), loop=loop)
        self._subprocess_pool = ManagedProcessPool()
        self._capture_pool = ManagedProcessPool()
        self._subprocessMonitor = SubprocessMonitor()
        self._buffer_sensors = {}
        self._buffer_key_map = {}
        self._dada_buffers: List[dada.DadaBuffer] = []
        self._input_buffer: dada.DadaBuffer = None
        self._watchdog: SensorWatchdog = None
        self._core_manager: CoreManager = None
        self._channel_set: set = set()
        self._polarization_set: set = set()
        self._node_id: int = None
        self._total_input_rate: float = 0
        self._measurement_ts: int = 0
        self._background_task: asyncio.tasks = None
        _LOG.info("VDIFPacker pipeline instantiated")


    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the EDD VLBi pipeline

        """
        _LOG.info("Configuring EDD backend for processing")

        # Ensure that the input_data_streams items provide all necessary entrys
        for desc in self._config["input_data_streams"]:
            desc = sanitize_dictionary(desc, DDCFormat.stream_meta)

        self.set_numa_node()
        self.remove_faulty_ostreams()
        self.validate_input_streams()
        input_buffer_size = 0
        self._core_manager = CoreManager(self._node_id)
        buffer_creation_tasks = []
        # Sort streams by channel and polarization
        self._istreams = sorted(self._config["input_data_streams"], key=lambda x: (x["polarization"], x["channel"]))
        for ii, desc in enumerate(self._istreams):
            _LOG.info("STREAM with channel: %s and polarization %s", desc["channel"], desc["polarization"])
            self._samples_per_heap = desc["samples_per_heap"]
            self._polarization_set.add(str(desc["polarization"]))
            self._channel_set.add(str(desc["channel"]))
            self._sample_rate = desc["sample_rate"]
            # ------------------------- #
            # Do necessary calculations #
            # ------------------------- #
            vdif_payload_size = int(vdif.payload_size(self._sample_rate))
            if self._config["samples_per_block"] == "auto":
                self._config["samples_per_block"] = int(get_samples_per_block(
                    vdif_payload_size, self._samples_per_heap, self._sample_rate))
            inter_buffer_size = int(self._config["samples_per_block"] * desc["bit_depth"]) // 8
            input_buffer_size += inter_buffer_size
            frames_per_block = (self._config["samples_per_block"] * 2) // (vdif_payload_size * 8)
            output_buffer_size = (vdif_payload_size + vdif.HEADER_SIZE) * frames_per_block
            input_rate = self._sample_rate * desc["bit_depth"]
            output_data_rate = input_rate * 2 / desc["bit_depth"]
            self._total_input_rate += input_rate / 8
            _LOG.info('Paramters for output streams:\n\
            sample rate per channel:    %f \n\
            raw data rate per channel:  %f Mb/s\n\
            buffer slot size:           %d',\
                self._sample_rate, output_data_rate/(1000**2), output_buffer_size)
            # ------------------- #
            # Create Dada Buffers #
            # ------------------- #
            self._dada_buffers.append(dada.DadaBuffer(inter_buffer_size, pin=True, lock=True, node_id=self._node_id))
            buffer_creation_tasks.append(asyncio.create_task(self._dada_buffers[-1].create()))
            self._dada_buffers.append(dada.DadaBuffer(output_buffer_size, pin=True, lock=True, node_id=self._node_id))
            buffer_creation_tasks.append(asyncio.create_task(self._dada_buffers[-1].create()))
            # ------------------------------------- #
            # Assign output_data_stream definitions #
            # ------------------------------------- #
            name = desc["name"]
            if name not in self._config["output_data_streams"]:
                self._config["output_data_streams"][name] = {}
            self._config["output_data_streams"][name]["name"] = name
            self._config["output_data_streams"][name]["sync_time"] = desc["sync_time"]
            self._config["output_data_streams"][name]["center_freq"] = float(desc["center_freq"])
            self._config["output_data_streams"][name]["sample_rate"] = self._sample_rate
            self._config["output_data_streams"][name]["thread_id"] = self._config["thread_ids"][ii]
            self._config["output_data_streams"][name]["station_id"] = self._config["station_id"]
            self._config["output_data_streams"][name]["payload_size"] = vdif_payload_size
            self._config["output_data_streams"][name]["data_rate"] = output_data_rate
            self._config["output_data_streams"][name]["channels"] = 1
            self._buffer_key_map[name] = {}
            self._buffer_key_map[name]["input_key"] = self._dada_buffers[-2].key
            self._buffer_key_map[name]["output_key"] = self._dada_buffers[-1].key
            self.add_output_stream_sensor(self._dada_buffers[-2].key)
            self._core_manager.add_task(f"vdif_packer_{name}", 1)
            self._core_manager.add_task(f"vdif_output_{name}", 2)
        self._core_manager.add_task(f"splitter", 1)
        self._core_manager.add_task(f"capture", len(self._istreams)+1)

        self._input_buffer = dada.DadaBuffer(input_buffer_size, pin=True, lock=True, node_id=self._node_id)
        buffer_creation_tasks.append(asyncio.create_task(self._input_buffer.create()))
        await asyncio.gather(*buffer_creation_tasks)

        self.add_input_stream_sensor(self._input_buffer.key)
        self._input_buffer.get_monitor(self.buffer_status_handle).start()
        for buf in self._dada_buffers:
            buf.get_monitor(self.buffer_status_handle).start()

        self._configUpdated()
        _LOG.info("Final configuration %s", json.dumps(self._config, indent=4))


    @state_change(target="ready", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """
        start streaming output
        """
        _LOG.info("VDIFPacker received capture-start")


    @state_change(target="set", allowed=["ready"], intermediate="measurement_preparing")
    async def measurement_prepare(self, config: dict=None):
        """
        prepare the measurment
        """
        _LOG.info("VDIFPacker received measurement-prepare")
        # Create a timestamp which refers to the configure call
        # The timestamp is used for folder creation when "outout_type" equals "disk"
        self._measurement_ts = time.time()
        # If there is a background task await it and reset the dada buffers
        if self._background_task is not None:
            await self._background_task
            self._background_task = None
            tasks = []
            for buf in self._dada_buffers:
                tasks.append(asyncio.create_task(buf.reset()))
            tasks.append(asyncio.create_task(self._input_buffer.reset()))
            await asyncio.gather(*tasks)

        # Start output handler processes
        self._subprocessMonitor = SubprocessMonitor()
        self.start_splitter_process()
        for name, desc in self._config["output_data_streams"].items():
            self.start_output_handler(desc, name)
            self.start_vdif_packer(desc, name)
        self._subprocessMonitor.start()


    @state_change(target="measuring", allowed=["set"], intermediate="measurement_starting")
    async def measurement_start(self):
        """
        start measuring
        """
        _LOG.info("VDIFPacker received measurement-start")
        # Start the capture process
        try:
            self.start_input_handler()
        except Exception as err:
            _LOG.error("Error capture starting pipeline: %s", err)
            raise err
        # Attach a watchdog
        idx1_modulo = np.lcm(self._samples_per_heap, int(self._sample_rate))
        wd_timeout = (self._config["samples_per_block"] + idx1_modulo) / self._sample_rate * 16
        if wd_timeout < 10:
            wd_timeout = 10
        sensor = self._buffer_sensors[self._input_buffer.key]
        self._watchdog = SensorWatchdog(sensor["input_buffer_total_write"],
                                        wd_timeout, self.watchdog_error)
        self._watchdog.start()


    @state_change(target="ready", allowed=["measuring", "set"], ignored=["ready"],
                  intermediate="measurement_stopping")
    async def measurement_stop(self):
        """
        stop measuring output
        """
        _LOG.info("VDIFPacker received measurement-stop")
        if self._watchdog is not None:
            self._watchdog.stop_event.set()
        # Stop capture process / input handler
        self._capture_pool.terminate(threaded=True)
        self._capture_pool.clean()
        self._background_task = asyncio.to_thread(self.stop_subprocesses)


    @state_change(target="idle", intermediate="capture_stopping")
    async def capture_stop(self):
        """
        Stop streaming of data
        """
        _LOG.info("VDIFPacker received capture-stop")


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        deconfigure the pipeline.
        """
        _LOG.info("Deconfiguring VDIFPacker pipeline")
        if self._background_task is not None:
            await self._background_task
        else:
            self.stop_subprocesses()
        # Destroy the dada buffers
        _LOG.debug("Destroying dada buffers")
        tasks = []
        if self._input_buffer is not None:
            tasks.append(asyncio.create_task(self._input_buffer.destroy()))
        for buffer in self._dada_buffers:
            tasks.append(asyncio.create_task(buffer.destroy()))
        await asyncio.gather(*tasks)
        self._background_task = None
        self._input_buffer = None
        self._core_manager = None
        self._node_id = None
        self._total_input_rate = 0
        self._dada_buffers = []
        self._channel_set = set()
        self._polarization_set = set()


    def start_output_handler(self, desc: dict, name: str):
        """
        starts the VDIF output handle

        Args:
            desc (dict): _description_
            name (str): _description_
        """
        cpu = self._core_manager.get_coresstr(f"vdif_output_{name}")
        buffer_key = self._buffer_key_map[name]["output_key"]
        # Sending VDIF stream to the network
        if self._config["output_type"] == "network":
            nic, params = numa.getFastestNic(self._node_id)
            desc = dict(desc)
            desc["nic"] = params["ip"]
            desc["input_key"] = buffer_key
            cmd = get_vdifsend_command(desc, cpu)
            prc = ManagedProcess(cmd)
            _LOG.info("Sending data on NIC %s [ %s ] @ %s Mbit", nic, params['ip'],params['speed'])
        # Storing VDIF stream to the disk
        elif self._config["output_type"] == "disk":
            path = folder_string(self._measurement_ts, self._config["output_directory"], name)
            os.makedirs(path, mode=0o777, exist_ok=True)
            self.add_output_file(path)
            disk = dada.DbDisk(buffer_key, path)
            prc = disk.start()
            _LOG.info("Writing stream %s to disk @ %s ", name, path)
        # Forward VDIF stream to /dev/null
        else:
            null = dada.DbNull(buffer_key)
            prc = null.start()
            _LOG.warning("output type not known, will forward data to /dev/null")
        self.add_process(prc)

    def start_vdif_packer(self, desc: dict, name: str) -> None:
        """
        starts the VDIF packetization process

        Args:
            desc (dict): _description_
            name (str): _description_
        """
        desc = dict(desc) # Copy the dictioanry
        desc["input_key"] = self._buffer_key_map[name]["input_key"]
        desc["output_key"] = self._buffer_key_map[name]["output_key"]
        desc["input_batch"] = self._samples_per_heap
        cpu = self._core_manager.get_coresstr(f"vdif_packer_{name}")
        cmd = get_vdifpacker_command(desc, cpu)
        prc = ManagedProcess(cmd, env={"CUDA_VISIBLE_DEVICES": numa.getInfo()[self._node_id]['gpus'][0]})
        self.add_process(prc)

    def start_splitter_process(self) -> None:
        """
        Starts the DADA buffer splitter process
        """
        dada_key_list = [buf.key for buf in self._dada_buffers[::2]]
        proc = dada.DbSplitDb(self._input_buffer.key, dada_key_list,
            self._samples_per_heap * DDCFormat.stream_meta["bit_depth"] / 8,
            physcpu=self._core_manager.get_coresstr(f"splitter")).start()
        self.add_process(proc)


    def start_input_handler(self) -> None:
        """starts the capture process
        """
        # prepare the stream description for mkrecv_v4

        file_size = vdif.volume(self._sample_rate, self._config["file_duration"])

        if self._config['input_type'] == 'network':
            fastest_nic, nic_params = numa.getFastestNic(self._node_id)
            _LOG.info("Receiving data on NIC %s [ %s ] @ %s Mbit/s",
                    fastest_nic, nic_params['ip'], nic_params['speed'])
            mk_sensor = self._buffer_sensors[self._input_buffer.key]["mkrecv_sensors"]
            _LOG.info("Receive dictionary: %s", json.dumps(self._istreams, indent=4))
            handler = mk.Mkrecv(self._istreams, self._input_buffer.key,
                                self._core_manager.get_coresstr(f"capture"))
            handler.set_header("packet_size", 8400)
            handler.set_header("buffer_size", 128000000)
            handler.set_header("dada_nslots", 8)
            handler.set_header("ibv_if", nic_params["ip"])
            handler.set_header("file_size", file_size)
            handler.set_item("timestamp", "step", DDCFormat.samples_per_heap)
            handler.set_item("timestamp", "index", 1)
            handler.set_item("timestamp", "modulo", np.lcm(int(DDCFormat.samples_per_heap),
                                                           int(self._sample_rate)))
            handler.set_item("polarization", "index", 2)
            handler.set_item("channel", "index", 3)
            handler.stdout_handler(mk_sensor.stdout_handler)
            _LOG.info("Mkrecv command: %s", handler.cmd)
        else:
            _LOG.warning("Creating dummy input instead of listening to network!")
            default_header = make_default_header({"sample_clock_start":0, "file_size":file_size})
            handler = dada.JunkDb(self._input_buffer.key, default_header, self._total_input_rate, 3600)
        proc = handler.start()
        self._capture_pool.add(proc)


    def stop_subprocesses(self):
        """_summary_
        """
        if self._watchdog is not None:
            self._watchdog.stop_event.set()
        # Stop monitoring subprocesses
        if self._subprocessMonitor.is_alive():
            self._subprocessMonitor.stop()
        # Stop capture process / input handler
        self._capture_pool.terminate(threaded=True)
        self._capture_pool.clean()
        # Stop all other processes
        self._subprocess_pool.terminate(threaded=True)
        self._subprocess_pool.clean()


    def set_numa_node(self) -> None:
        """Sets the NUMA node used for processing
        """
        # Get NUMA node and its charachteristics
        for node_id, desc in numa.getInfo().items():
            # remove numa nodes with missing capabilities
            if len(desc['gpus']) < 1:
                _LOG.warning("Not enough gpus on numa node %i - removing from pool.", node_id)
            elif len(desc['net_devices']) < 1:
                _LOG.warning("Not enough nics on numa node %i - removing from pool.", node_id)
            else:
                _LOG.info("Using numa node %s", str(node_id))
                self._node_id = node_id
                return

        if self._node_id is None:
            if self._config["nonfatal_numacheck"]:
                _LOG.warning("No NUMA node found for processing, but nonfata_numacheck enabled")
                self._node_id = list(numa.getInfo().keys())[0]
                return
            _LOG.error("No NUMA node found for processing")
            raise FailReply("No NUMA node found for processing")

    def add_process(self, proc: ManagedProcess):
        """
        adds a process
        """
        self._subprocessMonitor.add(proc, self._subprocess_error)
        self._subprocess_pool.add(proc)

    def add_input_stream_sensor(self, key: str):
        """
        add sensors for i/o buffers for an input stream with given key.
        """
        if key not in self._buffer_sensors:
            self._buffer_sensors[key] = {}
        self._buffer_sensors[key]["mkrecv_sensors"] = mk.MkrecvSensors("")
        for s in self._buffer_sensors[key]["mkrecv_sensors"].sensors.values():
            self.sensors.add(s)
        self._buffer_sensors[key]["input_buffer_fill_level"] = Sensor(
            float, f"input-buffer-fill-level",
            description = f"Fill level of the input buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self._buffer_sensors[key]["input_buffer_total_write"] = Sensor(
            float, f"input-buffer-total-write",
            description = f"Total write into input buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._buffer_sensors[key]["input_buffer_fill_level"])
        self.sensors.add(self._buffer_sensors[key]["input_buffer_total_write"])
        self.mass_inform("interface-changed")

    def add_output_stream_sensor(self, key: str):
        """
        add sensors for i/o buffers for an output stream with given key
        """
        if key not in self._buffer_sensors:
            self._buffer_sensors[key] = {}
        self._buffer_sensors[key]["output_buffer_fill_level"] = Sensor(
            float, f"output-buffer-fill-level",
            description = f"Fill level of output buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self._buffer_sensors[key]["output_buffer_total_read"] = Sensor(
            float, f"output-buffer-total-read",
            description = f"Total read of output buffer",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._buffer_sensors[key]["output_buffer_total_read"])
        self.sensors.add(self._buffer_sensors[key]["output_buffer_fill_level"])
        self.mass_inform("interface-changed")


    def buffer_status_handle(self, status: dict):
        """
        Process a change in the buffer status
        """
        timestamp = time.time()
        if status["key"] not in self._buffer_sensors:
            return
        sensor = self._buffer_sensors[status['key']]
        # The buffer is an input buffer
        if status['key'] == self._input_buffer.key:
            conditional_update(sensor["input_buffer_total_write"], status['written'],
                               timestamp=timestamp)
            sensor["input_buffer_fill_level"].set_value(status['fraction-full'],
                                                        timestamp=timestamp)
        # The buffer is an output buffer
        if status['key'] in [buffer.key for buffer in self._dada_buffers[::2]]:
            sensor["output_buffer_fill_level"].set_value(status['fraction-full'],
                                                         timestamp=timestamp)
            conditional_update(sensor["output_buffer_total_read"], status['read'],
                               timestamp=timestamp)

    def validate_input_streams(self):
        streams = self._config["input_data_streams"]
        chan = []
        pol = set([l["polarization"] for l in streams])
        for p in pol:
            chan.append(set([l["channel"] for l in streams if l["polarization"] == p]))
        print(pol, chan)
        if not len(set(map(frozenset, chan))) <= 1:
            _LOG.error("Channel IDs of polarization do not match, got %s", str(chan))
            raise FailReply(f"Channel IDs of polarization do not match, got {str(chan)}")

    def remove_faulty_ostreams(self):
        """_summary_
        """
        # Validate if output_data_streams have a corresponding input stream
        istream_names = [istream["name"] for istream in self._config["input_data_streams"]]
        ostream_to_remove = []
        for ostream in self._config["output_data_streams"].keys():
            if ostream not in istream_names:
                _LOG.warning("No corresponding stream for output %s found, removing it", ostream)
                ostream_to_remove.append(ostream)
        for ostream in ostream_to_remove:
            del self._config["output_data_streams"][ostream]

def main_cli():
    """The main function wrapper used to build a CLI
    """
    asyncio.run(launchPipelineServer(VDIFPacker))

if __name__ == "__main__":
    main_cli()
