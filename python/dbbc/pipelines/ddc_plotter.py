#!@Python3_EXECUTABLE@
"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

The DDCPlotter subscribes to DDC streams and plots a spectra of the different DDC-streams
"""
import asyncio
import multiprocessing
from typing import List
# pylint: disable=wrong-import-position
from mpikat.core import cfgjson2dict
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.utils import numa
from mpikat.utils.spead_capture import SpeadCapture, SpeadPacket
from mpikat.utils.data_streams import RedisJSONSender, RedisSpectrumStreamFormat
import mpikat.core.logger as logging

from dbbc import data_streams as ds
from dbbc import round_nacc2time, sanitize_dictionary
# pylint: enable=wrong-import-position

_LOG = logging.getLogger("mpikat.dbbc.pipelines.ddc_plotter")

_DEFAULT_CONFIG = {
    "id": "DDCPlotter",
    "type": "DDCPlotter",
    "input_data_streams":
    [
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.123",
            "port": "8125",
            "name": "polarization_0_0_lower",
            "sync_time":0,
            "sample_rate":2e6,
            "center_freq":1e6,
            "samples_per_heap":2048
        },
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.124",
            "port": "8125",
            "name": "polarization_0_0_upper",
            "sync_time":0,
            "sample_rate":2e6,
            "center_freq":1e6,
            "samples_per_heap":2048
        }
    ],
    "output_data_streams":
    {
        "polarization_0_0_lower" :
        {
            "format": "RedisSpectrumStream:1",
            "ip": "",
            "port": ""
        },
        "polarization_0_0_upper" :
        {
            "format": "RedisSpectrumStream:1",
            "ip": "",
            "port": ""
        }
    },
    "spead_config": dict(SpeadCapture.DEFAULT_CONFIG),
    # User params
    "file_size":0,
    "output_directory": "/mnt/",
    "n_accumulate":"auto",
    "input_type":"float32",
    "output_type":"float32",
    "n_channel":1024, # Not used
    # Debug / expert params
    "log_level": "debug"
}

class DDCPlotter(EDDPipeline):
    """DDCPlotter pipeline class."""

    DATA_FORMATS = [ds.DDCDataStreamFormat.stream_meta]

    def __init__(self, ip: str, port: int, loop: asyncio.AbstractEventLoop=None):
        """
        Description:
            Construct the DDCPlotter pipeline.

        Args:
            - ip (str): The ip of the KATCP server
            - port (int): The port of the KATCP server
            - loop (asyncio.AbstractEventLoop, optinal): Defaults to None
        """
        super().__init__(ip, port, default_config=dict(_DEFAULT_CONFIG), loop=loop)
        self._numa_node = None
        self._capture: SpeadCapture = None
        self._nchannel = ds.DDCDataStreamFormat.samples_per_heap // 2
        self._streams: List[RedisJSONSender] = []
        self._queues: dict = {}
        _LOG.info("DDCPlotter pipeline instantiated")

    # --------------- #
    # Async functions #
    # --------------- #
    async def set(self, config_json: str):
        """
        Set the config but also set output data streams
        """
        if "output_data_streams" in config_json:
            config = cfgjson2dict(config_json)
            self._config["output_data_streams"] = config["output_data_streams"]
        else:
            self._config["output_data_streams"] = {}
        await super().set(config_json)

    # ---------------------- #
    # State change functions #
    # ---------------------- #
    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the EDD DDC plotter pipeline
        """
        _LOG.info("Configuring %s", self._config["id"])

        # Gathering NIC information
        nic_name, nic_desc = numa.getFastestNic()
        _LOG.info("Capturing on interface %s, ip %s, speed %s Mbit/s",
                   nic_name, nic_desc["ip"], nic_desc["speed"])

        if len(self._config["input_data_streams"]) == 0:
            _LOG.warning("0 input_data_streams given, won't plot")
        # Setup a shared and managed queue between SpeadCapture and SpectrumStreamHandler
        qmanager = multiprocessing.Manager()
        for i, desc in enumerate(self._config["input_data_streams"]):
            desc = sanitize_dictionary(self._config["input_data_streams"][i],
                                       ds.DDCDataStreamFormat.stream_meta)
            # Adjust spead_config
            self._config["spead_config"]["interface_address"] = nic_desc["ip"]
            self._config["spead_config"]["numa_affinity"] = numa.getInfo()[nic_desc['node']]['cores'][i*2:i+1]
            # Update output stream definitions
            name = desc["name"]
            # If there is no corresponding output_data_stream for the input stream use default config
            if name not in self._config["output_data_streams"]:
                self._config["output_data_streams"][name] = RedisSpectrumStreamFormat.stream_meta
            # Otherwise we check if all required params are in the dictionary
            else:
                self._config["output_data_streams"][name] = sanitize_dictionary(
                    self._config["output_data_streams"][name], RedisSpectrumStreamFormat.stream_meta)

            if self._config["n_accumulate"] == "auto":
                self._config["n_accumulate"] = round_nacc2time(desc["sample_rate"], 1, self._nchannel)
            self._config["output_data_streams"][name]["name"] = f"{self._config['id']}:{name}_stream"
            self._config["output_data_streams"][name]["ip"] = self._config["data_store"]["ip"]
            self._config["output_data_streams"][name]["port"] = int(self._config["data_store"]["port"])
            self._config["output_data_streams"][name]["dtype"] = self._config["output_type"]
            self._config["output_data_streams"][name]["sync_time"] = desc["sync_time"]
            self._config["output_data_streams"][name]["sample_rate"] = float(desc["sample_rate"])
            self._config["output_data_streams"][name]["center_freq"] = float(desc["center_freq"])
            self._config["output_data_streams"][name]["samples_per_heap"] = int(desc["samples_per_heap"])

            section_id = f"{desc['channel']}_{desc['polarization']}"
            self._queues[section_id] = qmanager.Queue(self._config["n_accumulate"])

            # Initialize streaming to Redis
            converter = ds.Converter(name, self._config["output_data_streams"][name],
                                     int(self._config["n_accumulate"]))
            sender = RedisJSONSender(self._config["data_store"]["ip"],
                                     self._config["data_store"]["port"], 2,
                                     self._config["id"], queue=self._queues[section_id],
                                     callback=converter.convert)
            sender.connect()
            sender.start()
            self._streams.append(sender)
        # Initialize SpeadCapture
        self._capture = SpeadCapture(self._config["input_data_streams"],
                                     ds.DDCSpeadHandler(),
                                     self.distribute_heaps,
                                     self._config["spead_config"])

        self._configUpdated()
        _LOG.info("Pipeline configured")


    @state_change(target="streaming", allowed=["configured"], intermediate="configured")
    async def capture_start(self):
        """
        start streaming output
        """
        if self._capture is not None:
            self._capture.start()
        _LOG.info("Started capturing")


    @state_change(target="idle", allowed=["streaming"], intermediate="streaming")
    async def capture_stop(self):
        """
        Stop streaming of data
        """
        await self.deconfigure()


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        deconfigure the pipeline.
        """
        self._destroy_capture()
        self._destroy_stream()
        self._queues = {}
        _LOG.info("Deconfigured")

    def distribute_heaps(self, heap: SpeadPacket):
        """Distribute the SpeadPackets to corresponding queues. This method is called
        by the SpeadCapture object on received heaps

        Args:
            heap (SpeadPacket): The heap / SpeadPacket
        """
        section_id = f"{heap.channel}_{heap.polarization}"
        self._queues[section_id].put_nowait(heap)


    # ---------------- #
    # Helper functions #
    # ---------------- #
    def _destroy_stream(self):
        """Destroy the SpectrumSpeadHandle-streams
        """
        _LOG.debug("Stopping streams")
        for stream in self._streams:
            if isinstance(stream, RedisJSONSender):
                if stream.is_alive():
                    stream.stop()
                    stream.join(1)
        self._streams = []
        _LOG.info("Streams stoped")


    def _destroy_capture(self):
        """Destroy the capture threads
        """
        _LOG.debug("Stopping capture")
        if isinstance(self._capture, SpeadCapture):
            if self._capture.is_alive():
                self._capture.stop()
                self._capture.join(5)
        self._capture = []
        _LOG.info("Capture stopped")

def main_cli():
    """The main function wrapper used to build a CLI
    """
    asyncio.run(launchPipelineServer(DDCPlotter))

if __name__ == "__main__":
    main_cli()
