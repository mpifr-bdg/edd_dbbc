#!@Python3_EXECUTABLE@
"""
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:
The DigitalDownConverter receives data in MPIFR EDD packetizer format. By default
the pipeline will consume one polarization stream. However, by using two GPUs on
of two different NUMA nodes the pipeline can handle two polarizations.

Data is captured by the mkrecv-process, processed by ddc_processing-script and
stored to disk or published to the network using the mksend CLI.

Configuration Settings
    input_data_streams (dict)
        The DigitalDownConverter requires at least one and at most two input
        streams. As mentioned for two input streams, two GPUs are required.

    output_samplerate (float):
        The samplerate per sideband in Hz. It is usually set to 2e6,4e6,..., 512e6

    f_lo (list of floats):
        The local oscillations for selecting the center frequencies of the
        sidebands. The length of the list is the number of channels*2 (lower and
        upper sideband per oscillation)

    output_type (list):
        Options are 'network', 'disk' or any.
            1. Use 'network' for streaming VDIF to the network.
            2. Use 'disk' to store the data.
            3. Use any arbitary stream to forward the output to /dev/null
"""

import os
import time
import json
import asyncio
import numpy as np
from typing import List

from aiokatcp.sensor import Sensor
from aiokatcp.connection import FailReply

from mpikat.core import cfgjson2dict
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.utils.process_tools import ManagedProcess, ManagedProcessPool
from mpikat.utils.process_monitor import SubprocessMonitor
from mpikat.utils.sensor_watchdog import SensorWatchdog, conditional_update
from mpikat.utils.core_manager import CoreManager
from mpikat.utils import numa
from mpikat.utils import dada_tools as dada
from mpikat.utils import mk_tools as mk
from mpikat.utils.data_streams import PacketizerStreamFormat as PFormat
from mpikat.utils.ip_utils import concatenate_ips
import mpikat.core.logger as logging

from dbbc import folder_string, make_default_header
from dbbc import ddc
from dbbc.data_streams import DDCDataStreamFormat as DDCFormat


_LOG = logging.getLogger("mpikat.dbbc.pipelines.ddc_processor")

_DEFAULT_CONFIG = {

    "id": "DigitalDownConverter",
    "type": "DigitalDownConverter",
    # Debug / expert params
    "samples_per_block": 0,
    "minimum_size":2**24,
    "output_bit_depth": 32,
    "log_level": "debug",
    "input_data_streams":
    [
        {
            "format": "MPIFR_EDD_Packetizer:1",         # Format has version seperated via colon
            "ip": "225.0.0.112+3",
            "port": "7148",
            "bit_depth" : 8,
            "sample_rate" : int(1.024e9),
            "samples_per_heap": 4096,                     # this needs to be consistent with the mkrecv configuration
            "polarization":0
        }
    ],
    "output_data_streams":
    {
        "polarization_0_0_lower" :
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.123",
            "port": "8125"
        },
        "polarization_0_0_upper" :
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.124",
            "port": "8125"
        }
    },
    # User params
    "f_lo" : [256e6],
    "output_samplerate" : int(128e6),
    "output_directory": "/mnt/",
    "nonfatal_numacheck":False,
    "output_type":["network"],
    "input_type":"network",
    "input_file":"",
    "channel_id_start":0,
    "file_duration":60
}

class DigitalDownConverter(EDDPipeline):
    """
    DigitalDownConverter pipeline class.
    """
    DATA_FORMAT = [DDCFormat.stream_meta]
    def __init__(self, ip: str, port: int, loop: asyncio.AbstractEventLoop=None):
        """
        Description:
            Construct the DigitalDownConverter pipeline.

        Args:
            - ip (str): The ip of the KATCP server
            - port (int): The port of the KATCP server
            - loop (asyncio.AbstractEventLoop, optinal): Defaults to None
        """
        # Call to the base class for constructing the pipeline
        _LOG.info("DigitalDownConverter pipeline instantiated")
        super().__init__(ip, port, default_config=dict(_DEFAULT_CONFIG), loop=loop)
        self._buffer_sensors = {}
        self._numa_nodes = []
        self._watchdogs: List[SensorWatchdog] = []
        self._core_manager: List[CoreManager] = []
        self._dada_buffers: List[dada.DadaBuffer] = []
        self._capture_pool: ManagedProcessPool = ManagedProcessPool()
        self._process_pool: ManagedProcessPool = ManagedProcessPool()
        self._n_channel: int = 0
        self._configure_ts: int = 0

    async def set(self, config_json: str):
        """
        Set the config but also set output data streams
        """
        if "output_data_streams" in config_json:
            config = cfgjson2dict(config_json)
            self._config["output_data_streams"] = config["output_data_streams"]
        await super().set(config_json)

    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the EDD ddc processor pipeline
        """
        _LOG.info("Configuring %s", self._config["id"])
        self._configure_ts = time.time()
        self._set_numa_node()
        self._core_manager = []
        self._subprocessMonitor = SubprocessMonitor()
        self._n_channel = len(self._config["f_lo"])*2

        if not isinstance(self._config["output_type"], list):
            self._config["output_type"] = [self._config["output_type"]]

        if len(self._config["output_data_streams"]) // 2\
            != len(self._config["f_lo"])*len(self._config["input_data_streams"]):
            _LOG.error("Number of 'output_data_streams' is not a double of number of 'f_lo'")
            raise FailReply("Number of 'output_data_streams' is not a double of number of 'f_lo'")

        #-------------------- #
        # Prepare environment #
        #-------------------- #
        for i, stream_desc in enumerate(self._config['input_data_streams']):
            buffer_creation_tasks = []
            pid = f"polarization_{stream_desc['polarization']}"
            _LOG.debug("Associating %s with numa node %s", pid, self._numa_nodes[i])
            nic, nic_params = numa.getFastestNic(self._numa_nodes[i])
            if self._config["samples_per_block"] == 0:
                self._config["samples_per_block"] = int(ddc.get_nsamples(
                    int(stream_desc["sample_rate"]),
                    int(self._config["output_samplerate"]),
                    DDCFormat.samples_per_heap,
                    self._config["minimum_size"]))

            self._core_manager.append(CoreManager(self._numa_nodes[i]))
            self._buffer_sensors[pid] = {}
            self.add_input_buffer_sensor(pid)
            self.add_output_buffer_sensor(pid)
            #-------------------#
            # INPUT DADA BUFFER #
            #-------------------#
            _LOG.debug("Creating buffer for input stream %s", pid)
            n_heaps = self._config["samples_per_block"]\
                / stream_desc["samples_per_heap"]
            buffer_size = n_heaps * stream_desc["samples_per_heap"]
            self._dada_buffers.append(dada.DadaBuffer(buffer_size, node_id=i, pin=True, lock=True))
            buffer_creation_tasks.append(asyncio.create_task(self._dada_buffers[-1].create()))
            _LOG.info(f'Input parameters for stream {pid}:\n\
                Heap size:        {stream_desc["samples_per_heap"]} byte\n\
                Heaps per block:  {n_heaps}\n\
                Data rate         {stream_desc["sample_rate"]*stream_desc["bit_depth"]//1e6} Mb/s\n\
                Buffer size:      {buffer_size} byte')
            #------------------- #
            # OUTPUT DADA BUFFER #
            #------------------- #
            _LOG.debug("Creating buffer for %s", pid)
            nsamples_per_channel = self._config["output_samplerate"] \
                *  self._config["samples_per_block"] // stream_desc["sample_rate"]
            nsamples_total = nsamples_per_channel * self._n_channel
            buffer_size = nsamples_total * self._config["output_bit_depth"] // 8
            fill_rate = self._config["output_samplerate"] * self._n_channel * 4 # 4 = compute type float32
            self._dada_buffers.append(dada.DadaBuffer(buffer_size, node_id=i, pin=True, lock=True,
                                                      n_reader=len(self._config["output_type"])))
            buffer_creation_tasks.append(asyncio.create_task(self._dada_buffers[-1].create()))
            stream_desc["key"] = self._dada_buffers[-2].key
            stream_desc["nic"] = nic_params["ip"]
            stream_desc["heap_size"] = PFormat.samples_per_heap * stream_desc["bit_depth"] // 8
            self._output_rate_status.set_value(fill_rate/1e9)
            _LOG.info(f'Buffer properties of {pid}:\n\
                Fill rate:      {fill_rate/1e6} Mb/s\n\
                Buffer size:    {buffer_size} byte\n\
                Samples:        {nsamples_total}')
            #------------------------- #
            # OUTPUT STREAM DEFINITION #
            #------------------------- #
            cnt = 0
            for name, desc in self._config["output_data_streams"].items():
                if pid not in name:
                    _LOG.warning("Cannot associate stream %s with an input stream %s", name, pid)
                    continue
                # Update output stream description
                desc["name"] = name
                desc["sync_time"] = stream_desc["sync_time"]
                desc["bit_depth"] = self._config["output_bit_depth"]
                desc["sample_rate"] = self._config["output_samplerate"]
                desc["polarization"] = stream_desc["polarization"]
                desc["channel"] = self._config["channel_id_start"] + cnt
                desc["samples_per_heap"] = DDCFormat.samples_per_heap
                desc["heap_size"] = DDCFormat.samples_per_heap * DDCFormat.data_type(0).itemsize
                desc["type"] = "DigitalDownConverter:1"
                desc["center_freq"] = ddc.get_center_frequency(self._config["f_lo"][cnt//2],
                                                               self._config["output_samplerate"])[cnt%2]
                _LOG.info(f'Output parameters for stream {i}:\n\
                    Data rate:      {fill_rate / (1e6 * self._n_channel)} Mb/s\n\
                    Name:           {name}\n\
                    IP:             {desc["ip"]}\n\
                    Port:           {desc["port"]}')
                cnt += 1
            # Before starting the subprocesses await all buffers to be allocated
            await asyncio.gather(*buffer_creation_tasks)

            # ---------------------------------- #
            # Reserve CPU cores for applications #
            # ---------------------------------- #
            n_ip_recv = len(concatenate_ips(stream_desc["ip"].split(',')))
            self._core_manager[-1].add_task(f"ouput_handle_{pid}", 2)
            self._core_manager[-1].add_task(f"mkrecv_{pid}", n_ip_recv+1, prefere_isolated=True)
            #-------------------- #
            # Start the processes #
            #-------------------- #
            _LOG.info("Sending on %s [%s] @ %s Mbit/s", nic, nic_params['ip'], nic_params['speed'])
            self.start_output_process(pid, nic_params['ip'])
            self.start_ddc_process(i)

        for buf in self._dada_buffers:
            if buf.n_reader == 1:
                buf.get_monitor(self._buffer_status_handle).start()
        self._subprocessMonitor.start()
        self._configUpdated()
        _LOG.info("Final configuration %s", json.dumps(self._config, indent=4))


    @state_change(target="streaming", allowed=["configured"], intermediate="capture_starting")
    async def capture_start(self):
        """
        start streaming output
        """
        _LOG.info("Starting DDC pipeline")
        for i, stream_desc in enumerate(self._config['input_data_streams']):
            pid = f"polarization_{stream_desc['polarization']}"
            nic, params = numa.getFastestNic(self._numa_nodes[i])
            self.start_capture_process(stream_desc)
            self._watchdogs.append(SensorWatchdog(
                self._buffer_sensors[pid]["input_buffer_total_write"],
                10, self.watchdog_error))
            self._watchdogs[-1].start()
            _LOG.info("Receiving on %s [%s] @ %s Mbit/s", nic, params['ip'], params['speed'])


    @state_change(target="configured", allowed=["streaming"], intermediate="capture_stopping")
    async def capture_stop(self):
        """
        Stop streaming of data
        """
        _LOG.debug("Stopping capturing processes ...")
        self._capture_pool.terminate()
        self._capture_pool.clean()
        for wd in self._watchdogs:
            wd.stop_event.set()
        self._watchdogs = []


    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        deconfigure the pipeline.
        """
        _LOG.info("Deconfiguring DDCPipeline backend")
        for wd in self._watchdogs:
            wd.stop_event.set()
        if self._subprocessMonitor is not None:
            self._subprocessMonitor.stop()
        _LOG.debug("Stopping processes ...")
        self._capture_pool.terminate(threaded=True)
        self._capture_pool.clean()
        self._process_pool.terminate(threaded=True)
        self._process_pool.clean()
        _LOG.debug("Destroying dada buffers")
        destroy_tasks = []
        for buffer in self._dada_buffers:
            destroy_tasks.append(asyncio.create_task(buffer.destroy()))
        await asyncio.gather(*destroy_tasks)
        self._config["samples_per_block"] = 0
        # Reset lists
        self._dada_buffers = []
        self._numa_nodes = []
        self._watchdogs = []


    def start_output_process(self, stream_id: str, nic_ip: str) -> None:
        """Start the output process, which can be
            1 network = mksend
            2 disk = dada_dbdisk;
            3 null = dada_dbnull

        Args:
            stream_id (str): The stream ID
            node (int): The node ID where to run the output handler
        """
        physcpu = self._core_manager[-1].get_coresstr(f"ouput_handle_{stream_id}")
        for otype in self._config["output_type"]:
            if otype == 'network':
                channel_list = range(self._config["channel_id_start"],
                                     self._config["channel_id_start"] + self._n_channel)
                streams = [desc for key, desc in self._config["output_data_streams"].items()
                           if stream_id in key]
                handler = mk.Mksend(streams, self._dada_buffers[-1].key, physcpu)
                handler.set_header("heap_count", self._n_channel)
                handler.set_header("ibv_if", nic_ip)
                handler.set_header("rate", self._config["output_samplerate"] * self._n_channel
                                  * DDCFormat.data_type(0).itemsize * 1.1)
                handler.set_header("sample_clock_start", "unset")
                handler.set_header("sample_clock", "unset")
                handler.set_item("timestamp", "step", DDCFormat.samples_per_heap)
                handler.set_item("timestamp", "index", 1)
                handler.set_item("channel", "list", mk.get_index_list(channel_list))
                handler.set_item("channel", "index", 2)
                handler.set_item("polarization", "list", streams[0]["polarization"])
            elif otype == 'disk':
                ofpath = folder_string(self._configure_ts, self._config["output_directory"], stream_id)
                if not os.path.isdir(ofpath):
                    os.makedirs(ofpath)
                handler = dada.DbDisk(self._dada_buffers[-1].key, ofpath, physcpu=physcpu)
                _LOG.info("Writing output to %s", ofpath)
            else:
                _LOG.warning("Selected null output. Not sending data!")
                handler = dada.DbNull(self._dada_buffers[-1].key, True, physcpu=physcpu)
            proc = handler.start()
            self.add_process(proc)


    def start_ddc_process(self, stream_id: int) -> None:
        """
        Starts the digital down converter process

        Args:
            desc (dict): description for the DDC
            name (str): Name of the stream
            node (int): The node ID where to capture the data
        """
        # physcpu = self._core_manager[name].get_coresstr(f"ddc_processor_{name}")
        cmd = []
        cmd.append(f'numactl --cpunodebind {self._numa_nodes[stream_id]}')
        cmd.append('ddc_processor')
        cmd.append(f'--input_key {self._dada_buffers[stream_id*2].key}')
        cmd.append(f'--output_key {self._dada_buffers[stream_id*2+1].key}')
        cmd.append(f'--oscillations {",".join([str(i) for i in self._config["f_lo"]])}')
        cmd.append(f'--sample_clock {int(self._config["input_data_streams"][stream_id]["sample_rate"])}')
        cmd.append(f'--output_clock {int(self._config["output_samplerate"])}')
        cmd.append(f'--device {numa.getCudaDeviceHighestComputeArch(self._numa_nodes[stream_id])}')
        cmd.append(f'--nsamples_pack {DDCFormat.samples_per_heap}')
        cmd = ' '.join(cmd)
        _LOG.info("DDC processor command: %s", cmd)
        proc = ManagedProcess(cmd, env={"CUDA_DEVICE_ORDER": "PCI_BUS_ID"})
        self.add_process(proc)


    def start_capture_process(self, desc: dict) -> None:
        """
        Starts the capture process

        Args:
            desc (dict): Description of the input data stream
        """
        stream_id = f"polarization_{desc['polarization']}"
        physcpu = self.get_cores_by_taskname(f"mkrecv_{stream_id}")
        file_size = ddc.volume(self._config["output_samplerate"], self._config["file_duration"],
                               self._n_channel, self._config["output_bit_depth"],
                               DDCFormat.samples_per_heap)
        default_header = make_default_header({"sample_clock_start":0, "file_size":file_size})
        # Do network capturing
        if self._config['input_type'] == "network":
            modulo = np.lcm(int(PFormat.samples_per_heap),
                            int(desc["sample_rate"]))
            handler = mk.Mkrecv(desc, desc["key"], physcpu)
            handler.set_header("packet_size", 8400)
            handler.set_header("buffer_size", 128000000)
            handler.set_header("dada_nslots", 3)
            handler.set_header("slots_skip", 0)
            handler.set_header("file_size", file_size)
            handler.set_item("timestep", "step", PFormat.samples_per_heap)
            handler.set_item("timestep", "index", 1)
            handler.set_item("timestep", "modulo", modulo)
            handler.stdout_handler(self._buffer_sensors[stream_id]["mkrecv_sensors"].stdout_handler)
        # Do disk reading
        elif self._config['input_type'] == "disk":
            if not os.path.exists(self._config["input_file"]):
                _LOG.error(f"File {self._config['input_file']} does not exists")
                raise FailReply(f"File {self._config['input_file']} does not exists")
            # Read the file for an hour
            n_reads = int(3600/(os.path.getsize(self._config["input_file"]) / desc["sample_rate"]))
            handler = dada.DiskDb(desc["key"], self._config["input_file"], default_header,
                                  n_reads, desc["sample_rate"], physcpu=physcpu)
        # Do dummy mode
        elif self._config['input_type'] == "dummy":
            _LOG.warning("Creating dummy input instead of listening to network!")
            handler = dada.JunkDb(desc["key"], default_header, desc["sample_rate"], 3600, 1, physcpu=physcpu)
        proc = handler.start()
        self._capture_pool.add(proc)
        self._subprocessMonitor.add(proc, self._subprocess_error)

    def _set_numa_node(self):
        """
        Set NUMA nodes
        """
        # Get NUMA node and its charachteristics
        for node_id, desc in numa.getInfo().items():
            # remove numa nodes with missing capabilities
            if len(desc['gpus']) < 1:
                _LOG.debug("Not enough gpus on numa node %i - removing from pool.", node_id)
            elif len(desc['net_devices']) < 1:
                _LOG.debug("Not enough nics on numa node %i - removing from pool.", node_id)
            else:
                self._numa_nodes.append(node_id)

        _LOG.info("NUMA nodes %i remain in pool after constraints.", len(self._numa_nodes))
        # Proof if enough NUMA nodes are available
        n_istreams = len(self._config['input_data_streams'])
        if n_istreams > len(self._numa_nodes):
            if self._config["nonfatal_numacheck"]:
                _LOG.warning("Not enough NUMA nodes to process data")
                self._numa_nodes = list(numa.getInfo().keys())
            else:
                _LOG.error(f"Not enough numa nodes to process {n_istreams} streams!")
                raise FailReply(f"Not enough numa nodes to process {n_istreams} streams!")


    def setup_sensors(self):
        """
        Setup monitoring sensors
        """
        super().setup_sensors()
        self._output_rate_status = Sensor(
            float, "output-rate",
            description="Output data rate [Gbps]",
            initial_status=Sensor.Status.UNKNOWN
        )
        self.sensors.add(self._output_rate_status)


    def add_input_buffer_sensor(self, stream_id: str):
        """Add sensors for input buffer by given stream_id.

        Args:
            stream_id (str): The stream_id of the stream
        """
        self._buffer_sensors[stream_id]["mkrecv_sensors"] = mk.MkrecvSensors(stream_id)
        for s in self._buffer_sensors[stream_id]["mkrecv_sensors"].sensors.values():
            self.sensors.add(s)
        self._buffer_sensors[stream_id]["input_buffer_fill_level"] = Sensor(
            float, f"input-buffer-fill-level-{stream_id.replace('_', '-')}",
            f"Fill level of the input buffer for stream{stream_id}",
            initial_status=Sensor.Status.UNKNOWN)
        self._buffer_sensors[stream_id]["input_buffer_total_write"] = Sensor(
            float, f"input-buffer-total-write-{stream_id.replace('_', '-')}",
            f"Total write into input buffer for stream {stream_id}",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._buffer_sensors[stream_id]["input_buffer_fill_level"])
        self.sensors.add(self._buffer_sensors[stream_id]["input_buffer_total_write"])
        self.mass_inform("interface-changed")

    def add_output_buffer_sensor(self, stream_id: str):
        """Add sensors for monitoring the output buffers

        Args:
            stream_id (str): The stream_id of the stream
        """
        self._buffer_sensors[stream_id]["output_buffer_fill_level"] = Sensor(
            float, f"output-buffer-fill-level-{stream_id.replace('_', '-')}",
            "Fill level of output buffer for stream {stream_id}",
            initial_status=Sensor.Status.UNKNOWN)
        self._buffer_sensors[stream_id]["output_buffer_total_read"] = Sensor(
            float, f"output-buffer-total-read-{stream_id.replace('_', '-')}",
            f"Total read of output buffer for stream {stream_id}",
            initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._buffer_sensors[stream_id]["output_buffer_total_read"])
        self.sensors.add(self._buffer_sensors[stream_id]["output_buffer_fill_level"])
        self.mass_inform("interface-changed")

    def _buffer_status_handle(self, status: dict):
        """
        Process a change in the DADA buffer status

        Args:
            status (dict): A dictionary containing information of the DADA buffer status
        """
        timestamp = time.time()
        for stream_desc in self._config["input_data_streams"]:
            key = f"polarization_{stream_desc['polarization']}"
            if status['key'] in [buf.key for buf in self._dada_buffers[::2]]:
                conditional_update(self._buffer_sensors[key]["input_buffer_total_write"],
                                   status['written'], timestamp=timestamp)
                self._buffer_sensors[key]["input_buffer_fill_level"]\
                    .set_value(status['fraction-full'], timestamp=timestamp)
            if status['key'] in [buf.key for buf in self._dada_buffers[1::2]]:
                conditional_update(self._buffer_sensors[key]["output_buffer_total_read"],
                                   status['read'], timestamp=timestamp)
                self._buffer_sensors[key]["output_buffer_fill_level"]\
                    .set_value(status['fraction-full'], timestamp=timestamp)

    def add_process(self, proc: ManagedProcess) -> None:
        """
        Adds a process to the pipeline

        Args:
            proc (ManagedProcess): the process to add
        """
        self._subprocessMonitor.add(proc, self._subprocess_error)
        self._process_pool.add(proc)


    def get_cores_by_taskname(self, taskname:str) -> str:
        """_summary_

        Args:
            taskname (str): _description_

        Returns:
            str: _description_
        """
        for manager in self._core_manager:
            if taskname in manager.tasknames:
                return manager.get_coresstr(taskname)
        return None

def main_cli():
    """The main function wrapper used to build a CLI
    """
    asyncio.run(launchPipelineServer(DigitalDownConverter))

if __name__ == "__main__":
    main_cli()
