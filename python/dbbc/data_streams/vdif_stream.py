from mpikat.core.data_stream import DataStream
from mpikat.core.logger import getLogger

_LOG = getLogger("mpikat.dbbc..data_streams.vdif_stream")

class VDIFDataStreamFormat(DataStream):
    """
    Class for VDIF data stream
    """
    stream_meta = {
        "type":"VDIF:1",
        "ip":"",
        "port":"",
        "name":"",
        "description":"The EDD VDIF stream 1",
        "sync_time":0,
        "center_freq":0,
        "sample_rate":2e6,
        "thread_id":0,
        "station_id":0,
        "payload_size": 8000,
        "data_rate": 500e3,
        "channels":1,
    }
