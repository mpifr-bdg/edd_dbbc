import tempfile
import time
import numpy as np
import spead2
import spead2.recv

from mpikat.core import logger
from mpikat.core.data_stream import DataStream, edd_spead
from mpikat.utils.spead_capture import SpeadPacket

_LOG = logger.getLogger("mpikat.dbbc..data_streams.ddc_stream")

class DDCDataStreamFormat(DataStream):
    """
    Format description of the DigitalDownConverter
    """
    samples_per_heap = 2048
    data_type = np.float32
    data_items = [
        edd_spead(5632, "timestamp", "The timestamp of the current heap", (6,), np.int8),
        edd_spead(5633, "channel", "The channel ID of the current heap", (6,), np.int8),
        edd_spead(5634, "polarization", "The polarization of the current heap", (6,), np.int8),
        edd_spead(5635, "data", "Payload data of spead heap", (samples_per_heap,), data_type)
    ]

    stream_meta = {
        "type":"DigitalDownConverter:1",
        "description":"The DigitalDownConverter stream 1",
        "ip":"239.0.255.1",
        "nic":"127.0.0.1",
        "port":8125,
        "name": "default",
        "sync_time":0,
        "sample_rate":2e6,
        "center_freq":1e6,
        "samples_per_heap":samples_per_heap,
        "polarization":0,
        "channel": 0,
        "dtype": "float32",
        "bit_depth":data_type(0).itemsize*8
    }


class DDCSpeadHandler():
    """
    Class for handling spead heaps in the DigitalDownConverter:1 format
    """
    def __init__(self):
        """
        Constructor
        """
        self.format = DDCDataStreamFormat()
        self.item_group: spead2.ItemGroup = self.format.create_item_group()
        self.packet: SpeadPacket = None

    def __call__(self, heap: spead2.recv.Heap) -> SpeadPacket:
        """Called when a heap is received

        Args:
            heap (spead2.recv.Heap): A received heap

        Returns:
            SpeadPacket: The built SpeadPacket
        """
        return self.build_packet(self.item_group.update(heap))


    def build_packet(self, items: spead2.ItemGroup) -> SpeadPacket:
        """Builds a SPeadPacket according to the passed ItemGroup
            and validates the passed items

        Args:
            items (spead2.ItemGroup): The ItemGroup

        Returns:
            SpeadPacket: The built SpeadPacket
        """
        if not DDCDataStreamFormat.validate_items(items):
            return None
        return SpeadPacket(items)


_mkrecv_header ="""
HEADER DADA
HDR_VERSION 1.0
HDR_SIZE 4096
DADA_VERSION 1.0
SAMPLE_CLOCK_START unset
PACKET_SIZE 8400
IBV_VECTOR -1
IBV_MAX_POLL 10
BUFFER_SIZE 128000000
DADA_NSLOTS 5
NTHREADS 32
NHEAPS 64
NGROUPS_TEMP 65536
NINDICES 2

IDX1_ITEM 0
IDX1_INDEX 1

IDX2_ITEM 1
IDX2_INDEX 2
IDX2_MASK 0xffffffff

IDX3_ITEM 2
IDX3_INDEX 3
IDX3_MASK 0xffffffff
"""

def ddc_mkrecv_header(desc: dict) -> str:
    """
    Create a mkrecv header file

    Args:
        desc (dict):

    Returns:
        str: filename of the header file
    """
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as f:
        if "sync_time" not in desc:
            _LOG.warning("No 'sync_time' in stream description, using now()")
            desc["sync_time"] = time.time()
        _LOG.debug("Creating mkrecv header: %s", f.name)
        f.write(_mkrecv_header)
        f.write(f'NBIT {desc["bit_depth"]}\n')
        if "file_size" in desc:
            f.write(f'FILE_SIZE {desc["file_size"]}\n')
        else:
            f.write(f'FILE_SIZE {1024*1024*512}\n')
    return f.name

def ddc_mkrecv_cmd(desc: dict) -> str:
    """
    Construct the mkrecv command with the given params

    Args:
        desc (dict): The dictionary describing the receiving stream
    Returns:
        str: _description_
    """
    header_file = ddc_mkrecv_header(desc)
    idx1_modulo = np.lcm(int(desc["samples_per_heap"]), int(desc["sample_rate"]))
    cmd = []
    if "physcpu" in desc:
        cmd.append(f'taskset -c {desc["physcpu"]} mkrecv_v4 --quiet')
    else:
        cmd.append('mkrecv_v4 --quiet')
    cmd.append(f'--header {header_file}')
    cmd.append(f'--heap-size {(desc["samples_per_heap"]*desc["bit_depth"])//8}')
    cmd.append(f'--idx1-step {desc["samples_per_heap"]}')
    cmd.append(f'--idx1-modulo {idx1_modulo}')
    cmd.append(f'--idx2-list {desc["polarization_list"]}')
    cmd.append(f'--idx3-list {desc["channel_list"]}')
    cmd.append(f'--dada-key {desc["key"]}')
    cmd.append(f'--sync-epoch {desc["sync_time"]}')
    cmd.append(f'--sample-clock {desc["sample_rate"]}')
    cmd.append(f'--ibv-if {desc["nic"]}')
    cmd.append(f'--port {desc["port"]}')
    cmd.append(f'{desc["ip"]}')
    return ' '.join(cmd)
